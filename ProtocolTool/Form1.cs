﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProtocolTool
{
 
    public partial class MianForm : Form
    {
        private DeviceManager deviceManager;
        private DeviceManager.ProcessInfo[] procs;
        public MianForm()
        {
            InitializeComponent();
            //init listview
            listView1.View = View.Details;
            listView1.Columns.Add("序列号", 50);
            listView1.Columns.Add("进程id", 100);
            listView1.Columns.Add("进程名", 300);
        }

        private void getprocess_Click(object sender, EventArgs e)
        {
            if(deviceManager == null)
            {
                deviceManager = new DeviceManager(this.textBox1.Text);
                if (!deviceManager.connectDevice())
                {
                    this.statusStrip1.Text = "连接设备失败";
                    deviceManager = null;
                    return;
                }
            }
            
            //DeviceManager.ProcessInfo info;
            listView1.Items.Clear();
            DeviceManager.ProcessInfo[] procs = deviceManager.getAllPorc();
            this.procs = procs;
            int i = 0;
            foreach(DeviceManager.ProcessInfo info in procs)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = i.ToString();
                lvi.SubItems.Add(info.pid.ToString());
                lvi.SubItems.Add(info.name);
                this.listView1.Items.Add(lvi);
                i++;
            }
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //do sth.
            string strPid = listView1.FocusedItem.SubItems[1].Text;
            int pid;
            int.TryParse(strPid, out pid);
            deviceManager.traceProcess(pid);
            Form mainForm = new ProtoDetailForm("Mobile");
            this.Hide();
            mainForm.ShowDialog();
            this.Show();
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.F:
                    if(e.Control)
                    {
                        this.findBox.Visible = true;
                        this.findBox.Focus();
                        e.Handled = true;
                    }
                    break;
                default:
                    break;
            }
        }

        private void findBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                this.findBox.Text = "";
                this.findBox.Visible = false;
                e.Handled = true;
            }
        }

        private void findBox_TextChanged(object sender, EventArgs e)
        {
            this.listView1.Items.Clear();
            string strFlt = this.findBox.Text;
            int i = 0;
            foreach(DeviceManager.ProcessInfo info in this.procs)
            {
                if(info.name.Contains(strFlt))
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = i.ToString();
                    lvi.SubItems.Add(info.pid);
                    lvi.SubItems.Add(info.name);
                    this.listView1.Items.Add(lvi);
                    i++;
                }
            }
            this.listView1.Update();
        }

        private void btnPCStart_Click(object sender, EventArgs e)
        {
            Form mainForm = new ProtoDetailForm("PC");
            this.Hide();
            mainForm.ShowDialog();
            this.Show();
        }
    }
}
