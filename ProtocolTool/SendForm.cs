﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Be.Windows.Forms;

namespace ProtocolTool
{
    public partial class SendForm : Form
    {
        private MemoryStream stream;
        private ProtoDetailForm mainnForm;
        private int client_fd;
        public SendForm()
        {
            InitializeComponent();
        }

        public bool initData(ProtoDetailForm mainFrom, int client_fd, byte[] hexData)
        {
            this.mainnForm = mainFrom;
            this.client_fd = client_fd;
            stream = new MemoryStream(hexData);
            DynamicFileByteProvider dynamicProvider = new DynamicFileByteProvider(stream);
            this.hexBox1.ByteProvider = dynamicProvider;
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (hexBox1.ByteProvider == null)
                return;
            DynamicFileByteProvider dynamicProvider = this.hexBox1.ByteProvider as DynamicFileByteProvider;
            dynamicProvider.ApplyChanges();
            byte[] hexData = new byte[stream.Length];
            stream.Position = 0;
            stream.Read(hexData, 0, hexData.Length);
            this.mainnForm.SendToGame(client_fd, hexData);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
