﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolTool
{
    class RemoteCall
    {
        public static string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    returnStr += bytes[i].ToString("X2");
                }
            }
            return returnStr;
        }

        public static byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public virtual bool Connecnt(int port)
        {
            return false;
        }

        public virtual bool Disconnecnt()
        {
            return false;
        }

        public virtual bool startSniff()
        {
            return false;
        }

        public virtual bool pauseSniff()
        {
            return false;
        }

        public virtual bool sendGamePack(int client_fd, byte[] hexData)
        {
            return false;
        }

        public virtual bool sendGamePack(int client_fd, string strData)
        {
            return false;
        }
    }
}
