﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ProtocolTool
{
    public partial class ProtoDetailForm : Form
    {
        private RemoteCall remoteCall;
        private Dictionary<int, String> proto_dic = new Dictionary<int, String>();
        private List<string> flterId = new List<string>();
        private List<string> onlyShow = new List<string>();
        private string type;

        public delegate void OutDelegate(int type, int fd, string ipAddr, string data);

        public bool LoadProtoInfo(string strFilePath)
        {
            StreamReader reader = new StreamReader(strFilePath);
            string line;
            while((line = reader.ReadLine()) != null)
            {
                string[] strArray = line.Split(':');
                string strId = strArray[0].Replace(" ", "");
                string strName = strArray[1].Replace(" ", "");
                int id = Convert.ToInt32(strId, 16);
                proto_dic.Add(id, strName);
            }
            return true;
        }
        public ProtoDetailForm(string type)
        {
            InitializeComponent();

            //init listview1
            listView1.Columns.Add("序列号", 50);
            listView1.Columns.Add("类型", 70);
            listView1.Columns.Add("目标地址", 150);
            listView1.Columns.Add("套接字句柄", 100);
            listView1.Columns.Add("描述", 300);
            listView1.Columns.Add("数据", 200);

            this.type = type;
            //this.hexBox1.Font = new Font(SystemFonts.MessageBoxFont.FontFamily, SystemFonts.MessageBoxFont.Size, SystemFonts.MessageBoxFont.Style);
            LoadProtoInfo("proto-id.txt");
        }

        private void 打开ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            string hexData = this.listView1.FocusedItem.SubItems[5].Text;
            DynamicByteProvider dynamicByteProvider = new DynamicByteProvider(hexData);
            this.hexBox1.ByteProvider = dynamicByteProvider;

            string protoName = this.listView1.FocusedItem.SubItems[4].Text;
            if (protoName.Length < 0)
            {
                return;
            }

            if (hexData.Length < 0)
            {
                return;
            }
            byte[] hexByte = RemoteCallMoblie.strToToHexByte(hexData);
            StringBuilder sb = new StringBuilder(0x4096);
            ProtoExt.explain_proto(protoName, hexByte, hexByte.Length, sb, sb.Capacity);
            explainTextBox.Text = sb.ToString();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if(remoteCall == null)
            {
                //todo
                if (type == "Mobile")
                {
                    remoteCall = new RemoteCallMoblie(this);
                    remoteCall.Connecnt(20086);
                } else if (type == "PC")
                {
                    remoteCall = new RemoteCallPC(this);
                    remoteCall.Connecnt(20086);
                }
            }
            remoteCall.startSniff();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            remoteCall.pauseSniff();
        }

        public void OutText(int type, int fd, string ipAddr, string data)
        {
            if (listView1.InvokeRequired)
            {
                OutDelegate outdelegate = new OutDelegate(OutText);
                this.BeginInvoke(outdelegate, new object[] { type, fd, ipAddr, data });
                return;
            }
            
            if (type == 0 && !Send_ToolStripMenuItem.Checked)
                return;
            if (type == 1 && !Recv_ToolStripMenuItem.Checked)
                return;
            ListViewItem lvi;
            string[] tmp = ipAddr.Split(':');
            int port = Convert.ToInt32(tmp[1]);
            string strProtoId = "";
            if(port == 80)
            {
                lvi = new ListViewItem();
                int index = listView1.Items.Count;
                lvi.Text = index.ToString();
                if (type == 0)
                {
                    lvi.SubItems.Add("发包");
                    lvi.BackColor = Color.PeachPuff;
                }
                else
                    lvi.SubItems.Add("收包");
                lvi.SubItems.Add(ipAddr);
                lvi.SubItems.Add(fd.ToString());
                lvi.SubItems.Add("http协议");
            }
            else
            {
                string strDesc = "";
                for (int i = 0; i < 4; i++)
                {
                    strProtoId += data.Substring((7 - i) * 2, 2);
                }
                int id = Convert.ToInt32(strProtoId, 16);
                if (proto_dic.Keys.Contains(id))
                {
                    strDesc = proto_dic[id];
                }
                if(onlyShow.Count > 0)
                {
                    if(onlyShow.Contains(strDesc))
                    {
                        lvi = new ListViewItem();
                        int index = listView1.Items.Count;
                        lvi.Text = index.ToString();
                        if (type == 0)
                        {
                            lvi.SubItems.Add("发包");
                            lvi.BackColor = Color.PeachPuff;
                        }
                        else
                        {
                            lvi.SubItems.Add("收包");
                        }
                        lvi.SubItems.Add(ipAddr);
                        lvi.SubItems.Add(fd.ToString());
                        lvi.SubItems.Add(strDesc);
                    }
                    else
                    {
                        return;
                    }
                }
                else if(!flterId.Contains(strDesc))
                {
                    lvi = new ListViewItem();
                    int index = listView1.Items.Count;
                    lvi.Text = index.ToString();
                    if (type == 0)
                    {
                        lvi.SubItems.Add("发包");
                        lvi.BackColor = Color.PeachPuff;
                    }
                    else
                    {
                        lvi.SubItems.Add("收包");
                    }
                    lvi.SubItems.Add(ipAddr);
                    lvi.SubItems.Add(fd.ToString());
                    lvi.SubItems.Add(strDesc);
                }
                else
                {
                    return;
                }
                    
            }
            lvi.SubItems.Add(data);
            this.listView1.Items.Add(lvi);
        }

        private void updataListView()
        {
            if(onlyShow.Count > 0)
            {
                for (int i = listView1.Items.Count - 1; i >= 0; i--)
                {
                    string strDesc = listView1.Items[i].SubItems[4].Text;
                    if (!onlyShow.Contains(strDesc))
                    {
                        listView1.Items[i].Remove();
                    }
                }
                return;
            }

            for(int i=listView1.Items.Count-1;i>=0;i--)
            {
                string strDesc = listView1.Items[i].SubItems[4].Text;
                if (flterId.Contains(strDesc))
                {
                    listView1.Items[i].Remove();
                }
            }
        }

        private void Copy_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(listView1.FocusedItem.SubItems[5].Text);
        }

        private void Clear_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }

        private void Send_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsi = (ToolStripMenuItem)sender;
            if(tsi.Checked)
            {
                tsi.CheckState = CheckState.Unchecked;
            }
            else
            {
                tsi.CheckState = CheckState.Checked;
            }
        }

        private void Recv_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsi = (ToolStripMenuItem)sender;
            if (tsi.Checked)
            {
                tsi.CheckState = CheckState.Unchecked;
            }
            else
            {
                tsi.CheckState = CheckState.Checked;
            }
        }

        private void SendToGame_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendForm sendForm = new SendForm();
            string strFd = this.listView1.FocusedItem.SubItems[3].Text;
            int fd = Convert.ToInt32(strFd);
            string strData = this.listView1.FocusedItem.SubItems[5].Text;
            byte[] hexData = RemoteCallMoblie.strToToHexByte(strData);
            sendForm.initData(this, fd, hexData);
            sendForm.ShowDialog();
        }

        public void SendToGame(int client_fd, byte[] hexData)
        {
            remoteCall.sendGamePack(client_fd, hexData);
        }

        private void ProtoDetailForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(remoteCall != null)
                remoteCall.Disconnecnt();
        }

        private void luaButton_Click(object sender, EventArgs e)
        {
            SendForm sendForm = new SendForm();
            if (this.listView1.SelectedItems == null || this.listView1.SelectedItems.Count <= 0)
            {
                logTextBox.Text = "选中一条协议再试";
                return;
            }
            string strData = this.listView1.FocusedItem.SubItems[5].Text;
            byte[] hexByte = RemoteCallMoblie.strToToHexByte(strData);
            string protoName = this.listView1.FocusedItem.SubItems[4].Text;

            StringBuilder sb = new StringBuilder(0x4096);
            if (ProtoExt.script_run(hexByte, hexByte.Length, sb, sb.Capacity))
            {
                string strFd = this.listView1.FocusedItem.SubItems[3].Text;
                int fd = Convert.ToInt32(strFd);
                byte[] sendByte = RemoteCallMoblie.strToToHexByte(sb.ToString());
                SendToGame(fd, sendByte);
                logTextBox.Text = "发送完毕";
            }
            else
            {
                logTextBox.Text = sb.ToString();
            }
        }

        public class ProtoExt
        {
            [DllImport("ProtoExt.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
            public extern static void explain_proto(string protoName, byte[] data, int size_t, StringBuilder outStr, int outLen);

            [DllImport("ProtoExt.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
            public extern static bool script_run(byte[] data, int size_t, StringBuilder outStr, int outLen);
        }

        private void 不显示ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string strDesc = listView1.FocusedItem.SubItems[4].Text;
            flterId.Add(strDesc);
            updataListView();
        }

        private void 仅显示ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string strDesc = listView1.FocusedItem.SubItems[4].Text;
            onlyShow.Add(strDesc);
            updataListView();
        }
    }
}