﻿namespace ProtocolTool
{
    partial class ProtoDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProtoDetailForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listView1 = new System.Windows.Forms.ListView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Copy_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Filter_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Clear_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SendToGame_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.hexBox1 = new Be.Windows.Forms.HexBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.explainTextBox = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.luaButton = new System.Windows.Forms.Button();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.wenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关闭ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.显示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Send_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Recv_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.过滤ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.搜索ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.仅显示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.不显示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.进制数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.字符串ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.不显示ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.仅显示ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(655, 27);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton1.Text = "开始抓包";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton2.Text = "暂停抓包";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 52);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(655, 353);
            this.splitContainer1.SplitterDistance = 149;
            this.splitContainer1.TabIndex = 1;
            // 
            // listView1
            // 
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(655, 149);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Copy_ToolStripMenuItem,
            this.Filter_ToolStripMenuItem,
            this.Clear_ToolStripMenuItem,
            this.SendToGame_ToolStripMenuItem,
            this.Delete_ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 114);
            // 
            // Copy_ToolStripMenuItem
            // 
            this.Copy_ToolStripMenuItem.Name = "Copy_ToolStripMenuItem";
            this.Copy_ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.Copy_ToolStripMenuItem.Text = "复制";
            this.Copy_ToolStripMenuItem.Click += new System.EventHandler(this.Copy_ToolStripMenuItem_Click);
            // 
            // Filter_ToolStripMenuItem
            // 
            this.Filter_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.不显示ToolStripMenuItem1,
            this.仅显示ToolStripMenuItem1});
            this.Filter_ToolStripMenuItem.Name = "Filter_ToolStripMenuItem";
            this.Filter_ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.Filter_ToolStripMenuItem.Text = "过滤";
            // 
            // Clear_ToolStripMenuItem
            // 
            this.Clear_ToolStripMenuItem.Name = "Clear_ToolStripMenuItem";
            this.Clear_ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.Clear_ToolStripMenuItem.Text = "清空";
            this.Clear_ToolStripMenuItem.Click += new System.EventHandler(this.Clear_ToolStripMenuItem_Click);
            // 
            // SendToGame_ToolStripMenuItem
            // 
            this.SendToGame_ToolStripMenuItem.Name = "SendToGame_ToolStripMenuItem";
            this.SendToGame_ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.SendToGame_ToolStripMenuItem.Text = "发送";
            this.SendToGame_ToolStripMenuItem.Click += new System.EventHandler(this.SendToGame_ToolStripMenuItem_Click);
            // 
            // Delete_ToolStripMenuItem
            // 
            this.Delete_ToolStripMenuItem.Name = "Delete_ToolStripMenuItem";
            this.Delete_ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.Delete_ToolStripMenuItem.Text = "删除";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(655, 200);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.hexBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(647, 174);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "16进制数据";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // hexBox1
            // 
            this.hexBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hexBox1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.hexBox1.LineInfoVisible = true;
            this.hexBox1.Location = new System.Drawing.Point(3, 3);
            this.hexBox1.Name = "hexBox1";
            this.hexBox1.ShadowSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(60)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
            this.hexBox1.Size = new System.Drawing.Size(641, 168);
            this.hexBox1.StringViewVisible = true;
            this.hexBox1.TabIndex = 0;
            this.hexBox1.UseFixedBytesPerLine = true;
            this.hexBox1.VScrollBarVisible = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.explainTextBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(647, 174);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "协议树";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // explainTextBox
            // 
            this.explainTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.explainTextBox.Location = new System.Drawing.Point(3, 3);
            this.explainTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.explainTextBox.Multiline = true;
            this.explainTextBox.Name = "explainTextBox";
            this.explainTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.explainTextBox.Size = new System.Drawing.Size(641, 168);
            this.explainTextBox.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(647, 174);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Log";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // luaButton
            // 
            this.luaButton.Location = new System.Drawing.Point(5, 2);
            this.luaButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.luaButton.Name = "luaButton";
            this.luaButton.Size = new System.Drawing.Size(93, 34);
            this.luaButton.TabIndex = 1;
            this.luaButton.Text = "脚本发送";
            this.luaButton.UseVisualStyleBackColor = true;
            this.luaButton.Click += new System.EventHandler(this.luaButton_Click);
            // 
            // logTextBox
            // 
            this.logTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logTextBox.Location = new System.Drawing.Point(0, 0);
            this.logTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logTextBox.Size = new System.Drawing.Size(541, 174);
            this.logTextBox.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wenToolStripMenuItem,
            this.显示ToolStripMenuItem,
            this.过滤ToolStripMenuItem,
            this.搜索ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(655, 25);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // wenToolStripMenuItem
            // 
            this.wenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开ToolStripMenuItem,
            this.保存ToolStripMenuItem,
            this.关闭ToolStripMenuItem});
            this.wenToolStripMenuItem.Name = "wenToolStripMenuItem";
            this.wenToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.wenToolStripMenuItem.Text = "文件";
            // 
            // 打开ToolStripMenuItem
            // 
            this.打开ToolStripMenuItem.Name = "打开ToolStripMenuItem";
            this.打开ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.打开ToolStripMenuItem.Text = "打开";
            this.打开ToolStripMenuItem.Click += new System.EventHandler(this.打开ToolStripMenuItem_Click);
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.保存ToolStripMenuItem.Text = "保存";
            // 
            // 关闭ToolStripMenuItem
            // 
            this.关闭ToolStripMenuItem.Name = "关闭ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.关闭ToolStripMenuItem.Text = "关闭";
            // 
            // 显示ToolStripMenuItem
            // 
            this.显示ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Send_ToolStripMenuItem,
            this.Recv_ToolStripMenuItem});
            this.显示ToolStripMenuItem.Name = "显示ToolStripMenuItem";
            this.显示ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.显示ToolStripMenuItem.Text = "显示";
            // 
            // Send_ToolStripMenuItem
            // 
            this.Send_ToolStripMenuItem.Checked = true;
            this.Send_ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Send_ToolStripMenuItem.Name = "Send_ToolStripMenuItem";
            this.Send_ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.Send_ToolStripMenuItem.Text = "发包";
            this.Send_ToolStripMenuItem.Click += new System.EventHandler(this.Send_ToolStripMenuItem_Click);
            // 
            // Recv_ToolStripMenuItem
            // 
            this.Recv_ToolStripMenuItem.Checked = true;
            this.Recv_ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Recv_ToolStripMenuItem.Name = "Recv_ToolStripMenuItem";
            this.Recv_ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.Recv_ToolStripMenuItem.Text = "收包";
            this.Recv_ToolStripMenuItem.Click += new System.EventHandler(this.Recv_ToolStripMenuItem_Click);
            // 
            // 过滤ToolStripMenuItem
            // 
            this.过滤ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.仅显示ToolStripMenuItem,
            this.不显示ToolStripMenuItem});
            this.过滤ToolStripMenuItem.Name = "过滤ToolStripMenuItem";
            this.过滤ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.过滤ToolStripMenuItem.Text = "过滤";
            // 
            // 搜索ToolStripMenuItem
            // 
            this.搜索ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.进制数据ToolStripMenuItem,
            this.字符串ToolStripMenuItem});
            this.搜索ToolStripMenuItem.Name = "搜索ToolStripMenuItem";
            this.搜索ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.搜索ToolStripMenuItem.Text = "搜索";
            // 
            // 仅显示ToolStripMenuItem
            // 
            this.仅显示ToolStripMenuItem.Name = "仅显示ToolStripMenuItem";
            this.仅显示ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.仅显示ToolStripMenuItem.Text = "仅显示";
            // 
            // 不显示ToolStripMenuItem
            // 
            this.不显示ToolStripMenuItem.Name = "不显示ToolStripMenuItem";
            this.不显示ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.不显示ToolStripMenuItem.Text = "不显示";
            // 
            // 进制数据ToolStripMenuItem
            // 
            this.进制数据ToolStripMenuItem.Name = "进制数据ToolStripMenuItem";
            this.进制数据ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.进制数据ToolStripMenuItem.Text = "16进制数据";
            // 
            // 字符串ToolStripMenuItem
            // 
            this.字符串ToolStripMenuItem.Name = "字符串ToolStripMenuItem";
            this.字符串ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.字符串ToolStripMenuItem.Text = "字符串";
            // 
            // 不显示ToolStripMenuItem1
            // 
            this.不显示ToolStripMenuItem1.Name = "不显示ToolStripMenuItem1";
            this.不显示ToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.不显示ToolStripMenuItem1.Text = "不显示";
            this.不显示ToolStripMenuItem1.Click += new System.EventHandler(this.不显示ToolStripMenuItem1_Click);
            // 
            // 仅显示ToolStripMenuItem1
            // 
            this.仅显示ToolStripMenuItem1.Name = "仅显示ToolStripMenuItem1";
            this.仅显示ToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.仅显示ToolStripMenuItem1.Text = "仅显示";
            this.仅显示ToolStripMenuItem1.Click += new System.EventHandler(this.仅显示ToolStripMenuItem1_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.luaButton);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.logTextBox);
            this.splitContainer2.Size = new System.Drawing.Size(647, 174);
            this.splitContainer2.SplitterDistance = 102;
            this.splitContainer2.TabIndex = 2;
            // 
            // ProtoDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 405);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ProtoDetailForm";
            this.Text = "ProtoDetailForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProtoDetailForm_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem wenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打开ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关闭ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private Be.Windows.Forms.HexBox hexBox1;
        private System.Windows.Forms.ToolStripMenuItem Copy_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Filter_ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Clear_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 显示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Send_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Recv_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SendToGame_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Delete_ToolStripMenuItem;
        private System.Windows.Forms.TextBox explainTextBox;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.Button luaButton;
        private System.Windows.Forms.ToolStripMenuItem 不显示ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 仅显示ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 过滤ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 仅显示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 不显示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 搜索ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 进制数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 字符串ToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer2;
    }
}