﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Be.Windows.Forms;

namespace ProtocolTool
{
    class DynamicByteProvider : IByteProvider
    {
        byte[] _pData;
        long _length;

        private static byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public static string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    returnStr += bytes[i].ToString("X2");
                }
            }
            return returnStr;
        }

        public DynamicByteProvider(string data)
        {
            _pData = strToToHexByte(data);
            _length = _pData.Length;
        }

        #region IByteProvider Members
        public long Length
        {
            get
            {
                return this._length;
            }
        }

        public event EventHandler Changed;
        public event EventHandler LengthChanged;

        public void ApplyChanges()
        {
            throw new NotImplementedException();
        }

        public void DeleteBytes(long index, long length)
        {
            throw new NotImplementedException();
        }

        public bool HasChanges()
        {
            throw new NotImplementedException();
        }

        public void InsertBytes(long index, byte[] bs)
        {
            throw new NotImplementedException();
        }

        public byte ReadByte(long index)
        {
            return _pData[index];
        }

        public bool SupportsDeleteBytes()
        {
            return false;
        }

        public bool SupportsInsertBytes()
        {
            return false;
        }

        public bool SupportsWriteByte()
        {
            return false;
        }

        public void WriteByte(long index, byte value)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
