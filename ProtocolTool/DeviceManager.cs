﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Text.RegularExpressions;
using System.Collections;

namespace ProtocolTool
{
    class DeviceManager
    {
        private Socket clientSocket;
        private string strIpaddr;

        private const int DATA_TRANSMIT_BEGIN = 0X00000001;         // begin transmition
        private const int DATA_TRANSMIT = 0X00000002;               // transmit data to pc
        private const int DATA_TRANSMIT_END = 0X00000003;           // end transmition 
        private const int SHUTDOWN_SERVER = 0X00000004;             // shutdown the thread_main
        private const int EXIT_CLIENT = 0X00000005;                 // exit from current thread sub 
        private const int TRANS_CMD_ECHO = 0X00000006;              // echo what the thread has done for our commmand from pc
        private const int GET_PROC_LIST = 0X00000007;               // get process list
        private const int INJECT_PROCESS = 0X00000008;              // inject to a specified process

        private const int HEADER_SIZE = 16;

        private byte[] recvBuffer = new byte[1024 * 2];
        private int recvLen = 0;
        private int index = 0;

        public class ProcessInfo
        {
            public string name;
            public string pid;
        }

        public DeviceManager(string strIpaddr)
        {
            this.strIpaddr = strIpaddr;
        }
        public bool connectDevice()
        {
            //return true;
            string[] strs = strIpaddr.Split(':');
            IPAddress ip = IPAddress.Parse(strs[0]);
            this.clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                int port;
                int.TryParse(strs[1], out port);
                clientSocket.Connect(new IPEndPoint(ip, port)); //配置服务器IP与端口  
            }
            catch
            {
                return false;
            }
            return true;
        }

        public int disconnectDevice()
        {
            clientSocket.Shutdown(SocketShutdown.Both);
            clientSocket.Close();
            clientSocket = null;
            return 1;
        }

        public byte[] peekPack()
        {
            byte[] retBuf;
            while(true)
            {
                if(index == 0 ||
                    index == recvLen)
                {
                    recvLen = clientSocket.Receive(recvBuffer);
                    index = 0;
                }
                int imsg_type= BitConverter.ToInt32(recvBuffer, index);
                index += 4;             //msg_type
                int imsg_len = BitConverter.ToInt32(recvBuffer, index);
                index += 4;
                retBuf = new byte[imsg_len + 8];

                Array.Copy(recvBuffer, index-8, retBuf, 0, 8);

                //数据不够，需要继续收包
                if (imsg_len > recvLen - index)
                {
                    int curlen = recvLen - index;
                    int needLen = imsg_len - curlen;
                    Array.Copy(recvBuffer, index, retBuf, 8, curlen);
                    do
                    {
                        recvLen = clientSocket.Receive(recvBuffer);
                        if (recvLen >= needLen)
                        {
                            Array.Copy(recvBuffer, 0, retBuf, curlen, needLen);
                            index = recvLen - needLen;
                            needLen = 0;
                        } 
                        else
                        {
                            Array.Copy(recvBuffer, 0, retBuf, curlen, recvLen);
                            curlen += recvLen;
                            needLen -= recvLen;
                        }
                    } while (needLen > 0);
                }
                else
                {
                    Array.Copy(recvBuffer, index, retBuf, 8, imsg_len);
                    index += imsg_len;
                }
                return retBuf;
            }
        }

        public byte[] getData()
        {
            byte[] retData = null;
            byte[] recvData;
            bool first = false;
            int index = 0;
            while (true)
            {
                recvData = peekPack();
                int imsg_type = BitConverter.ToInt32(recvData, 0);
                if (imsg_type == DATA_TRANSMIT_BEGIN)
                {
                    first = true;
                }
                if (imsg_type == DATA_TRANSMIT)
                {
                    int imsg_len = BitConverter.ToInt32(recvData, 4);
                    if(first)
                    {
                        int imsg_sub_type = BitConverter.ToInt32(recvData, 8);
                        int ibody_len = BitConverter.ToInt32(recvData, 12);
                        retData = new byte[ibody_len+16];
                        Array.Copy(recvData, 8, retData, index, imsg_len);
                        index += imsg_len;
                        first = false;
                    }
                    else
                    {
                        Array.Copy(recvData, 8, retData, index, imsg_len);
                        index += imsg_len;
                    }
                }
                if (imsg_type == DATA_TRANSMIT_END)
                {
                    break;
                }
            }
            return retData;
        }

        public ProcessInfo[] getAllPorc()
        {
            //
            byte[] recvData;
            byte[] tmp;
            byte[] msg_data = new byte[4 + 4 + 8];
            
            tmp = BitConverter.GetBytes(GET_PROC_LIST);
            tmp.CopyTo(msg_data, 0);
            clientSocket.Send(msg_data);
            while(true)
            {
                recvData = getData();
                int imsg_sub_type = BitConverter.ToInt32(recvData, 0);
                if (imsg_sub_type == GET_PROC_LIST)
                    break;
            }
            string strProcess = Encoding.Default.GetString(recvData, 16, recvData.Length - 16);
            string[] strprocs = strProcess.Split('|');
            string pattern = "\\[(.*)\\](.*)";
            Regex reg = new Regex(pattern);
            ProcessInfo[] procs = new ProcessInfo[strprocs.Length-1];
            for(int i=0;i<strprocs.Length-1;i++)
            {
                ProcessInfo info = new ProcessInfo();
                Match match = reg.Match(strprocs[i]);
                info.pid = match.Groups[1].Value;
                info.name = match.Groups[2].Value;
                procs[i] = info;
            }
            return procs;
        }

        public bool traceProcess(int pid)
        {
            byte[] msg_data = new byte[16];
            byte[] tmp;
            tmp = BitConverter.GetBytes(INJECT_PROCESS);
            tmp.CopyTo(msg_data, 0);
            tmp = BitConverter.GetBytes(pid);
            tmp.CopyTo(msg_data, 8);
            clientSocket.Send(msg_data);
            //byte[] recvData = getData();
            return true;
        }
    }
}
