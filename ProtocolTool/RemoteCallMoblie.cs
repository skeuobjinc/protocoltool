﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace ProtocolTool
{
    class RemoteCallMoblie : RemoteCall
    {
        private ProtoDetailForm _form;
        private Socket client;
        private Thread th;
        private byte[] recvBuffer = new byte[HEADER_SIZE];
        private byte[] packBuffer;
        private int packIndex;
        private int curImsgType;
        private int curClientFd;
        private int curSize;
        private string curIPAddr;

        private const int START_SEND_TO_PC = 0X00000001;        // start hijacking
        private const int STOP_SEND_TO_PC = 0X00000002;         // stop hijacking 
        private const int EXIT_CLIENT = 0X00000003;             // exit from current thread sub
        private const int SHUTDOWN_SERVER = 0X00000004;         // shutdown the thread_main
        private const int TRANS_CMD_ECHO = 0X00000005;          // echo what the thread has done for our commmand from pc
        private const int TRANS_SEND_DATA = 0X00000006;         // transmit the hijacked-send data to pc 
        private const int TRANS_RECV_DATA = 0X00000007;         // transmit the hijacked-received data to pc
        private const int PROXY_SEND_DATA = 0X00000008;         // proxy mobile to send msg to target server

        private const int HEADER_SIZE = 44;

        public RemoteCallMoblie(ProtoDetailForm mainForm)
        {
            this._form = mainForm;
        }
        
        private int RecvPack(byte[] buffer, int size, out int imsgType, out int clientFd, out string strAddr, out int needSize)
        {
            return RecvPack(buffer, 0, size, out imsgType, out clientFd, out strAddr, out needSize);
        }
        private int RecvPack(byte[] buffer, int offset, int size, out int imsgType, out int clientFd, out string strAddr, out int needSize)
        {
            int recvLengh = 0;

            //读取一个游戏包
            do
            {
                if (packBuffer == null || packBuffer.Count() == packIndex)
                {

                    recvLengh = client.Receive(recvBuffer, HEADER_SIZE, SocketFlags.None);
                    if (recvLengh != HEADER_SIZE)
                    {
                        imsgType = 0;
                        clientFd = 0;
                        strAddr = "";
                        needSize = 0;
                        return 0;
                    }
                    curImsgType = BitConverter.ToInt32(recvBuffer, 0);
                    int ibodyLen = BitConverter.ToInt32(recvBuffer, 4);
                    curClientFd = BitConverter.ToInt32(recvBuffer, 8);
                    curIPAddr = Encoding.Default.GetString(recvBuffer, 12, 32);
                    packBuffer = new byte[ibodyLen];
                    recvLengh = client.Receive(packBuffer, ibodyLen, SocketFlags.None);
                    if (recvLengh < ibodyLen)
                    {
                        int index = recvLengh;
                        do
                        {
                            recvLengh = client.Receive(packBuffer, index, ibodyLen - index, SocketFlags.None);
                            index += recvLengh;
                        } while (index < ibodyLen);
                    }
                    curSize = ibodyLen;
                    packIndex = 0;
                    //如果是80端口且接受buffer小于当前buffer就直接返回
                    if(curIPAddr.Contains("80"))
                    {
                        if(size < curSize)
                        {
                            imsgType = 0;
                            clientFd = 0;
                            strAddr = "";
                            needSize = curSize;
                            return -1;
                        }
                        else
                        {
                            imsgType = curImsgType;
                            clientFd = curClientFd;
                            strAddr = curIPAddr;
                            needSize = 0;
                            return curSize;
                        }
                    }
                    //如果不是游戏数据就继续收包
                    if (curImsgType == TRANS_CMD_ECHO)
                    {
                        packBuffer = null;
                        continue;
                    }
                }
                if (size >= curSize)
                {
                    Array.Copy(packBuffer, packIndex, buffer, offset, curSize);
                    packIndex += curSize;
                    imsgType = curImsgType;
                    clientFd = curClientFd;
                    strAddr = curIPAddr;
                    needSize = 0;
                    return curSize;
                }
                else
                {
                    Array.Copy(packBuffer, packIndex, buffer, offset, size);
                    packIndex += size;
                    curSize -= size;
                    imsgType = curImsgType;
                    clientFd = curClientFd;
                    strAddr = curIPAddr;
                    needSize = 0;
                    return size;
                }
            } while (true);
        }

        private void ThreadMethod()
        {
            int recvLengh = 0;
            byte[] head_buff = new byte[8];
            while(true)
            {
                int imsgType;
                int clientFd;
                string strAddr;
                int needSize;
                byte[] retBuf;
                recvLengh = RecvPack(head_buff, 8, out imsgType, out clientFd, out strAddr, out needSize);
                if(recvLengh == -1)
                {
                    retBuf = new byte[needSize];
                    int rlen = RecvPack(retBuf, needSize, out imsgType, out clientFd, out strAddr, out needSize);
                }
                else if(recvLengh != 8)
                {
                    //erro
                    continue;
                }
                else
                {
                    int len = BitConverter.ToInt32(head_buff, 0);
                    retBuf = new byte[len + 8];
                    head_buff.CopyTo(retBuf, 0);

                    recvLengh = RecvPack(retBuf, 8, len, out imsgType, out clientFd, out strAddr, out needSize);
                    if (recvLengh < len)
                    {
                        int index = recvLengh;
                        do
                        {
                            recvLengh = RecvPack(retBuf, index, len - index, out imsgType, out clientFd, out strAddr, out needSize);
                            index += recvLengh;
                        } while (index < len);
                    }
                }
                string strData = byteToHexStr(retBuf);
                switch (imsgType)
                {
                    case TRANS_CMD_ECHO:
                        break;
                    case TRANS_RECV_DATA:
                        _form.OutText(1, clientFd, strAddr, strData);
                        break;
                    case TRANS_SEND_DATA:
                        _form.OutText(0, clientFd, strAddr, strData);
                        break;
                    default:
                        break;
                }
            }
        }

        public override bool Connecnt(int port)
        {
            IPAddress ip = IPAddress.Parse("127.0.0.1");
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                client.Connect(new IPEndPoint(ip, port)); //配置服务器IP与端口  
                th = new Thread(new ThreadStart(ThreadMethod));
                th.Start();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public override bool Disconnecnt()
        {
            th.Abort();
            client.Shutdown(SocketShutdown.Both);
            client.Close();
            return true;
        }

        public override bool startSniff()
        {
            byte[] msg_data = new byte[HEADER_SIZE];
            byte[] tmp;
            tmp = BitConverter.GetBytes(START_SEND_TO_PC);
            tmp.CopyTo(msg_data, 0);
            client.Send(msg_data);
            return true;
        }

        public override bool pauseSniff()
        {
            byte[] msg_data = new byte[HEADER_SIZE];
            byte[] tmp;
            tmp = BitConverter.GetBytes(STOP_SEND_TO_PC);
            tmp.CopyTo(msg_data, 0);
            client.Send(msg_data);
            return true;
        }

        public override bool sendGamePack(int clientFd, byte[] hexData)
        {
            int body_len = hexData.Count();
            byte[] msg_data = new byte[HEADER_SIZE + body_len];
            byte[] tmp;
            tmp = BitConverter.GetBytes(PROXY_SEND_DATA);
            tmp.CopyTo(msg_data, 0);
            tmp = BitConverter.GetBytes(body_len);
            tmp.CopyTo(msg_data, 4);
            tmp = BitConverter.GetBytes(clientFd);
            tmp.CopyTo(msg_data, 8);
            hexData.CopyTo(msg_data, HEADER_SIZE);
            client.Send(msg_data);
            return true;
        }

        public override bool sendGamePack(int clientFd, string strData)
        {
            byte[] body_data = strToToHexByte(strData);
            return sendGamePack(clientFd, body_data);
        }
    }
}
