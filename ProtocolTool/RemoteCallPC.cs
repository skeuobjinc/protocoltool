﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProtocolTool
{
    class Packet
    {
        public enum PacketType {
            PacketSend = 0,
            PacketRecv = 1,
        };

        public PacketType type;
        public string ip;
        public int sock;
        public byte[] body;
    }

    class RemoteCallPC : RemoteCall
    {
        private const int HEADER_SIZE = 18;
        private ProtoDetailForm _form;
        private Socket client;
        private Thread th;
        private bool isSniff = false;

        public RemoteCallPC (ProtoDetailForm mainForm)
        {
            this._form = mainForm;
        }

        private bool RecvPack(out Packet packet)
        {
            packet = new Packet();
            try {
                byte[] header = new byte[HEADER_SIZE];
                int ret = client.Receive(header);
                if (ret != HEADER_SIZE)
                {
                    return false;
                }

                int bodyLen = BitConverter.ToInt32(header, 0);
                packet.type = (Packet.PacketType)BitConverter.ToInt32(header, 4);

                uint ip = BitConverter.ToUInt32(header, 8);     // ip
                uint port = BitConverter.ToUInt16(header, 10);     // port
                int sock = BitConverter.ToInt32(header, 12);     // sock

                packet.ip = "127.0.0.1:123";     // TODO : 以后写
                packet.sock = sock;

                byte[] body = new byte[bodyLen];
                ret = client.Receive(body);
                if (ret != bodyLen)
                {
                    return false;
                }

                packet.body = body;
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        private void ThreadMethod()
        {
            while (true)
            {
                if (!isSniff)
                {
                    Thread.Sleep(0);
                    continue;
                }

                Packet packet;
                if (!RecvPack(out packet))
                {
                    continue;
                }

                string strData = byteToHexStr(packet.body);
                switch (packet.type)
                {
                    case Packet.PacketType.PacketSend:
                        _form.OutText(0, packet.sock, packet.ip, strData);
                        break;
                    case Packet.PacketType.PacketRecv:
                        _form.OutText(1, packet.sock, packet.ip, strData);
                        break;
                    default:
                        break;
                }
            }
        }

        public override bool Connecnt(int port)
        {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            try
            {
                IPAddress addr = IPAddress.Parse("127.0.0.1");
                client.Bind(new IPEndPoint(addr, 20181));
                th = new Thread(new ThreadStart(ThreadMethod));
                th.Start();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool Disconnecnt()
        {
            th.Abort();
            client.Shutdown(SocketShutdown.Both);
            client.Close();
            return true;
        }

        public override bool startSniff()
        {
            isSniff = true;
            return true;
        }

        public override bool pauseSniff()
        {
            isSniff = false;
            return true;
        }

        public override bool sendGamePack(int client_fd, byte[] hexData)
        {
            return false;
        }

        public override bool sendGamePack(int client_fd, string strData)
        {
            return false;
        }
    }
}
