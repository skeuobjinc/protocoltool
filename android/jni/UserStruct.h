//#include <iostream>
#include <assert.h>
#include "GameDefine.h"
// [커뮤니티 기능 추가]

#ifndef __USERSTRUCT_H__
#define __USERSTRUCT_H__

//#include "ResultCode.h"

/************************************************************************/
/* INCLUDES                                                             */
/************************************************************************/


#pragma pack( push, 1 )

/************************************************************************/
/* DEFINITIONS                                                          */
/************************************************************************/
//#define MAX_AVATARLAYER 26


#define MAX_MAC_ADDRESS_LEN			32			// Mac Address
#define MAX_CPU_DESC				64			// CPU 설명 정보
#define MAX_TOKEN_SIZE				256			// Token Size
#define MAX_NATION_CODE				4			// 국가
#define MAX_LANGUAGE_CDOE			4			// 언어 코드
#define MAX_SESSIONID_LEN			32			// 텐센트 세션ID
#define MAX_SESSIONTYPE_LEN			32			// 텐센트 세션Type
#define MAX_PF_LEN					64			// 텐센트 pf
#define MAX_PFKEY_LEN				32			// 텐센트 pfKey
#define MAX_BILLNO_LEN				40			// 텐센트 billno
#define MAX_UUID_LEN				128			// 텐센트 UUID
#define MAX_LINE_REQSTR				500			// 라인 로그인 인증용


#define WH_FIRST		0			// 로그인하기전
#define WH_LOGIN		1			// 로그인중
#define WH_TITLE		2			// 타이틀 화면(WH_CHAN 랑 같음.) 클라이언트용
#define WH_CHAN			2			// 타이틀 화면(WH_TITLE 랑 같음.) 서버용
#define WH_LOBY			3			// 대기실
#define WH_GAME			4			// 게임중
#define WH_TERRITORY	10			// 내 영토 클라이언트만 사용


#define			MAX_COMMAND_STR			4096


//- lorddan(2013/04/04) - 
#define			MAX_IP_STR			20
#define			MAX_SERVER_NAME		20
#define			MAX_SERVER_ID		20
#define			MAX_SERVER_PW		20
#define			MAX_DOMAIN_NAME		128	// NO_DB 작업할때 IP 20으로 되어 있어 DOMAIN_NAME 짤려서 추가 밑 확장 함..	

/// 라인용
#define MAX_LINE_TITLE_LEN 300
#define MAX_LINE_MEMO_LEN 100
#define MAX_LINE_SVCINF 30

//////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////                     마스터 서버용 구조체
//////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// 게임서버 상태 정보
typedef struct
{
	char	WorkState;			// 서버 가동 상태(0:정상 1:점검중 2:종료중)
} Ms_ServerStatus;

typedef struct
{
	int				SC;			// Game Server의 Server Code
	Ms_ServerStatus	Status;
}Ms_GameServerStatus;

typedef struct
{
	enum
	{
		RUN_SERVER = 0,
		MAINTENENCE_SERVER,
		DIE_SERVER,
	};
	char	WorkState;			// 서버 가동 상태(0:정상 1:점검중 2:종료중)
	int		nTotUser;
} SERVER_STATUS_TYP;

////////////////////////////////////////////////////////////////////////////////////////
// 게임서버 정보
typedef struct
{
	int				SNum;				// 서버 인식 번호(로그인 서버 번호)

	short			GameCode;			// 게임 코드
	unsigned short	ServerCode;			// 서버 코드
	char			ServerName[MAX_SERVER_NAME];		// 서버 이름
	char			IP[MAX_DOMAIN_NAME];				// 서버 IP
	int				Port;				// 서버 포트 번호
	int				TotUser;			// 서버에 접속한 유저 수
	Ms_ServerStatus	Status;		// 게임서버 상태 정보
} Ms_ServerInfo;

////////////////////////////////////////////////////////////////////////////////////////
// 게임 서버의 유저 정보
typedef struct
{					
	__int64		userUID;
	
	int			snsType;									// 계정 인증 타입
	char		snsUID[MAX_SNS_UID_LEN + 1];
	
	int			UNum;				// 유저 번호
	short		GroupNo;			// 접속한 그룹 번호
	short		ChanNo;				// 접속한 통합 채널 번호
	short		SNum;				// 접속한 서버 번호
	short		nState;				// 접속 상태
} Ms_UserInfo;

////////////////////////////////////////////////////////////////////////////////////////
// 게임채널 정보
typedef struct
{
	short	GroupNo;			// 그룹 번호
	short	ChanNo;				// 채널 번호(통합된 채널 번호)
	short	MaxUser;			// 최대 수용 인원
	short	NowUser;			// 현재 인원
	short	ResUser;			// 입장 예약중인 인원(마스터 서버에서 판단)
	char	WorkState;			// 채널 상태(0:정상 1:점검중 2:종료중)
} Ms_ChannelInfo;

////////////////////////////////////////////////////////////////////////////////////////
// 업데이트용 게임채널 정보
typedef struct
{
	short	GroupNo;			// 그룹 번호
	short	ChanNo;				// 채널 번호(통합된 채널 번호)
	short	NowUser;			// 현재 인원
	short	ResUser;			// 입장 예약중인 인원(마스터 서버에서 판단)
	char	WorkState;			// 채널 상태(0:정상 1:점검중 2:종료중)
} Ms_SmallChanInfo;

////////////////////////////////////////////////////////////////////////////////////////
// 채널 업데이트 정보
typedef struct
{
	int		TotNewChan;			// 새로운 채널 정보 개수
	int		TotModChan;			// 수정될 채널 정보 개수
	int		TotDelChan;			// 삭제될 채널 정보 개수
} Ms_ChanUpdateInfo;



//- lorddan(2013/09/26) - strcuture size 변경 할 것
typedef struct
{
	int			nUNum;			// 의뢰인 UNum
	__int64		nUserUID;		// 의뢰인 ID
	char		nGameType;
	int			nSeedType;
	int			nMapKind;
	int			nMaxUser;
	int			nVersion;
}Ms_MatchInfo;

typedef struct
{
	int				nServerNum;
	Ms_MatchInfo	INFO;
}Mt_MatchInfo;

typedef struct
{
	Ms_MatchInfo	INFO;			// Matching 요청 기본정보

	bool			bCreateRoom;	// 0 채널 이동 방 입장, 1. 방생성
	__int16			nGroupNum;		// 그룹 넘버
	__int16			nChannelNum;	// 채널 넘버
	__int16			nRoomNum;		// 방번호
	__int64			nUniqueKey;		// Room Unique Key
}Ms_Result_MatchInfo;

typedef struct
{
	int						nServerNum;
	Ms_Result_MatchInfo		RESULT;
}Mt_Result_MatchInfo;

typedef struct
{
	bool		bDeleteRoom;			// 1: 룸 삭제

	char		nGameType;				// 게임 타입.
	int			nSeedType;
	int			nMapKind;

	__int16		_currentUserCount;		// 현재 룸 인원수
	__int16		_maxUserCount;			// 최대 인원수

	__int16		_groupNum;				// 그룹 넘버
	__int16		_channelNum;			// 채널 넘버
	__int16		_roomNum;				// 방번호
	__int64		nUniqueKey;				// Room UniqueKey
	int			nVersion;				//방 생성 시점에 정해지는 방의 버전(방장의 클라이언트 버전 정보를 따라간다)
}Ms_MatchRoomInfo;

////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
//- lorddan(2013/04/04) - 
#define			MAX_IP_STR			20
#define			MAX_SERVER_NAME		20
#define			MAX_SERVER_ID		20
#define			MAX_SERVER_PW		20

// 게임서버 로그인 정보
typedef struct
{
	char	Account[MAX_SERVER_ID];
	char	Password[MAX_SERVER_PW];
	Ms_ServerInfo  SvInfo;
} Ms_ServerLoginInfo;

////////////////////////////////////////////////////////////////////////////////////////
// 마스터 서버 상태 정보
typedef struct
{
	bool	bAccessDeny;			// 접속 거부중인가?
	bool	bUseSessionManagement;	// 세션 관리 기능을 사용하고 있는 상태인가?
} Ms_MasterStatus;

////////////////////////////////////////////////////////////////////////////////////////
// 데이터 센터 정보
typedef struct
{
	int		totalUser;			// 현재 접속 인원
	int		maxUser;			// 최대 접속 인원
	int		WorkState;			// 서버 상태
} DC_SmallServerInfo;


typedef struct
{
	short		GameCode;			// 게임 코드
	unsigned short	ServerCode;			// 서버 코드
	char	ServerName[MAX_SERVER_NAME];		// 서버 이름
	char	IP[MAX_SERVER_ID];				// 서버 IP
	int		Port;				// 서버 포트 번호	
	DC_SmallServerInfo	smallInfo;
} DC_ServerInfo;



////////////////////////////////////////////////////////////////////////////////////////
// 그룹 정보
typedef struct
{
	short	GroupNo;				// 그룹 번호
	char	GroupName[24];			// 그룹 이름
} Ms_GroupInfo;

////////////////////////////////////////////////////////////////////////////////////////
// 사용자 채널 변경 정보
enum
{
	USERCHANGE_ENTER		= 1,			// LOGIN
	USERCHANGE_DISCONN		= 2,			// USER DISCONNECT
	USERCHANGE_EXIT			= 3,			// USER REAL EXIT
	USERCHANGE_RECONN		= 4,			// USER RECONNECT
	USERCHANGE_MOVEEXIT		= 5,			// 채널 이동을 위한 종료
};


typedef struct
{
	char		Kind;			// 정보의 종류(1:채널 입장 유저, 2:채널 변경 유저, 3:채널 퇴장 유저)
	__int64		SeqNo;			// 인식용 시퀀스 번호
	short		ServerTotUser;	// 게임서버 전체의 유저수
	short		ChanTotUser;	// 입장한 채널의 유저수
	Ms_UserInfo Ui;				// 유저 정보	
} Ms_UserChangeInfo;

typedef struct
{
	__int64		SeqNo;								// 인식용 시퀀스 번호
	//int			snsType;							// 계정 인증 타입
	//char		snsUID[MAX_SNS_UID_LEN + 1];
} Ms_NoUserChangeInfo;

typedef struct
{	
	__int64		nRequestUUID;			// 요청자의 UUID
	__int64		nResponseUUID;			// 응답자의 UUID
	int			nKind;
	unsigned short datacenterServerCode;	// 요청자의 datacenterServerCode;
} Ms_ResponseInvite;

//- lorddan(2013/04/04) - 
typedef struct
{
	char		Kind;			// 정보의 종류(1:채널 입장 유저, 2:채널 변경 유저, 3:채널 퇴장 유저)
	__int64		SeqNo;			// 인식용 시퀀스 번호
	Ms_UserInfo Ui;				// 유저 정보	
} Ms_UserChangeInfoSimple;

////////////////////////////////////////////////////////////////////////////////////////
// 사용자 강제 종료 명령 정보
typedef struct
{
	short	Code;			// 코드
	short	UNum;			// 유저 번호	
	__int64 userUID;		// 유저 DB UID
	bool	bDontSaveDB;	// 강제 종료시 DB기록을 취소시킬 것인가?
} Ms_DisconnectUserInfo;
////////////////////////////////////////////////////////////////////////////////////////
// 사용자 접속 체크 명령 정보
typedef struct
{
	short	Code;			// 코드
	short	UNum;			// 유저 번호	
	__int64 userUID;		// 유저 DB UID
} Ms_CheckUserInfo;

////////////////////////////////////////////////////////////////////////////////////////
// 다른 서버로 채널 변경 요청 정보
typedef struct
{	
	short	FromSNum;		// 이동을 요청한 서버 번호
	short	ToUNum;			// 이동 요청을 했던 유저 번호
	short	ReqGroupNo;		// 요청한 Group 번호
	short	ReqChanNo;		// 요청한 Channel 번호
	short	TarGroupNo;		// 이동하고자하는 그룹 번호
	short	TarChanNo;		// 이동하고자하는 채널 번호
	short	TotGameItem;	// 사용자가 소유하고 있는 아이템 개수
	short	shTotMarbleItemNum;
	short	shTotOwnedSkillNum;
	unsigned short ToDataCenterServerCode;	// 이동할 곳의 데이터 센터 번호
	unsigned short FromDataCenterServerCode;	// 이동 요청한 곳의 데이터 센터 번호
} Ms_AskMoveChan;

////////////////////////////////////////////////////////////////////////////////////////
// 다른 서버로 채널 변경 요청 결과 정보
typedef struct
{
	short				Code;			// 결과 코드(0보다 작으면 입장 불가)
	short				ToSNum;			// 이동 요청을 했던 서버 번호
	short				ToUNum;			// 이동 요청을 했던 유저 번호
	short				ReqGroupNo;		// 요청한 Group 번호
	short				ReqChanNo;		// 요청한 Channel 번호
	char				WaitIP[MAX_DOMAIN_NAME+1];		// 이동할 서버의 IP
	int					WaitPort;		// 이동할 서버의 포트 번호
	__int32				WaitPass;		// 접속 대기 패스 번호
	Ms_UserChangeInfo	UCInfo;			// 유저의 채널 변경 정보 
	unsigned short		ToDataCenterServerCode; // 요청한 데이터 센터 코드
} Ms_ResultMoveChan;

////////////////////////////////////////////////////////////////////////////////////////
// 접속할 게임 서버 정보
typedef struct
{
	char	IP[MAX_DOMAIN_NAME];				// 서버 IP
	int		Port;				// 서버 포트 번호
	__int64	SeqNo;				// 접속 시퀀스 번호
	short	GroupNo;			// 그룹 번호
	short	ChanNo;				// 통합 채널 번호
	short	SNum;				// 서버 번호
	bool	bTest;
	bool	bGamePlayDisconnetFlag;		// 게임중 접속이 끊겼냐?(클라이언트에 알려줄러고
} Ms_DestServerInfo;

typedef struct
{
	int			ClientUNum;						// 의뢰인 UNum
	__int64		ClientUserUID;					// 의뢰인 ID
	__int64		FriendUID;						// 친구 SNSUID
	__int16 	_groupNum;						// 그룹 넘버
	__int16 	_channelNum;					// 채널 넘버
	__int16 	_roomNum;						// 방번호
	__int64 	nUniqueKey;						// Room Unique 번호
	int			inviteType;						// sns로 초대인지, 북마크 초대인지	
} REQ_INVITE_FRIEND;


typedef struct
{
	int			nUNum;				// 대상자의 UNum
	//char		friendSNSUid[MAX_SNS_UID_LEN];	
	__int64 	ClientUserUID;			// 의뢰인 ID
	__int64		FriendUID;
	__int16 	_groupNum;			// 그룹 넘버
	__int16 	_channelNum;			// 채널 넘버
	__int16 	_roomNum;			// 방번호
	__int64 	nUniqueKey;			// Room Unique 번호
	int			inviteType;			// sns로 초대인지, 북마크 초대인지
	unsigned short		dataCenterServerCode;			// 의뢰한 데이터 센터 코드
} RESULT_INVITE_FRIEND;

typedef struct
{
	__int64		nRequestUUID;			// 요청자의 UUID
	__int64		nResponseUUID;			// 응답자의 UUID
	int			nUNum;
	int			nKind;
} RESULT_RESPONSE_INVITE;

//////////////////////////////////////////////////////////////////////////////
//-- 모바일용 구조체
//////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////                               [게임 아이템 작업]                               //////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define MAX_ITEM_NUM           (100) // 최대아이템 갯수

#define GAMEITEM_MES_ERROR     (-1) // 에러
#define GAMEITEM_MES_SUCCESS   ( 0) // 성공
#define GAMEITEM_MES_DBERROR   ( 1) // DB에러
#define GAMEITEM_MES_NOITEM    ( 2) // 아이템이 없거나 사용가능한 아이템 없다
#define GAMEITEM_MES_USEABLE   ( 3) // 아이템이 있고 사용하지 않았다
#define GAMEITEM_MES_EXPIRED   ( 4) // 이미 사용중인 아이템이고, 기간이 만료되었다
#define GAMEITEM_MES_USEFAIL   ( 5) // 아이템사용에 실패했으며, 다시 시도 요청



/////////////////////////////////////////////////////////////////////////////////////////
// 리컨넥션 정보
typedef struct
{
	bool bDontChangeIP;	// TRUE이면 재접속시 IP를 변경하지 않는다(아래의 값을 무효화)
	char ServIP[MAX_DOMAIN_NAME+1];	// 새로운 서버 IP
	__int32 ServPort;		// 새로운 서버 Port
} RECONNECTINFO;



/////////////////////////////////////////////////////////////////////////////////////////


typedef struct
{
	__int64				ownedItemUID;
	int					gradeType;
	int					itemInfoUID;
	int					Ability[Max_Ability_Kind];
	int					EnchantAbility[Max_Ability_Kind];
	int					SkinAbility[Max_Ability_Kind];
	int					EnchantNum;
	int					EnchantExp;
	int					MaxEnchantNum;

	__int64				EquipDiceOwnedItemUID;						// 장비 주사위 아이템 번호.
	int					EquipDiceItemInfoUID;						// 주사위 아이템 번호
	int					EquipDiceMaxEnchantNum;						// 장착 다이스 최대 인첸트
	int					EquipDiceEnchantNum;						// 장착 다이스 인첸트
	int					EquipSkinItemInfoUID;						// 장착한 Skin 번호
} stCHARINFO;


struct USER_OPTION
{
	char				isSPointRejectFlag;
	char				isHideFlag;					//	카톡 친구 초대 차단.

	enum eBookmarkFriendOption
	{
		eBookmarkFriendOption_none,		// 접속 차단안함
		eBookmarkFriendOption_reject,	// 접속 차단
		eBookmarkFriendOption_withdrawal	// 탈퇴한 유저
	};

	char				bookmarkFriendOption;	//	즐겨찾기 친구 옵션 eBookmarkFriendOption
};


enum
{
	NOT_DISCONN = 0,			// Connection State
	NORMAL_DISCONN,				// 일반 Disconnect State
	PLAY_DISCONN,				// Play Disconnect State
};

struct OWNED_SKILL_INFO
{
	__int64 ownedSkillUID;
	int SkillUID;
	int enchantNum;
};

//////////////////////////////////////////////////////////////////////////
// Tencen Load_VIP
//////////////////////////////////////////////////////////////////////////
enum{
	VIP_NULL = 0,
	VIP_NORMAL = 1,	// 일반회원
	VIP_BLUE = 4,	// 블루 다이아몬드
	VIP_RED = 8,	// 레드 다이아몬드
};

typedef struct _LOAD_VIP_INFO
{
	int VIP;		// enum VIP등급
	bool isVIP;		// VIP 여부(유져 VIP상태 판단의 유일 표시, 0:아님, 1:옮음)
	int year;		// 연회비 여부(빨강등급은 연회비 표시 없음)
	int level;		// VIP 레벨(빨강등급은 VIP레벨 표시 없음)
	int luxury;		// 스페셜 버전 여부

	void Reset()
	{
		VIP = VIP_NULL;
		isVIP = false;
		year = 0;
		level = 0;
		luxury = 0;
	}
}LOAD_VIP_INFO;

typedef struct _LOAD_VIP
{
	int count;
	LOAD_VIP_INFO loadVipInfo[MAX_LOAD_VIP];

	void Reset()
	{
		count = 0;
		for(int i = 0; i < MAX_LOAD_VIP; i++)
		{
			loadVipInfo[i].Reset();
		}
	}

	bool IsVIP()
	{
		//- 임시 코드
		return false;
		for(int i = 0; i < count; i++)
		{
			if(loadVipInfo[i].isVIP == true)
				return true;
		}

		return false;
	}
}LOAD_VIP;
//////////////////////////////////////////////////////////////////////////

// 텐센트 TLog
struct TLOG_BASE
{
	//int				vGameSvrId;						// 로그인 한 게임 서버 넘버	
	//__int64			dtEventTime;					// 로그 기록 시간	포맷 “YYYY-MM-DD HH:MM:SS”
	int				PlatID;							// 게임 플랫폼 넘버	안드로이드 ,ios ,jail break,
	char			OpenID[MAX_SNS_UID_LEN+1];		// 유저의 index(유일함)	QQ 넘버 혹은 게임 내 유저의 index
	int				snsType;						// GameAppID와 매칭 qq 인지 wechat 인지
};

// 텐센트 TLog 유저등록, 로그인, 로그아웃
typedef struct
{
	//TLOG_BASE	BASE;						// 사용자 기본정보
	int			ClientVersion;				// 게임 클라 버전 넘버	예:1.0.1 
	int			SystemSoftware;				// 앱  버전 넘버	예: 91, 텐센트 게임, 안드로이드 마켓 등
	int			SystemHardware;				// 핸드폰 기기 타입	
	int			TelecomOper;				// 운영회사	china telecom, china uincom, china mobile
	int			Network;					// wifi 타입	3G/WIFI/2G
	int			ScreenWidth;				// 스크린 해상도 넓이	
	int			ScreenHight;				// 스크린 해상도 높이	
	float		Density;					// 스크린 해상도 밀도	
	int			Channel;					// 게임 런칭 마케 넘버	
	char		UUID[MAX_MAC_ADDRESS_LEN];	// 모바일 기기 MAC 주소	
	char		CpuHardware[MAX_CPU_DESC];	// Cpu 파라미터	Cpu 타입, 빈도, 코어 수
	int			Memory;						// 메모리 사이즈	단위 M
	int			GLRender;					// render정보	
	int			GLVersion;					// 버전	
	int			DeviceId;					// 장비ID	모바일 기기 유일한 표기(index 혹은 기타)
}PlayerRegister;

// 사용자 정보
struct USERINFO
{
	int			IAPType;

	// line iap
	char		lineMemberId[MAX_SNS_UID_LEN];			// 라인 로그인 인증으로 받은 라인멤버ID

	// Game DB ID
	int			DBConnectID;								// Game DB 컨넥션 ID
	__int64		userUID;									// Game 유니크 ID

	int			snsType;									// 계정 인증 타입
	char		snsUID[MAX_SNS_UID_LEN + 1];
	char		snsNickName[MAX_NICKNAME_LEN + 1];
	char		snsPicURL[MAX_SNS_PICURL_LEN +1];

	int			UNum;										// 유저 번호				- DB에 저장되는 정보가 아님
	char		NickName[MAX_NICKNAME_LEN + 1];				// 닉네임수정
	char		Greeting[MAX_GREETING_LEN + 1];
	stCHARINFO	stCharInfo;

	bool		isTutorial;
	bool		isTutorial_2;
	bool		isLoginRewardCoin;
	bool		isMyPic;									// 내 사진 보여주기 설정
	bool		isAppReviewReward;							// 앱리뷰 리워드 보상 여부
	int			ClearAIPlay;								// Clear AI Play 진행도
	int			isTutorialCardEnchant;						// 카드 인첸트 튜토리얼
	int			isTutorialCardMix;							// 카드 믹스 튜토리얼
	int			isTutorialSkill;							// 스킬 튜토리얼
	int			isTutorialEnchantSkill;						// 강화 스킬 튜토리얼

	char		Sex;										// 성별
	int			exp;

	__int64		DailyItemReceiveTime;						// 데일리 아이템받은 시간.
		
	int			WinNum;										// 승수
	int			LooseNum;									// 패수
	int			iTeamWinNum;								// 팀 승리
	int			iTeamLooseNum;								// 팀 패
	int			winConsecutive;								// 연승
	int			loseConsecutive;							// 연패
	int			loseConsecutiveRewardCount;					// 연패 보상 카운트
	__int64		loseConsecutiveChangedDay;					// 연패 보상 날자

	//- lorddan(2013/07/01) - 
	int			iDayWinNum;									// 당일 승리
	int			iDayLooseNum;								// 당일 패배
	int			IAPRecordCount;								// 결재 여부(한번이라도 하지 않았다면 0)
	__int64		IAPRecordLastDate;							// 인앱 마지막 갱신 시간

	
	int			ALGPoint;									// ALG 포인트
		
	TMoney		Coin;										// 무료로 지급 받은 코인
	TMoney		Coin_P;										// 캐쉬로 구매한 코인

	TMoney		tUserMoney;									// 실제 머니	

	TMoney		sPoint;										// 소셜 포인트 소셜 활동으로 받는 포인트
	TMoney		sPoint_P;									// 무료로 소셜 포인트 소셜 활동으로 받는 포인트

	__int64     sPointChargedTime;							// 소셜 포인트 마지막 차지 타임

	TMoney		Gold;										// 게임에서 획득한 머니 골드 환산
	TMoney		Gold_P;		

	TMoney		MaxSPoint;									// 보유할수 있는 최대 소셜 포인트
	int			MaxRecvSPoint;								// 보유 할수 있는 소셜 선물 개수

	TMoney		tMaxWinUserMoney;							// 기록용 최대 승리 머니
	__int64		weekRankScore;								// 이번주 랭크 스코어
	__int64		MaxRankScore;								// 최대 랭크 스코어
	__int64		MaxRankScore2;								// 한번에 딴 최고 랭크 점수(누적아님)

	// 금주 랭크 , 전전 추가 내용
	/*__int64		MaxWeekRankScore;							// 이번주 최대 랭크 스코어
	__int64     MaxWeekRankScore2;							// 이번주 한판 최고 랭크 점수

	int			weekWinNum;									// 이번주 승리 수
	int			weekLoseNum;								// 이번주 패배 수*/

	int			rankRecordCount1;							// 1위 기록
	int			rankRecordCount2;							// 2위 기록
	int			rankRecordCount3;							// 3위 기록

	//----------------------------------------------------------------------------

	__int64		logoutTime;

	__int64		FreeChargedDay;								// 무료 충전 남은 시간.

	int			i3MonopolyWinCnt;							// 기록용 트리플 독점 횟수
	int			iLineMonopolyWinCnt;						// 기록용 라인 독점 횟수
	int			iAllTourWinCnt;								// 기록용 관광지 독점 횟수
	int			i3MonopolyLineWinCnt;						// 트리플X라인 독점 횟수
	int			iLineAllTourWinCnt;							// 라인X관광지 독점 횟수

	__int64		i64UserIP;									// 유저 아이피
	char		clientAddr[MAX_IP_STR];						// 유저 아이피

	int			inviteCount;								// 친구를 초대한 카운트
	int			sendSPointExp;								// 소셜포인트 선물 exp
	int			characterTicketCount;							// 소셜 포인트 선물로 받은 프리티켓

	int			BusinessTicketCount;						// 비지니스 입장 티켓
	int			FirstTicketCount;							// 퍼스트 입장 티켓
	int			DiceBoosterCount;
	int			WithDrawBoosterCount;
	int			RandomGoldCardID;
	int			ODDItemCount;

	int			friendPlayTicketCount;						// 남은 친구 게임 하기 티켓 카운트
	__int64		friendPlayTicketChargedDay;					// 충전 받은 시간.

	int			sendSPointCount;							// 소셜 포인트 선물 가능 카운트
	int			sendSPointQuiescenceCount;					// 소셜 포인트 선물 가능 카운트. 휴면
	__int64		sendSpointCountChargedDay;					// 카운트 충전 시간.

	int			nVersion;									// Packet Version
	USER_OPTION	OPTION;

	int			MaxSkillInventory;
	int			activeSlotNum;
	OWNED_SKILL_INFO skillSlotinfo[MAX_SKILL_SLOT];			// 스킬 슬롯 정보	

	LOAD_VIP	loadVIP;									// 텐센트 Load_VIP	

	TLOG_BASE GetTLogBase()
	{
		TLOG_BASE info;

		strncpy(info.OpenID, snsUID, sizeof(info.OpenID));

		if( IAPType == IAP_APPLE )
			info.PlatID = 0;
		else
			info.PlatID = 1;

		info.snsType = snsType;

		return info;
	}
};


// 텐센트 결제시 필요한 데이터
struct TENCENT_CASH_TOKEN
{
	char m_Token[ MAX_TOKEN_SIZE + 1 ];			// Token(텐센트: Access Token, 라인: Hash String)
	char m_pay_Token[ MAX_TOKEN_SIZE + 1 ];		// 텐센트용 payToken
	char m_pfKey[ MAX_PFKEY_LEN + 1 ];			// 텐센트용 pfKey
};

/////////////////////////////////////////////////////////////////////////////////////////
// 사용자 정보2 (부가 정보들: 다른 서버로 채널 이동시 전송이 필요한 정보들)
typedef struct
{
	bool			bNoWhisper;			// 귓말 거부중인가?
	time_t			ConnectSec;			// 접속 시간(초)
	__int64			userUID;		// 유저 DB UID
	int				GamePlaySec;		// 플레이시간
	char			UserIP[24];
	bool			bQuiescence;		// 휴면 유저인가?

	// 텐센트용	
	char			sessionID[MAX_SESSIONID_LEN];				// 텐센트 세션ID
	char			sessionType[MAX_SESSIONTYPE_LEN];			// 텐센트 세션Type
	char			pf[MAX_PF_LEN];								// 텐센트 pf
	
	TENCENT_CASH_TOKEN tencentCashToken;	
	
	// 라인용
	char			nation[MAX_NATION_CODE];					// 국가 code(ISO-3166-alpha-2)
	char			language[MAX_LANGUAGE_CDOE];				// Client의 언어 설정(ISO-639-1)

	PlayerRegister	m_TLog_PlayerRegister;	// 유저 등록, 로그인, 로그아웃 
	int				friendCount;

} USERINFO2;


/////////////////////////////////////////////////////////////////////////////////////////
// 로그인 요청 정보	[SSO 작업] - 추가
enum
{
	WITHDRAWAL_DEFAULT = 0,
	WITHDRAWAL_ROLLBACK,
	WITHDRAWAL_CANCEL,
};

typedef struct
{
	int  IAPType;
	int  snsType;						// SNS 계정 인증 타입 0 게스트... 추후 추가
	char snsUID[MAX_SNS_UID_LEN+1];		// SNS 일경우 검증용 UID를 받는다.
	char snsPicURL[MAX_SNS_PICURL_LEN+1];		// SNS 사진 Url
	char snsNickName[MAX_NICKNAME_LEN + 1];		// 닉네임수정
// 	char SockNameIP[20];
// 	char HostNameIP[20];
// 	int	 Port;
// 
	// 접속할 서버 정보
	__int64  SeqNo;			// 접속자 인식용 시퀀스 번호
	short GroupNo;			// 접속할 그룹 번호
	short ChanNo;			// 접속할 통합 채널 번호
	short SNum;				// 접속할 서버 번호
	//short LocChanNo;		// 접속할 로컬 채널 번호
	
	int		nWithDrawal_rollback;				// 복구 여부 0: Default, 1:복구요청, 2:취소(끊어버림)
	//bool	bGamePlayDisconnetFlag;		// 게임중 접속이 끊겼냐?
	int		nVersion;
} ASK_LOGININFO;


// 라인용 로그인 구조체
struct ASK_LOGININFO_LINE : public ASK_LOGININFO
{
	char		reqStr[MAX_LINE_REQSTR]; // 라인 인증 정보( 클라이언트에서 제공 )
	// 국가 code, 언어
	char		Nation[MAX_NATION_CODE];	// 국가 code
	char		Language[MAX_LANGUAGE_CDOE];					// Client의 언어 설정	
};

// 라인용 로그인 구조체
struct ASK_LOGININFO_TENCENT : public ASK_LOGININFO
{
	int			SystemSoftware;				// 앱  버전 넘버	예: 91, 텐센트 게임, 안드로이드 마켓 등
	int			SystemHardware;				// 핸드폰 기기 타입	
	int			TelecomOper;				// 운영회사	china telecom, china uincom, china mobile
	int			Network;					// wifi 타입	3G/WIFI/2G
	int			ScreenWidth;				// 스크린 해상도 넓이	
	int			ScreenHight;				// 스크린 해상도 높이	
	float		Density;					// 스크린 해상도 밀도	
	int			Channel;					// 게임 런칭 마케 넘버	
	char		UUID[MAX_MAC_ADDRESS_LEN];	// 모바일 기기 MAC 주소	
	char		CpuHardware[MAX_CPU_DESC];	// Cpu 파라미터	Cpu 타입, 빈도, 코어 수
	int			Memory;						// 메모리 사이즈	단위 M
	int			GLRender;					// render정보	
	int			GLVersion;					// 버전	
	int			DeviceId;					// 장비ID	모바일 기기 유일한 표기(index 혹은 기타)
 	char		Token[MAX_TOKEN_SIZE];		// Token(텐센트: Access Token)
	char		session_ID[MAX_SESSIONID_LEN]; // 세션ID
	char		session_Type[MAX_SESSIONTYPE_LEN]; // 세션Type
	char		pf[MAX_PF_LEN];				// pf정보
	char		pay_Token[ MAX_TOKEN_SIZE + 1 ];	// 텐센트용 payToken
	char		pfKey[ MAX_PFKEY_LEN + 1 ];		// 텐센트용 pfKey
};

typedef union
{
	ASK_LOGININFO LoginInfo;
	ASK_LOGININFO_LINE LoginInfo_Line;
	ASK_LOGININFO_TENCENT LoginInfo_Tencent;
}UNION_ASKLOGIN;


//- lorddan(2013/04/04) - 
typedef struct
{	
	int		snsType;						// SNS 계정 인증 타입 0 게스트... 추후 추가
	char	snsUID[MAX_SNS_UID_LEN+1];		// SNS 일경우 검증용 UID를 받는다.
	__int64	nUniqueTime;					// User 확인
	bool	bDisconnLogin;					// 기존 유저를 끊고 접속할 것인가?
}ASK_DESTSERVERINFO;


//- napcahe(2013-12-12)
typedef struct
{
	unsigned short		retType;			// 결과 CDTD_RESULT_DESTSERVERINFO의 Enum 참조
	Ms_DestServerInfo	destServerpInfo;	// 목적지 서버
	ASK_DESTSERVERINFO	askInfo;			// 물어본 정보
	
} RES_DTD_DESTSERVERINFO;


struct SMALLUSERINFO	// 대기실 사용자 정보 갱신용
{
	int			UNum;							// 유저 번호
	__int64		userUID;		// 유저 DB UID
	char		NickName[MAX_NICKNAME_LEN + 1];		// 닉네임수정 	
	stCHARINFO	stCharInfo;
	short		Sex;							// 성별
	int			WinNum;							// 승수
	int			LooseNum;						// 패수
	int			iTeamWinNum;					// 팀 승리
	int			iTeamLooseNum;					// 팀 패
	int			iDayWinNum;						// 당일 승리
	int			iDayLooseNum;					// 당일 패
	int			winConsecutive;

	TMoney		tUserMoney;							
	TMoney		sPoint;
	TMoney		sPoint_P;

	short		Position;						// 현재 위치(WH_CHAN:채널선택 WH_LOBY: 대기실 WH_SHOP:상점 WH_GAME:게임방)
	short		RoomNum;						// 방번호(현재 위치가 게임방일때만 유효)
};

// 로그인시 유저 정보
typedef struct
{
	// 실시간 전적
	int		iOrgWinNum;
	int		iOrgLooseNum;
	int		iOrgTeamWinNum;
	int		iOrgTeamLooseNum;
	int		iOrgDayWinNum;									// 당일 승리
	int		iOrgDayLooseNum;								// 당일 패배
	int		iOrgWinConsecutive;
	int		iOrgALGPoint;
	int		iOrgExp;

	TMoney	tOrgUserMoney;
	TMoney	tOrgSPoint;
	TMoney	tOrgSPoint_P;
	TMoney	tOrgCoin;
	TMoney	tOrgCoin_P;
	TMoney	tOrgGold;
	TMoney	tOrgGold_P;

	
	// 게임 로그아웃할때
	bool	bChangeRecordFlag;
	//__int64		EquipDiceOwnedItemUID;

} ORGINFO;

/////////////////////////////////////////////////////////////////////////////////////////
// 서버 정보	[게임 아이템 작업]
typedef struct
{
	int  GameCode;			// 게임 코드	
	
	__int64	ServerTime;	// 서버의 현재 시간
} SERVERINFO;

/////////////////////////////////////////////////////////////////////////////////////////
// 로그인 정보
typedef struct
{
	USERINFO UI;			// 사용자 정보
	__int64 userUID;		// 유저 DB UID
	short GroupNo;			// 입장한 통합 채널 번호
	short ChanNo;			// 입장한 통합 채널 번호
	bool bGamePlayDisconnetReconnetFlag;			// 게임 중 접속 끊기 유저 다시 접속 했냐?
} LOGININFO;

/////////////////////////////////////////////////////////////////////////////////////////
// 채널 이동시 접속 정보(다른 서버에서 채널 이동)
typedef struct
{
	__int64 userUID;		// 유저 DB UID
	short	UNum;			// 접속자의 유저번호
	__int32	WaitPass;		// 접속 대기 패스 번호
} CONNECT_MOVECHANINFO;

/////////////////////////////////////////////////////////////////////////////////////////
// 채널 입장 정보
typedef struct
{
	int		Code;				// 처리 코드(0보다 작으면 입장 불가 코드)
	short	GroupNo;			// 입장한 채널의 그룹 번호
	short	ChanNo;				// 통합 채널 번호
	//short	LocChanNo;			// 로컬 채널 번호
	//char	ChanName[32];		// 채널 이름
} ENTERCHANINFO;

////////////////////////////////////////////////////////////////////////////////////////
// 클라이언트용 그룹 정보
typedef struct
{
	short	GroupNo;				// 그룹 번호
	char	GroupName[24];			// 그룹 이름
} GROUPINFO;

/////////////////////////////////////////////////////////////////////////////////////////
// 채널 정보
typedef struct
{
	short GroupNo;			// 그룹 번호
	short ChanNo;			// 통합 채널 번호
	short NowUser;			// 현재 참여인원
	short MaxUser;			// 최대 참여인원
	char  WorkState;		// 채널 상태(0:정상 1:점검중 2:종료중)
} CHANNELINFO;


////////////////////////////////////////////////////////////////////////////////////////
// 방 정보
struct	stROOM_PLAYER_INFO
{
	bool		bExist;		// 존재하느냐?
	TMoney		tUserMoney;		// 마블머니
	TMoney		sPoint;
	TMoney		sPoint_P;

	char		cGender;		// 성별
};

typedef struct	// 대기실 방 정보 갱신용
{
	
	short				RoomNum;			// 방번호
	int					UNum;				// 방장의 유저 번호
	__int64 userUID;		// 유저 DB UID
	char				NickName[MAX_NICKNAME_LEN + 1]; // 닉네임수정 		
	char				State;				// 상태(0:대기중 1:게임중)	
	char				NowUserNum;
	stROOM_PLAYER_INFO	stRoomPlayerInfo[MAX_PLAYER];



} SMALLROOMINFO;

////////////////////////////////////////////////////////////////////////////////////////
// 방 정보
enum{
	ROOMSTATE_WAIT = 0,
	ROOMSTATE_PLAY = 1,
	ROOMSTATE_GARA = 2,
};

enum{
	CTEAM_SINGLE = 0,				// 개인전
	CTEAM_TEAM = 1,					// 팀전
	CTEAM_AUTOSINGLE = 2,			// Auto Single
};
typedef struct
{
	short				RoomNum;			// 방번호
	int					UNum;				// 방장의 유저 번호
	__int64				userUID;			// 방장의 UID
	char				NickName[MAX_NICKNAME_LEN + 1]; // 닉네임수정 		
	char				State;				// 상태(0:대기중 1:게임중)	
	char				NowUserNum;
	stROOM_PLAYER_INFO	stRoomPlayerInfo[MAX_PLAYER];
	int					mapKind;			// 맵 종류
	int					seedType;			// 시드 머니 타입
		
//	char				Title[MAX_ROOM_TITLE_LEN];	// 방제목			
	char				cTeamFlag;			// 팀전이냐. CTEAM_SINGLE ~ CTEAM_AUTOSINGLE
	char				MaxUserNum;			// 최대 참여인원
	char				startUserNum;		// 시작시 참여 인원(중간에 게임을 나가도 변동 없음)
	char				Secret;				// 공개 여부(0=공개 1=비공개)

	short				shLeftGameTurnCount;
	short				shLeftGameTimeSec;
	short				shMAXGameTurnCount;
	short				shMAXGameTimeSec;
	__int64				nUniqueKey;			// 방생성시 생성되는 방의 Unique 정보
	int					nVersion;
} ROOMINFO;






struct	stETC_ROOM_INFO	// 기타 방정보. // 게임 시작 방정보. 시작할때나 참여자 정보 줄때 사용.
{
	TMoney		tWinTotGainMoney;			// 보상금


	int			iTurnListServerPnum[MAX_PLAYER];		// 턴 순서 pnum 리스트임 -1가 될수 있다.( 0번부터 턴이 시작하지 않음)
	int			iFestivalRgnNum[MAX_FESTIVAL_NUM];		// 축제 개최되는 도시 0 = 항상 나오는거 1, 2 숨겨져 있는것.
	
	bool		bThemeParkOpenNight;					// 테마파크맵 야간개장
	int			iParadeCarPos;							// 테마파크맵 퍼레이드카 위치, MAP_THEMEPARK_PARADE_CAR_COME_OUT_TRUN 턴후 등장, 초기 -1
	bool		bParadeCar;								// 테마파크맵 퍼레이드카 등장 여부, 상기 -1을 사용할시 여러 문제점이 있어 추가
};

////////////////////////////////////////////////////////////////////////////////////////
// 게임 정보
typedef struct
{
	ROOMINFO Ri;
	USERINFO Ui[MAX_PLAYER];
} GAMEINFO;

////////////////////////////////////////////////////////////////////////////////////////
// 유저한테 적용되는 찬스카드 관련 상태
typedef struct
{	
	char cRipoffPricesTurn;	// 바기지 요금 남은턴
	char cAccidentTurn;		// 교통사고 남은 턴
	int  iSkillRipOffUID;	// 스킬발동(바가지 면제)

	char cMoveReverse;		// 뒤로가기
	char cMoveX2;			// X2
	char cMoveChanceCard;	// 찬스카드이동
	char cMoveStart;		// 출발지이동
} stGK_STATE;

////////////////////////////////////////////////////////////////////////////////////////
// 플레이어 상태 정보

#define MAX_KEEP_CHANCE_CARD_NUM		1
#define MAX_LOADING_ROOM_TIME			30
#define MAX_READY_ROOM_TIME				15
#define MAX_WAIT_ROOM_TIME				45
#define MAX_RESET_ROOM_TIME				70
typedef struct
{
	char			JoinState;		// 게임 참여 상태(0:불참, 1:참여)
	char			PlayState;		// 게임 상태(0:게임종료 1:게임중)
	char			Team;			// 팀(0:싱글 1:A팀 2:B팀 3:C팀)
	char			cReadyState;	// 1 : 레디
	char			cTeamColor;		// 팀전에서 사용하는 팀전 색
	TMoney			tPlayMoney;		// 게임 플레이 머니.		(0 되면 파산).		
	TMoney			tTotWithDrawMoney;	// 총 인출금
	char			cAutoPlay;		// 0 : 없음 : 1 : 단축, 2 : 오토플레이
	bool			bAIPlayer;		// AI Player인가?

	bool			bBankruptFlag;	// 파산 했냐?

	stGK_STATE		stGKState;		// 유저한테 적용되는 찬스카드 관련 상태

	char			cChanceCardNo[MAX_KEEP_CHANCE_CARD_NUM];

	char			cMaxDiceItemCnt;
	char			cDiceItemCnt;
	char			cLoanCount;		// 대출 받을수 있는 회수

	__int64			dwTimeOut;		// Time Out에 의해서 Room에서 강퇴 당하기 까지 남은 시간
} PLAYSTATE;

////////////////////////////////////////////////////////////////////////////////////////
// 플레이어 상태 정보 2
typedef struct
{	
	bool bJoinBanishVote;	// 추방 투표에 참여중인가?
	bool bHaveBanishVote;	// 추방 투표를 마쳤는가?
	char cGameOverKind;		// 게임 오버가 됬냐?


} PLAYSTATE2;

////////////////////////////////////////////////////////////////////////////////////////
// 방 입장 정보
typedef struct
{
	ROOMINFO Ri;
	USERINFO Ui[MAX_PLAYER];
	PLAYSTATE Ps[MAX_PLAYER];

	stETC_ROOM_INFO	stEtcRoomInfo;
} ENTERROOMINFO;

////////////////////////////////////////////////////////////////////////////////////////
// 내 사용자 정보 변경 정보
typedef struct
{

	USERINFO stUserInfo;
} CHANGEMYINFO;

typedef struct
{
	int			WinNum;										// 승수
	int			LooseNum;									// 패수
	int			iTeamWinNum;								// 팀 승리
	int			iTeamLooseNum;								// 팀 패
	int			iDayWinNum;									// 당일 승리
	int			iDayLooseNum;								// 당일 패
	int			winConsecutive;								// 연승

	int			ALGPoint;									// ALG 포인트

	TMoney		tUserMoney;									// 실제 머니
	TMoney		Coin;										// 캐쉬로 충전한 코인
	TMoney		Coin_P;										// 캐쉬로 충전한 코인

	TMoney		sPoint;										// 소셜 포인트
	TMoney		sPoint_P;									// 무료 
	__int64		sPointChargedTime;							// 소셜 마지막 충전 시간

	TMoney		Gold;										// 골드
	TMoney		Gold_P;										// 무료
} CHANGEMYLOWINFO;


//////////////////////////////////////////////////////////////////////////////////////////
//  게임 정보 구조체


 


////////////////////////////////////////////////////////////////////////////////////////
// 게임 시작 정보
typedef struct
{
	ENTERROOMINFO		stEnterRoomInfo;
} STARTINFO;




////////////////////////////////////////////////////////////////////////////////////////
// 게임 진행 정보
typedef struct
{
	short RoomNum;		// 방 번호
	short ServPNum;		// 서버측 플레이 번호
	__int64 userUID;		// 유저 DB UID
} BASICINFO;



////////////////////////////////////////////////////////////////////////////////////////
// 게임 종료 결과


enum
{
	WithDrawKind_Init = 0,			// 초기 인출금
	WithDrawKind_Start,				// 월급
	WithDrawKind_Loan,				// 대출
	WithDrawKind_Bankrupt,			// 파산할때 정산 인출금
	WithDrawKind_WinMonoplay,		// 승리 독점할때 추가 인출금	
	WithDrawKind_Salary_Bokbulbok,	// 월급 복불복 
	Max_WithDrawKind,	
};

struct GAMEOVERUSERINFO
{	
	__int64 userUID;		// 유저 DB UID
	char NickName[MAX_NICKNAME_LEN + 1]; // 닉네임
	char ServPNum;
	char MyRank;   //개인순위 (몇등인지?)
	char cTeamNo;

	stCHARINFO	stCharInfo;
	//////////////////////////////////////////////////////////////////////////
	// 갱신 해야되는 유저 정보.
	int			iWinNum;
	int			iLooseNum;
	int			iTeamWinNum;
	int			iTeamLooseNum;	
	int			iDayWinNum;
	int			iDayLooseNum;
	TMoney		tUserMoney;			// 현재 머니
	///////////////////////////////////////////////////////////////////////////
	//
	PLAYSTATE	stPs;
	

	TMoney		tCharge;			// 게임비
	TMoney		tGetUserMoney;		// 얻은 돈
	TMoney		tLooseUserMoney;	// 잃은 돈.

	__int64		weekRankScore;
	TMoney		getUserGold;
	TMoney		getUserGoldBooster;
	TMoney		getUserGoldEvent;
	TMoney		getUserGoldTeam;
	TMoney		getUserGoldTencentVIP;	// 텐센트 VIP 유져인 경우 추가 골드 지급(한도액은 넘지 않는다)
	TMoney		getRankScore;
	TMoney		getRankScoreBonus;
	TMoney		getRankScoreBooster;
	TMoney		getUserRPEvent;
	TMoney		getUserExpEvent;
	int			iSkillGoldBoosterUID;

	char		cRookieWinKind;		// 루키 채널에서만 쓰는 승리 종류 0 : 없음 1 : 우승 2 : 준우승


	//////////////////////////////////////////////////////////////////////////
	// 추가 정보.
	TMoney		tWithDrawKind[Max_WithDrawKind];
	TMoney		tMacauWinMoney;
	TMoney		tMacauLooseMoney;	

	TMoney		tVipProfitMoney;		// 수익 (통행룡 +-)
	TMoney		tVipInvestMoney;		// 투자(건설비 - 매각)
	TMoney		tVipSupportMoney;		// 지원(걸설비)

	short		shWinPoint;
	short		shAllTourPoint;
	short		shLineMonopolyPoint;
	short		sh3MonopolyPoint;

	// 연패 보상 여부
	bool		loseConsecutiveReward;

	// 게임 참여/승리로 획득한 sPointExp
	int			sPointExp;
	int			tencentVIPBonusRate;
};

struct GAMEOVERRESULT
{
	GAMEOVERUSERINFO	Gui[MAX_PLAYER];
	EFinishType			FinishType;
	int					iLeftTurn;		// 남은 턴
	int					iLeftTime;		// 남은 시간
	TMoney				tWinTotGainMoney;		// 보상금

	//////////////////////////////////////////////////////////////////////////
	// 총 유저 누적 추가 정보.	
	TMoney		tTotWithDrawKind[Max_WithDrawKind];
	TMoney		tTotMacauWinMoney;
	TMoney		tTotMacauLooseMoney;
};

struct SINGLEGAMEOVERRESULT
{
	__int64					userUID;		// 유저 DB UID
	char					nServPNum;

	EFinishType				FinishType;
	int						iLeftTurn;				// 남은 턴
	int						iLeftTime;				// 남은 시간

	TMoney					tBasicReward;			// 기본 보상금
	TMoney					tClearReward;			// Clear 보상금
	int						nClearType;				// AI_LOOSE ~ AI_CLEAR_RESORT_MONOPOLY

	// 갱신 해야되는 유저 정보
	TMoney					tUserMoney;				// 현재 머니

	TMoney					tUserGoldTencentVIP;	// 텐센트 VIP 유져인 경우 추가 골드 지급(한도액은 넘지 않는다)
	int						tencentVIPBonusRate;	// 텐센트 VIP 유져인 경우 보너스 00%
};


// 최대 전송가능한 게임결과 갯수
#define MAX_GAME_RESULTS_HISTORY 10

struct GameResultsHistory
{	
	__int64					index;					// db 인덱스
	__int64					userUID[ MAX_PLAYER - 1 ];	// 팀전인 경우에 0번에 팀원 정보가 기록됨, 점수순	
	TMoney					score[ MAX_PLAYER ];  // 마지막에 자신의 점수가 기록됨 팀전인 경우에 0번에 팀원정보가 기록됨
	char					nickName[ MAX_PLAYER - 1 ][ MAX_NICKNAME_LEN + 1 ]; // 팀전인 경우에 0번에 팀원 정보가 기록됨, 점수순
	bool					bankrupt[ MAX_PLAYER ]; // 마지막에 자신이 파산했는지 기록됨, 팀전인 경우 0번에 팀원 정보가 기록됨
	bool					win;						// 승리 여부
	char					teamFlag;					// 0 싱글, 1 red, 2 blue
	__int64					gameEndDate;
};


struct GameResultsHistoryServerRecord : public GameResultsHistory
{
	__int64					myUserUID;
};

enum eBookmarkFriendType
{
	eBookmarkFriendType_delete = 0,
	eBookmarkFriendType_RegisteredSomeone,	// 상대방 등록
	eBookmarkFriendType_Denied,				// 상대방 거부
	eBookmarkFriendType_ImRegistered,		// 내가 등록
	eBookmarkFriendType_EachRegistered,		// 서로 등록
};

// 북마크 친구 정보
struct BookMarkFriendInfo
{
	__int64					friendUserUID;	// 친구 DB번호
	char					snsUID[ MAX_SNS_UID_LEN + 1 ];
	char					friendNickName[ MAX_NICKNAME_LEN + 1 ];	// 친구 닉네임	
	TMoney					score;		// 총 획득/손실 점수
	int						win;			// 승수
	int						lose;			// 패수
	int						friendType;		// eBookmarkFriendType 참조
	__int64					regDate;		// 등록 날짜
	TMoney					weekScore;		// 주간 점수
	int						connectNO;		// DB 접속 정보
	int						equipCardItemInfoID;	//	장착한 케릭터 번호
	int						equipSkinItemInfoID;		// 장착한 Skin 번호
	USER_OPTION				userOption;
};


// 내가 등록 할 수 있는 최대 즐겨찾기 친구 수
#define MAX_MY_REGISTERED_COUNT 20

// 나를 등록 할 수 있는 최대 즐겨찾기 친구 수
#define MAX_SOMEONE_REGISTERED_COUNT 60


//  방 옵션 변경 정보 
typedef struct
{
	short   RoomNum;			// 방번호
	short   UNum;				// 방장의 유저 번호
	__int64 userUID;		// 유저 DB UID
	char  Title[MAX_ROOM_TITLE_LEN];        // 방제목
	char  nCard;
	bool  bWhis;
	char  Passwd[5];
	short TotalUnum;
} STYLEROOMINFO;




////////////////////////////////////////////////////////////////////////////////////////
//! 게임 결과 정렬할때 사용함.
typedef struct
{
	int				iServerPNum;	
	char			cTeamNo;
	TMoney			tTotFortune;
	

	bool			bBankruptcyFlag;

	bool			bWinGameFlag;		//FinishType_3Monopoly FinishType_LineMonopoly FinishType_AllTour 에서 승리한사람이냐?
	int				iTurnOrderNum;

	TMoney			tVipTotMoney;

	///////////////////////////////////////////////////////////////////////////
	// 추가 계산할때 사용함
	int				iRank;
		
} stRESULT_SORT;


////////////////////////////////////////////////////////////////////////////////////////
// 그룹 입장 레벨&래더 정보.
typedef struct
{
	int		LimitLevel1;
	int		LimitLevel2;
	TMoney	 tMarbleMoney1;		// 
	TMoney	 tMarbleMoney2;		// 	
} stGROUP_GRADE_INFO;


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

int	GetLevel( TMoney Money_);

double GetWinPro(int winnum, int loosenum);

extern bool IsOnlyEvent(int nKind);


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// 이벤트

typedef struct
{
 	int			iEventFlag;			// 0 : 없음 이벤트 기간이냐, 피크 타임이냐 G_EVENT_이거 참고
	
 	__int64		i64StartDay;		// 1주 이벤트 시작 날짜.
	__int64		i64EndDay;			// 1주 이벤트 끝 날짜.	

	int			iEventGameCode;		// 이벤트 게임 코드.	
	int			iEventCode;			// 이벤트 코드
	int			iEventRound;		// 이벤트 라운드
	int			iEventType;			// 이벤트 타입
		
	int			iEventGetCount;		// 이벤트 얻는 카운트	

	int			iEventEtc1;			// 기타 변수
	int			iEventEtc2;			// 기타 변수
	int			iEventEtc3;			// 기타 변수

} stEVENT_INFO;

#define EVENT_NONE					0		// 이벤트 없음
#define EVENT_PERIOD				1		// 이벤트 기간

typedef struct
{
	short	shEventKind;					// 이벤트 종류
	short	shEventFlag;					// 이벤트 기간이냐, 피크 타임이냐
	
	short	shMyEventCoin;					// 보유 코인 해 	
	short	shGetEventCoin;					// 얻은 코인 해
	short	shGetEventCoinCount;			// XX개면 shGetEventCoin+1
	
	char	cEventMsgKind;					// 메시지 종류	EVENT_MSG_KIND_참고


	short		shEventLandMarkNum[Max_MapKind];			
	

} stEVENT_MSG_DATA;

#define EVENT_MSG_KIND_LOGIN			0		// 로그인 할때
#define EVENT_MSG_KIND_CHANGE_STATE		1		// 상태가 변할때

#define EVENT_MSG_KIND_GET_POINT_1			2		// 코인 얻을때.
#define EVENT_MSG_KIND_GET_POINT_COUNT_1	3		// 코인 카운터 얻을때.









//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
// 메시지를 보낼때 사용하는 구조체.






typedef struct
{
	char cDiceNum0;
	char cDiceNum1;
} TDices;	// 


typedef struct
{
	stGROUP_GRADE_INFO		stGroupGradeInfo[MAX_GROUP];
} stSD_GROUP_GRADE_INFO;	// 

typedef struct
{
	int					iFirstTurnSvrPNum;

} SScDecisionPlayTurn;		// 턴을 결정 한다.

typedef struct
{
	int					iUserNum;
	int					iSelectTurnCardPos;
} SCsDecidedPlayTurn;		// 턴을 결정 한다.
typedef struct
{
	int					iSvrPNum;
	int					iSelectTurnCardPos;
	bool				bSelectedDoneFlag;
} SScDecidedPlayTurn;		// 턴을 결정 한다.







typedef struct
{	
	int						iSvrPNum;
	bool					bEscapeIslandFlag;
	bool					bDoubleAgainFlag;		// 더블 했으면 한번 더~
	int						iThrowDiceCount;				// 몇개의 주사위로 던졌냐?
	int						iSkillEscapeIslandUID;	// 스킬발동(무인도탈출)
} stScDotoThrowDice;			// 주사위 던져라~


typedef struct
{	
	int						iUserNum;	
	float					fDiceGageCount;		// 	
	int						iDiceFuncKind;			// 홀수 아이템이냐 짝수 아이템이냐?
} SCsDoneThrowDice;		// 주사위를 던졌다~ <-


typedef struct
{	
	int						iSvrPNum;
	int						iThrowDiceCount;				// 몇개의 주사위로 던졌냐?
	int						iThrowDiceNum[MAX_THROW_DICE_NUM];

	int						iThrowDiceState;				// 주사위 상태(어디서 던졌냐? 무인도냥 그냥이냥 기타등등)

	bool					bDoubleFlag;

	bool					useDiceBooster;
	bool					applyAbilityDiceGage;
	bool					applyAbilityIslandEscape;

	int						iSkillDiceFirstDoubleUID;		// 스킬(첫턴주사위더블)
	int						iSkillDiceGageUID;				// 스킬(주사위 컨트롤 정확도 향상)
	int						iSkillDiceValueBoosterUID;		// 스킬(첫턴 주사위결과 x2 이동)
	int						iSkillDiceMarbleMoneyUID;		// 스킬(첫주사위 x 마블머니 획득)
	int						iSkillDiceAbilityUID;			// 스킬(주사위 더블확률 증가)
	int						iSkillDiceReverseMoveUID;		// 스킬(고장난 네비게이션)

} SScDoneThrowDice;		// 주사위를 던졌다~ ->

typedef struct				
{
	int						iUserNum;
} stCsDoneThrowDiceEffect;	// 주사위 던짐 연출이 완료됨




typedef struct
{
	int						iSvrPNum;
	int						iNewPos;
	int						iMoveType;
	bool					bReverse;		// true : 역방향   true : 정방향
	int						iGatePos;		// 테마파크 갈림길 위치
} SScMovePlayer;			// 플레이어 이동 시켜라

typedef struct
{
	int						iSvrPNum;
	int						iNewPos;
	int						iType;	// ForceMoveType_Normal
} SScForceMovePlayer;		// 강제로 이동 시킨다.


typedef struct
{
	int						iSvrPNum;		// 기준

	
	
	int						iNewPos[MAX_PLAYER];  // 서버 Pnum인덱스로 되어 있고 -1일될수 있다.

} SScForceMoveSelectPlayer;		// 강제로 이동 시킨다.

typedef struct
{
	int						iUserNum;
} SCsDoneMovePlayer;		// 이동했다



struct SScTax
{
	int						iSvrPNum;
	TMoney					tTotalTax;
	TMoney					tTotFortune;
	int						iSkillFreeTaxUID;	// 스킬(세금면제)
};

struct SCsTaxChecked
{
	int						iUserNum;
};

struct SCsBankruptcy
{
	int iUserNum;
	bool bBankruptcyFlag;
};

struct SScBankruptcy
{
	int				iSvrPNum;					// 파산하는 유저
	PLAYSTATE		PlayState;
	
};



// rso //////////////////////////////////////////////////////////////////////////////////////////
struct SScChangeStructFree
{
	int		iSvrPNum;
	
};

struct SCsChangeStructShow
{
	int		iUserNum;
	int		iRgnNum;		// 선택한 지역번호
	bool	bUseFlag;
};

struct SScChangeStructAsk
{
	int		iSvrPNum;
	int		iRgnNum;		// 건설할 지역번호
	
};

struct SCsChangeStruct
{
	int				iUserNum;
	int				iRgnNum;		// 건설할 지역번호
	bool			bBuildFlag;		// 건설했냐?
	stSTRUCT_CELL	stSturctCell;	// 건축물 상태	
};

struct SScChangeStruct
{
	int				iSvrPNum;
	int				iRgnNum;		// 건설할 지역번호
	stSTRUCT_INFO	stStructInfo;
};

struct SCsPass
{
	int				iUserNum;
	bool			bBuildCancelFlag;			// 건설 취소
};

struct SScPass
{
	int		iSvrPNum;
};

// 유저,서버간 돈이 오가는 생태

#define MONEY_STATE_NONE						0		// 머니 이동 효과 없음.
#define MONEY_STATE_NORMAL						1		// 일반적인거
#define MONEY_STATE_TOLL						2		// 통행료
#define MONEY_STATE_TAX							3		// 세금
#define MONEY_STATE_DONATION					4		// 불우이웃돕기
#define MONEY_STATE_RECV_TAX					5		// 환급
#define MONEY_STATE_STRUCT_SALE					6		// 건물매각금



struct SScPlayMoneyState
{
	int		iSrcSvrPNum;			// -1이면 은행
	TMoney	tSrcPlayMoney;			// 최종금액
	
	int		iDstSvrPNum;			// 최종금액
	TMoney	tDstPlayMoney;			// -1이면 은행
	

	TMoney	tChangedPlayMoney;		// 변화량

	int		iMoneyState;

	float	fDelaySec;				// 효과 줄때 딜레이를 준다.

	int		iSkillUID;
	int		iSkillSvrPNum;
};


struct SScUserMoneyState
{
	int		iSvrPNum;			
	TMoney	tUserMoney;	

	TMoney	tPlayMoney;
	TMoney	tChangedPlayMoney;		// 변화량
	TMoney	tChangedUserMoney;		// 변화량
	bool	bEffectFlag;
	int		iUserMoneyKind;
	bool	useWithDrawBooster;
	float	fDelaySec;				// 효과 줄때 딜레이를 준다.
	TMoney	tWinTotGainMoney;
	int		iSkillUID;				// 스킬적용
};// 유저,서버간 돈이 오가는 생태

struct SScIslandChargeChoose
{
	int			iSvrPNum;			// 무인도에 같힌 유저
	int			iLeftTurnToEscape;	// 무조건 탈출까지 남은 턴 수	
};

struct SCsIslandChargePay
{
	int			iUserNum;
	int			iUseState;			// 0 : 주사위 1 : 돈지불 2 : 탈출 찬스카드 사용
};

struct SScIslandChargePay
{
	int		iSvrPNum;			// 지불한 유저	
};

struct SScSpaceTravelChoose
{
	int		iSvrPNum;			// 세계여행 할 유저	
	int		iSkillFreeSpaceTravelUID;
};

struct SCsSpaceTravel
{
	int		iUserNum;
	int		iRgnNum;			// 이동하고자 하는 지역번호
	bool	bUseFlag;
};

struct SScSpaceTravel
{
	int		iSvrPNum;
	int		iRgnNum;			// 유저가 해당 지역으로 우주여행 함	
};


struct SCsSellForToll		// 통행료를 위한 매각
{
	int		iUserNum;
	bool	bSellFlag;
	bool	bStructSellFlag[MAX_BOARD_BLOCK_NUM];
};

struct SScSellForToll			// 다량 매각 결과
{
	int				iSvrPNum;
	stSTRUCT_INFO	stStructInfo[MAX_BOARD_BLOCK_NUM];	
};



struct SScMustSellForToll
{
	int		iSvrPNum;	
	TMoney	tNeedMoney;
	TMoney  loanGold;
	int		iSkillDiscountLoanUID;

	char	cLoanCount;
};

struct SScTakeOverDecide
{
	int		iSvrPNum;	
};

struct SCsTakeOver
{
	int		iUserNum;
	int		iRgnNum;					// 인수할 지역번호
	bool	bTakeOverFlag;
};

struct SScTakeOver
{
	int			iTakerSvrPNum;				// 인수자 SvrPNum
	int			iTakenSvrPNum;				// 피인수자의 SvrPNum
	int			iRgnNum;					// 지역 번호	
};

struct SScTurnChanged
{
	int			iSvrPNum;			// 차례가 돌아온 유저의 Room SvrPNum
	int			iLeftTurnToTheEnd;	// 게임종료까지 남은 턴
	int			iLeftTime;			// 남은 시간
};

struct SScChooseConstruct
{
};

struct SScChooseWithdrawOrDie
{
	int		iSvrPNum;
};


struct SScDie
{
	int		iSvrPNum;
};

struct SScChanceCardOpen
{
	int		iSvrPNum;
	int		iChanceCardNo;
	bool	applyAbilityChanceCard;
	int		SkillChanceCardUID;		// 스킬(첫번째 획득 황금찬스)
};

struct SScChanceCardGet
{
	int		iSvrPNum;
	int		iChanceCardNo;	
};

struct SCsChanceCardCheck
{
	int		iUserNum;
	bool	bKeep;		// true : 보관
	bool	bUseFlag;

};


struct SScKeepChanceCardUseChoose
{
	int		iSvrPNum;
	int		iChanceCardNo;
	int		attackerChanceCardNo;
	int		iRgnNum;
	char	cState;			// 0 : 통행료 관련, 1: 공격 관련
	TMoney	tPassMoney;		// 해당?도시의 통행료
};


struct SCsKeepChanceCardUse
{
	int		iUserNum;
	int		iChanceCardNo;
	bool	bUseFlag;
};

struct SScKeepChanceCardUse
{
	int		iSvrPNum;
	int		iChanceCardNo;
	int		iRgnNum;

	int		iAttackSvrPNum;			//	공격한 유저
	int		iAttackChanceCardNo;	//	공격한 찬스카드 종류
	int		iSkillUID;				//	스킬
};

struct SScMonopoly
{
	int		iMGNum;		// 독점 그룹 번호
	bool	bMonopoly;	// true : 독점    false : 독점해제

	
};

// 축제 개최

struct SScCreateFestival
{
	int					iRgnNum[MAX_FESTIVAL_NUM];			// 축제 지역번호
	stSTRUCT_INFO		stSturctInfo[MAX_FESTIVAL_NUM];
};




struct SScCreateOlympicChoose
{
	int		iSvrPNum;		// 이벤트 개최할 유저의 PNum	
	int		iNowOlympicCnt;	// 지금 현재 올림릭 개최 숫자
	int		iSkillFreeOlympicOpenUID;	//	스킬(올림픽 개최비용면제)
};

struct SCsCreateOlympic
{
	int		iUserNum;
	int		iRgnNum;		// 이벤트 개최할 지역번호
	bool	bUseFlag;
};

struct SScCreateOlympic
{
	int					iSvrPNum;		// 개최한 유저
	int					iRgnNum;		// 이벤트 개최한 지역번호	

	stSTRUCT_INFO		stSturctInfo;
};

struct SScDeleteOlympic
{
	int					iRgnNum;		// 삭제된 이벤트 번호
	int					iSvrPNum;		// 개최한 주체

	stSTRUCT_INFO		stSturctInfo;

};



struct SCsDiceForce
{
	TDices		Dices;
};



struct SCsAutoPlay
{
	int		iUserNum;
	char	cAutoPlay;

};

struct SScAutoPlay
{
	int		iSvrPNum;
	char 	cAutoPlay;
};

struct SScAttackChoose
{
	int			iGKNum;
	int			iSvrPNum;
	

};
struct SCsAttack
{
	int			iUserNum;
	bool		bAttackFlag;
	int			iGKNum;

	int			iRgnNum;		// 도시 교환 할때 사용
		
	int			iTargetSvrPNum;		// 공격 당할 사람 Pnum
	int			iTargetRgnNum;		// 공격 당할 위치.

	
};

struct stVALID_STRUCT_INFO
{
	bool			bValid;				// true : 변화된 지역,  false : 유효하지 않은 데이터
	stSTRUCT_INFO stStructInfo;
};

struct SScEarthquake
{
	int				iAttackerSvrPNum;
	int				iDefenderSvrPNum;
	int				iRgnNum;
	stSTRUCT_INFO	stStructInfo;
};

struct SScTsunamiWide
{

	int						iAttackerSvrPNum;
	int						iDefenderSvrPNum;
	stVALID_STRUCT_INFO		aValidStructInfo[MAX_BOARD_BLOCK_NUM];

};

struct SScChangeCity
{
	int				iAttackerSvrPNum;
	int				iDefenderSvrPNum;
	int				iMyRgnNum;		// 변경된 후 내 지역번호
	stSTRUCT_INFO	stMyStructInfo;	// 변경된 후 내 지역정보
	int				iOpRgnNum;		// 변경된 후 상대 지역번호
	stSTRUCT_INFO	stOpStructInfo;	// 변경된 후 상대 지역

};

struct SScChangePos
{
	int				iAttackerSvrPNum;
	int				iDefenderSvrPNum;
	int				iMyRgnNum;		// 변경된 후 내 지역번호
	stSTRUCT_INFO	stMyStructInfo;	// 변경된 후 내 지역정보
	int				iOpRgnNum;		// 변경된 후 상대 지역번호
	stSTRUCT_INFO	stOpStructInfo;	// 변경된 후 상대 지역

};

struct SScBlackOut
{
	int				iAttackerSvrPNum;
	int				iDefenderSvrPNum;
	int				iRgnNum;
	stSTRUCT_INFO	stStructInfo;

};

struct SScDisease
{
	int				iAttackerSvrPNum;
	int				iDefenderSvrPNum;
	int				iRgnNum;
	stSTRUCT_INFO stStructInfo;

};



struct SScDiseaseWide
{

	int					iAttackerSvrPNum;
	int					iDefenderSvrPNum;
	stVALID_STRUCT_INFO		aValidStructInfo[MAX_BOARD_BLOCK_NUM];
};

struct SScRgnChanged
{
	int				iSvrPNum;
	int				iRgnNum;
	stSTRUCT_INFO	stStructInfo;

};

struct SScTurnState
{	
	int				iSvrPNum;				// 서버 pnum이 PNUM_NULL이면 모두다.
	int				iTurnState;			// 턴 상태
	int				iTurnLimitSec;			// 턴 제한 시간.
	bool			bStopTimeCheckFlag;		// 클라이언트에서 타임 체크를 하지 말아라(효과 같은거 때문에)
};

struct SCsAddLoan
{
	int				iUserNum;
	bool			bAddLoanFlag;
};


struct SScTransferCity
{
	int				iSvrPNum;
	int				iTargetSvrPNum;
	int				iRgnNum;		// 양도 되는 지역 번호
	stSTRUCT_INFO	stStructInfo;	// 양도 되는 지역 정보

};


struct stAddBookmarkFriend
{
	BookMarkFriendInfo	friendInfo;	// 추가된 친구 정보	
};


struct stDelBookmarkFriend
{
	__int64			friendUserUID;		// 대상 친구 UserID
	unsigned short	bookmarkFriendType; // eBookmarkFriendType
};


#define GAME_NOTIFY_KIND_NONE							0
#define GAME_NOTIFY_KIND_NOT_ENOUGH_SEED_MONEY			1			// 시드머니가 없다.
#define GAME_NOTIFY_KIND_NOT_ENOUGH_MARBLE_MONEY		2			// 마블머니가 없다.
#define GAME_NOTIFY_KIND_DOUBLE_3_PENALTY				3			// 더블 3번했을때.
#define GAME_NOTIFY_KIND_OLYMPIC_NOT_ENOUGH_MONEY		4			// 올림픽 개최 돈이 없다.
#define GAME_NOTIFY_KIND_OLYMPIC_NOT_ENOUGH_CITY		5			// 올림픽 개최 할 도시가 없다.
#define GAME_NOTIFY_KIND_START_BUILD					6			// 출발지 건설
#define GAME_NOTIFY_KIND_NOT_ENOUGH_CITY				7			// 건설 불가 건설할 수 있는 도시가 없다.
#define GAME_NOTIFY_KIND_NOT_ENOUGH_BUILD_MONEY			8			// 건설 불가 돈이 없다.
#define GAME_NOTIFY_KIND_NOT_ENOUGH_MONEY				9			// 돈이 없다.
#define GAME_NOTIFY_KIND_TIME_OVER						10			// 타임오버
#define GAME_NOTIFY_KIND_NOT_ATTACK_CITY				11			// 공격 가능한 도시가 없습니다.
#define GAME_NOTIFY_KIND_NOT_CHANGE_CITY				12			// 교환할 도시가 없습니다.
#define GAME_NOTIFY_KIND_NOT_TRANSFER_CITY				13			// 양도할 도시가 없습니다.
#define GAME_NOTIFY_KIND_NOT_MOVE_TO_OLYMPIC			14			// 이동할 올림픽 개최 도시가 없습니다.
#define GAME_NOTIFY_KIND_BOKBULBOK_NOT_CITY				15			// 복불복 적용할 도시가 없다.
#define GAME_NOTIFY_KIND_TOLL_BY_LOAN					16			// 대출을 받아 통행료를 지불하였습니다
#define GAME_NOTIFY_KIND_NOT_ENOUGH_MONEY_TRAVEL		17			// 비용이 부족하여 세계여행을 할 수 없습니다
#define GAME_NOTIFY_KIND_NOT_MOVE_TO_BLACKHOLE			18			// 발생한 블랙홀이 없다.
#define GAME_NOTIFY_KIND_TAKEOVER_NOT_ENOUGH_MONEY		19			// 인수할 돈이 없다
#define GAME_NOTIFY_KIND_NOT_MOVE_TO_PARADE_CAR			20			// 퍼레이드카가 없다.



struct SScGameNotify
{
	int				iSvrPNum;
	int				iGameNotifyKind;
};

typedef struct
{
	int				iSvrPNum;
	stGK_STATE		stGKState;		// 유저한테 적용되는 찬스카드 관련 상태
} SScSendGKState;


#define DICE_FUNC_KIND_NONE				0
#define DICE_FUNC_KIND_ODD				1
#define DICE_FUNC_KIND_EVEN				2

typedef struct
{
	int				iUserNum;
	int				iDiceFuncKind;			// 홀수 아이템이냐 짝수 아이템이냐?
} SCsUseDiceFunc;

typedef struct
{
	int				iSvrPNum;
	int				iDiceFuncKind;			// 홀수 아이템이냐 짝수 아이템이냐?

	char			cDiceItemCnt;

	TMoney			sPoint;
	TMoney			sPoint_P;

	//char			cDiceOddCnt;	// 짝수 주사위 사용할 갯수
	//char			cDiceOvenCnt;	// 홀수 주사위 사용할 갯수
} SScUseDiceFunc;

//////////////////////////////////////////////////////////////////////////
// 마카오

#define MAX_MACAU_OPEN_CARD_NUM				6
#define MAX_MACAU_CARD_NUM					52 // 스프라이트 번호랑 매칭됨.

typedef struct
{
	int				iSvrPNum;	
	int				iOpenCard[MAX_MACAU_OPEN_CARD_NUM];		// 베팅 보여주고 있는카드

} SScBettingMacau;

// 베팅 종류
#define BETTING_KIND_LOW				1		// 낮음.	홀
#define BETTING_KIND_HIGH				2		// 높음		짝


typedef struct
{
	int				iUserNum;
	int				iBettingKind;
	int				iBettingMoneyType;
	bool			bUseFlag;				// 베팅 했냐?	
} SCsBettingMacau;

typedef struct
{
	int				iSvrPNum;
	bool			bSucceedFlag;
	bool			applyAbilityMacauWin;
	int				iOpenedCardNum;
	char			cBettingCount;				// 다시 베팅
	int				iBettingMoneyType;
	int				iBettingKind;
	bool			bStopFlag;
	TMoney			tGainMoney;
	int				iSkillUID;
} SScBettingMacauResult;

//////////////////////////////////////////////////////////////////////////
// 강원

#define MAX_KANGWON_DICE_HISTORY_NUM		6

typedef struct
{
	int				iSvrPNum;	
	__POINT			stDiceHistory[MAX_KANGWON_DICE_HISTORY_NUM];		// 주사위 기록.
} SScBettingKangwon;

typedef struct
{
	int				iUserNum;
	int				iBettingKind;
	int				iBettingMoneyType;
	bool			bUseFlag;				// 베팅 했냐?	
} SCsBettingKangwon;

typedef struct
{
	int				iSvrPNum;
	bool			bSucceedFlag;	
	bool			applyAbilityWin;
	__POINT			stDiceNum;
	char			cBettingCount;				// 다시 베팅
	int				iBettingMoneyType;
	int				iBettingKind;
	bool			bStopFlag;
	TMoney			tGainMoney;
	int				iSkillUID;
} SScBettingKangwonResult;



typedef struct
{
	int				iUserNum;
	char			cSelectTeamNo;
} SCsSelectTeam;

typedef struct
{
	char			cTeamFlag;
	int				cTeamNo[MAX_PLAYER];
	int				cTeamColor[MAX_PLAYER];
} SScSelectTeam;

typedef struct
{
	int				iSvrPNum;
	int				iPlayerPos[MAX_PLAYER];				// 서버 Pnum기준으로 되어 있음.
	short			shRoundCnt[MAX_PLAYER];				// 몇바퀴 돌았냐
	stSTRUCT_INFO	stStructInfo[MAX_BOARD_BLOCK_NUM];
	int				iNowTurnServerPnum;

} SSvSendGameInfo;

typedef struct
{
	int				iUserNum;
	int				itemInfoUID;
	char			szNickName[MAX_NICKNAME_LEN];		// Character 생성시, Nick Name도 함께 생성한다
} SCsCreateGameChar;

typedef struct
{
	int				iResult;							// 결과.
	char			szNickName[MAX_NICKNAME_LEN];		// Character 생성시, Nick Name도 함께 생성한다
} SScResultCreateGameChar;

typedef struct
{
	int				iUserNum;
} SCsMarbleMoneyStateReq;



typedef struct
{
	int				iUserNum;
	char			cReadyState;

	int				useODDItem;
	int				useDiceBooster;
	int				useWithDrawBooster;
} SCsReadyState;

typedef struct
{
	int				iSvrPNum;
	char			cReadyState;

	bool			successInstantItem;

	int				useODDItem;
	int				useDiceBooster;
	int				useWithDrawBooster;
} SScReadyState;


typedef struct
{
	int				iSvrPNum;
	int				iRgnNum;
} SScGameFinishWarnning;

typedef struct
{
	int				iUserNum;
	char			cTeam;	
} SCsChangeSingleTeam;

typedef struct
{
	int				iUserNum;
	int				itemInfoUID;
	int				iBuyNum;
	bool			bSale;				// Sale 여부(2013.10.10) - 현재
	bool			bCoin;				// 2013.11.26 - 골드로 구입 할 수 있는 것을 다야로 구입
} SCsBuyMarbleItem;


// 텐센트용 아이템 구매
struct SCsBuyMarbleItemTencent : public SCsBuyMarbleItem
{
	TENCENT_CASH_TOKEN m_token;
};


// #define BUY_MARBLE_ITEM_RESULT_OK			0
// #define BUY_MARBLE_ITEM_RESULT_NO_MONEY		1

typedef struct
{
	int				iUserNum;
	__int64			ownedItemUID;
} SCsDeleteMarbleItem;

typedef struct
{
	int				iUserNum;
	int				itemType;
	__int64			ownedItemUID;
} SCsEquipMarbleItem;

typedef struct
{
	int				iUserNum;
	int				equip;			// 장착여부 1: 장착, 0: 해제 (BOOL)
	__int64			ownedSkinItemUID;
} SCsEquipSkinMarbleItem;

//////////////////////////////////////////////////////////////////////////
// 조합.
typedef struct
{
	int				iUserNum;
	__int64			MixOwnedItemUID[MAX_MARBLE_ITEM_MIX_NUM];
	bool 			luckyMix;

	bool			cardUpgrade;

} SCsMarbleItemCardMix;


// 텐센트용
struct SCsMarbleItemCardMixTencent : public SCsMarbleItemCardMix
{
	TENCENT_CASH_TOKEN m_token;
};


typedef struct
{
	__int64			deleteOwnedItemUID[MAX_MARBLE_ITEM_MIX_NUM];
	stMARBLE_ITEM   mixAddMarbleItem;

	bool luckyMix;

	bool			cardUpgrade;
	
} SScResultMarbleItemCardMix;

//////////////////////////////////////////////////////////////////////////
// 강화


// 주사위 강화
typedef struct
{
	int				iUserNum;
	__int64			ownedItemUID;

} SCsMarbleItemDiceEnchant;

// 주사위 강화 결과
typedef struct
{
	stMARBLE_ITEM enchantOwnedItemInfo;

} SScMarbleItemDiceEnchant;

typedef struct
{

	int				iSvrPNum;
	int				iAbilityKind;
	int				iAbilityPercent;
	TMoney			tDiscountMoney;	
} SScAbilityDiscountEffect;

typedef struct
{
	int				iUserNum;
	int				iPlayerInfoUserNum;
} SCsPlayerInfo;

typedef struct
{
	USERINFO					stUserInfo;
	int							iRoomNum;
	short						shPosition;
} SScPlayerInfo;


#define MAX_ALL_SERVER_MSG_LEN		100
typedef struct
{
	char		cAllServerMsg[MAX_ALL_SERVER_MSG_LEN];
	__int32		stTextColor;
} SMSAllServerMsg;

typedef struct
{
	int				iUserNum;
	char			cMarbleNickName[MAX_NICKNAME_LEN];
} SCsChangeNickName;

typedef struct
{
	int				iResult;	
} SScResultChangeNickName;
//////////////////////////////////////////////////////////////////////////
// 맵 변경
typedef struct
{
	int				iUserNum;
	int				iMapKind;
} SCsChangeMap;

typedef struct
{	
	int				iMapKind;
} SScChangeMap;

//////////////////////////////////////////////////////////////////////////
// 복불복


typedef struct
{	
	int				iSvrPNum;
	int				iBokbulbokCardNO;
	int				iBokbulbokTargetNo;
	char			cBokbulbokApplySvrPNum[MAX_PLAYER];
} SScBokbulbokCardOpen;

typedef struct
{	
	int				iUserNum;
} SCsBokbulbokCardOk;

/*
typedef struct
{	
	int				iSvrPNum;
	int				iRgnNum;
	int				iBokbulbokCardNo;
} SScBokbulbokSetCard;
*/

typedef struct
{	
	int				iSvrPNum;	
} SScBokbulbokTargetMakeStart;

/*
typedef struct
{	
	int				iUserNum;
} SCsBokbulbokTargetMakeStartOk;

typedef struct
{	
	int				iSvrPNum;			
} SScBokbulbokTargetMakeStartOk;*/


typedef struct
{	
	int				iUserNum;
} SCsBokbulbokTargetMakeOk;

/// 모바일 친구 랭킹 정보
typedef struct _UserScoreInfo
{
	int				DBConnectID;	
	int				level;
	__int64			userUID;
	__int64			currentScore;
	__int64			lastWeekScore;
	__int64			updateDateTime;
	bool			playLastWeek;
	bool			bQuiescence;				// 유휴 유저 여부
	USER_OPTION		OPTION;
	char			snsUID[MAX_SNS_UID_LEN + 1];

	_UserScoreInfo() { memset( snsUID, 0, sizeof(snsUID) ); DBConnectID = NULL_VALUE; userUID = 0; currentScore = 0; lastWeekScore = 0; level = 0; updateDateTime = 0; playLastWeek = false; memset(&OPTION, 0, sizeof(USER_OPTION)); bQuiescence = 0; }
} UserScoreInfo;


typedef struct _UserScoreInfoEX : public _UserScoreInfo
{
	int					winNum;
	int					loseNum;
	int					teamWinNum;
	int					teamLoseNum;
	int					rank;
	signed long long	currentWeekScore;   // 주간 누적 최고 점수
	signed long long	currentWeekScore2;  //한판 최고 점수
}UserScoreInfoEX;


typedef struct 
{
	int				nUNum;								// Notify를 받아야 하는 대상
	__int64			nUserUID;							// Notify를 받아야 하는 대상
	UserScoreInfo	INFO;
}SCORE_INFO_TYP;


// szFriendUID를 기준으로 Packet Data최소화를 할 수 있기는 하나,
// 이부분은 packet 압축에 의해서 큰 문제가 되지 않을 가능성이 높기 때문에
// 기준 Data 최소화는 진행하지 않는다.
typedef struct 
{
	enum
	{
		STATUS_LOGIN = 0,
		STATUS_LOGOUT,
	};

	int		nUNum;										// Notify를 받아야 하는 대상
	__int64	nUserUID;									// Notify를 받아야 하는 대상
	__int64	nFriendUID;									// Login한 친구 UID
	unsigned char nStatus;								// 상태(STATUS_LOGIN, STATUS_LOGOUT)
}FRIEND_STATE_TYP;

/// SPoint 수신 거부 알림 정보
typedef struct _REQ_USEROPTION
{
	__int64		nUserUID;			// 요청자
	USER_OPTION	OPTION;
}REQ_USEROPTION;

typedef struct _Result_SPointInfo
{
	int				nUNum;				// 대상자의 UNum
	__int64			nDestUserUID;		// 대상자
	REQ_USEROPTION	REQOPTION;
}RES_USEROPTION;

typedef struct _MailNotify
{
	__int64 nUserUID;			// 대상자
}MailNotify;

typedef struct _Result_MailNotify
{
	__int64 nUserUID;			// 대상자
	int		nUNum;				// 대상자의 UNum
}Result_MailNotify;

typedef struct _SPointNotify
{
	__int64 nUserUID;			// 요청자
	__int64 nDestUserUID;		// 대상자
}SPointNotify;

typedef struct _Result_SPointNotify
{
	__int64 nUserUID;			// 요청자
	int		nUNum;				// 대상자의 UNum
	__int64 nDestUserUID;		// 대상자
}Result_SPointNotify;


/// 선물한 소셜 포인트 목록
typedef struct _SendSPointTable
{
	__int64 uID;
	int recvDBConnectID;
	__int64 recvUserUID;
	__int64 limitDatetime;
} SendSPointInfo;

typedef struct
{
	unsigned char		nLogKind;					// LOG_KIND_RANK_REWARD ~ LOG_KIND_EVENT_REWARD
	int					nWeekWin;					// 주간 승수
	int					nWeekLose;					// 주간 패배수
	int					nTotalFriend;				// 친구수
	int					nRank;						// 순위
	signed long long	nRP;						// 주간 RP
}LOG_REWARD_INFO;

typedef struct
{
	bool			bSuccess;
	int				nItemUID;					// 보상 받을 Item 정보
	int				nBoxType;					// 보상 받을 Box 정보
	int				nTotalPlay;					// 전주 주간 전체 판수
	__int64			nPlayRewardGold;			// 전주 주간 보상 골드
}RESULT_REWARD_INFO;

typedef struct
{
	bool			bSuccess;
	int				nRank;					// 전주 주간 랭킹
	//int				nTotUserLastWeek;		// 전주 전체 유저수
	int				nCurrencyType;			// 화폐종류
	__int64			nCurrencyValue;			// 화폐의 양
	int				nItemUID1;				// 보상 받을 Item 정보
	int				nItemUID2;				// 보상 받을 Item 정보
}RESULT_ALLRANK_REWARD_INFO;

/// 선물 받은 소셜 포인트 목록
typedef struct _ReceiveSPointTable
{
	__int64 uID;
	int sendDBConnectID;
	__int64 sendUserUID;
	char nickName[MAX_NICKNAME_LEN + 1];
	__int64 receiveDatetime;
	__int64 timeOutDatetime;
	bool isThanks;
} ReceiveSPointInfo;


typedef struct _RankRewardRequestInfo
{
	__int64	nUserUID;
	int		nDBConnectID;
	int		nSnsType;
	int		nTotalFriend;
	int		nRank;
	__int64	nRP;
	char	snsUID[MAX_SNS_UID_LEN+1];
}RankRewardRequestInfo;

typedef struct _AllRankRewardRequestInfo
{
	__int64	nUserUID;
	int		nDBConnectID;
	int		nSnsType;
	int		nAllRank;
	int		nTotUserLastWeek;		// 전주 전체 유저수
	char	snsUID[MAX_SNS_UID_LEN+1];
}AllRankRewardRequestInfo;

/// 메일 박스 정보
typedef struct _MailBoxInfo
{
	int DBConnectID;
	__int64 uID;

	__int64 recvUserUID;

	char snsUID[MAX_SNS_UID_LEN + 1];
	char nickName[MAX_NICKNAME_LEN + 1];
	char message[MAX_MAIL_MESSAGE_LEN + 1];

	__int64 sendDate;
	__int64 limitDate;
	int itemInfoUID;
	int currencyType;
	__int64 currencyValue;
	int mailBoxKind;
	int boxType;
	int skillStoreIndex;
} MailBoxInfo;

typedef struct _IAPTencentBillingInfo
{
	__int64 userUID;
	char snsUID[MAX_SNS_UID_LEN + 1];
	int snsType;
	int IAPType;	

	char sessionID[MAX_SESSIONID_LEN + 1];
	char sessionType[MAX_SESSIONTYPE_LEN + 1];	
	char pf[MAX_PF_LEN + 1];
	
	TENCENT_CASH_TOKEN token;
	
} IAPTencentBillingInfo;

/// 인앱 구조체
typedef struct _IAPBillingInfo
{
	__int64 VK;
	__int64 userUID;
	__int32 DBConnectID;

	char snsUID[MAX_SNS_UID_LEN + 1];
	char snsNickName[MAX_NICKNAME_LEN + 1];
	int snsType;

	__int32 IAPType;
	int  itemInfoUID;
	int	 currencyType;
	__int64	 currencyValue;
	char orderID[MAX_IAP_ORDERID_LEN+1];
	char productID[MAX_IAP_PRODUCTID_LEN+1];
	bool gift;
	__int64 recvUserUID;
	__int32 recvDBConnectID;
	char price[MAX_STRING_INDEX_LEN];
	bool consume;
} IAPBillingInfo;

// 라인용 인앱 구조체
struct IAPLineBillingInfo
{
	// 010_Line_Game_SDK_for_Android_kr_v2.0.pdf
	// 3.1.2 결제예약 (시퀀스 다이어그램 2.1.1 purchase)
	char Nation[ MAX_NATION_CODE ];	// 지역정보	필수
	char priceLine[ MAX_IAP_PRICE_LEN ];			// 결제 금액
	char currecnyLine[ MAX_IAP_CURRENCY ];			// 결제 화폐 종류
	char remoteip[ MAX_IP_STR ]; // 필수 	
};

/// 친구 정보
typedef struct _Friend_DDRInfo
{
	int			DBConnectID;
	__int64		userUID;
	char		snsUID[MAX_SNS_UID_LEN + 1];
	int			bQuiescence;

	USER_OPTION	OPTION;
} Friend_DDRInfo;

/// 친구 랭킹스코어 정보
typedef struct _FRIEND_SCORE
{
	signed long long	userUID;
	signed long long	CurrentScore;
	signed long long	LastWeekScore;
	signed long long	updateDateTime;
	__int64				rewardTime;
	int					PlayLastWeek;
	int					level;
	int					rank;
	USER_OPTION			OPTION;
}FRIEND_SCORE;

typedef struct _FRIEND_RANK
{
	signed long long	userUID;
	int					DBConnectID;
	int					itemInfoUID;
	int					gradeType;
	int					skinItemInfoUID;
	char				nickName[MAX_NICKNAME_LEN+1];
	int					rank;
	signed long long	score;
}FRIEND_RANK;

typedef struct _FRIEND_RANK_INFO
{
	FRIEND_RANK			INFO;
	char				snsNickName[MAX_NICKNAME_LEN + 1];
	int					iteminfoUID;
}FRIEND_RANK_INFO;

typedef struct _USER_SCORE
{
	signed long long	userUID;
	signed long long	CurrentScore;
	signed long long	LastWeekScore;
	signed long long	updateDateTime;
	__int64				rewardTime;
	int					PlayLastWeek;
	int					level;
	int					winNum;
	int					loseNum;
	int					teamWinNum;
	int					teamLoseNum;
	int					rank;				// 전국 이번주 랭킹
	int					totUserThisWeek;	// 전국 이번주 유저수
	int					rankLastWeek;		// 전국 전주 랭킹
	int					totUserLastWeek;	// 전국 이번주 유저수
	int					rankrewardible;		// 전국 랭킹 보상을 받을 수 있는지에 대한 여부
	signed long long	currentWeekScore;   // 주간 누적 최고 점수
	signed long long	currentWeekScore2;  //한판 최고 점수
	USER_OPTION			OPTION;
}USER_SCORE;

typedef struct _UPDATE_SCORE
{
	signed long long	CurrentScore;
	int					level;
	int					winNum;
	int					loseNum;
	int					teamWinNum;
	int					teamLoseNum;
	int					rank;
	int					MaxUser;
	signed long long	currentWeekScore;   // 주간 누적 최고 점수
	signed long long	currentWeekScore2;  //한판 최고 점수
}UPDATE_SCORE;


typedef struct _UPDATE_USERINFO
{
	int					DBConnectID;
	int					itemInfoUID;
	int					skinItemInfoUID;
	int					gradeType;
	char				nickName[MAX_NICKNAME_LEN+1];
}UPDATE_USERINFO;

//- lorddan(2013/04/17) - Friend 상세 정보
typedef struct _FRIEND_DETAIL_INFO
{
	__int64		userUID;
	char		NickName[MAX_NICKNAME_LEN+1];
	char		Greeting[MAX_GREETING_LEN+1];
	stCHARINFO	charInfo;									// Character 정보
	TMoney		tGold;										// 실제 머니
	TMoney		tMaxWinUserMoney;							// 기록용 최대 승리 머니
	__int64		MaxRankScore;								// 최대 랭크 스코어
	__int64		MaxRankScore2;								// 한번에 딴 최고 랭크 점수(누적아님)

	int			winNum;
	int			loseNum;
	OWNED_SKILL_INFO skillSlotinfo[MAX_SKILL_SLOT];			// 스킬 슬롯 정보
	int			equipskinUID;								// Skin 장착 Item 번호
}FRIEND_DETAIL_INFO;

#define ONEDAY 24*60*60*1000

class CRankScoreInfo
{
public:
	CRankScoreInfo()
	{
		score.userUID = 0;
		score.CurrentScore = 0;
		score.LastWeekScore = 0;
		score.updateDateTime = 0;
		score.PlayLastWeek = 0;
		score.rewardTime = 0;
		score.winNum = 0;
		score.loseNum = 0;
		score.teamWinNum = 0;
		score.teamLoseNum = 0;
		score.currentWeekScore	= 0;
		score.currentWeekScore2	= 0;
		score.level = 0;
		score.rank = 0;
		score.totUserThisWeek = 0;
		score.rankLastWeek = 0;		
		score.totUserLastWeek = 0;	
		score.rankrewardible = 0;
		Cache = 0;
		LastQueryTime = 0;
	};

	~CRankScoreInfo(){};
	USER_SCORE score;
	int     CheckExpire(__int64 time)
	{
		if( time - LastQueryTime > ONEDAY )
		{
			return 0;
		}
		return 1;
	}
	int     Cache;
	__int64 LastQueryTime;
};

typedef struct _WEEK_DATA
{
	int		m_Week;
	__int64  m_WeekStartTime;
	__int64  m_WeekEndTime;
}WEEK_DATA;

typedef struct
{
	char	IP[MAX_DOMAIN_NAME+1];				// 서버 IP
	int		Port;				// 서버 포트 번호
} Rs_ServerLoginInfo;

typedef struct
{
	int		SNum;				// 서버 인식 번호(로그인 서버 번호)
	char	IP[MAX_DOMAIN_NAME+1];				// 서버 IP
	int		Port;				// 서버 포트 번호
} Rs_ServerInfo;

#define			MAX_NOTICE_TITLE			60
#define			MAX_NOTICE_CONTENT			512

enum
{
	MARKET_ISO = 0,
	MARKET_GOGGLE,
};

enum
{
	NOTICE_STATE_INACTIVE = 0,
	NOTICE_STATE_ACTIVE
};

typedef struct _NOTICE_INFO
{
	__int64		NOTICE_SEQ;
	char		TITLE[MAX_NOTICE_TITLE+1];
	char		CONTENT[MAX_NOTICE_CONTENT+1];
	int			KIND;							// 공지종류(0: 문자공지, 1: URL 공지)
	int			STATE;							// 공지 상태(0:비활성, 1: 활성)
	int			MARKET;							// 공개 구분(0:모두, 1: ISO, 2:GOOGLE)
	__int64		START_DATETIME;					// 공지 시작 시간
	__int64		END_DATETIME;					// 공지 종료 시간
}NOTICE_INFO;


#define			MAX_EVENT_TITLE				60
#define			MAX_EVENT_CONTENT			256
#define			MAX_EVENT_SCRIPT			2048


enum
{
	EVENT_STATE_INACTIVE = 0,
	EVENT_STATE_ACTIVE
};



enum
{
	EVENT_KIND_LOGIN = 0,
	EVENT_KIND_PLAY,
	EVENT_KIND_PERIOD,			// 단순 기간 Event. Update가 필요없다.(이벤트 판매용)(단한개만 존재해야함)
	EVENT_KIND_MONEY_INCREASE,	// 게임 승리시 머니 증가 Event. Update가 필요없다. (단한개만 존재해야함)
	EVENT_KIND_INFINITE_PLAY,	// 특정 판수 별로 무한 보상 Event (단한개만 존재해야함)
	EVENT_KIND_TERMDAILYREWARD,	// 기간제 일일보상 로그인시 1회 확인 (단한개만 존재해야함)
	EVENT_KIND_QUIESCENCE,		// 휴면 유저 Event. (단한개만 존재해야함)
	EVENT_KIND_FREETICKET,		// 클로버 전송 Event. (단한개만 존재해야함)
	EVENT_KIND_PROMOTION,		// 마이턴 등 프로모션 용 Event 정의
	EVENT_KIND_DBOPTION_APPLAY,	// DB에 설정한 옵션데이터를 적용한다.
	EVENT_KIND_DBRANDOMBOX_PROBABILITY,	// DB에 설정한 랜덤박스(황금열쇠) 확률을 적용한다.
	EVENT_KIND_DBITEMMODIFYINFO_APPLAY,	// DB에 설정한 아이템 수정 정보를 적용한다.	
	EVENT_KIND_WINPLAY_NORMAL,			// 승리 Event
	EVENT_KIND_WINPLAY_3MONOPLOY,		// 3독점 승리 Event
	EVENT_KIND_WINPLAY_LINEMONOPLOY,	// 라인 독점 승리 Event
	EVENT_KIND_WINPLAY_ALLTOUR,			// All Tour 승리 Event
	EVENT_KIND_BUYCOIN,					// Coin 구입 Event
	EVENT_KIND_GET_LUCKYITEM,			// 행운 아이템 획득
	EVENT_KIND_GET_CHARACTERCARD_APLUS,	// A+ 캐릭터 카드 획득
	EVENT_KIND_GET_CHARACTERCARD_S,		// S 캐릭터 카드 획득
	EVENT_KIND_GET_CHARACTERCARD_SPLUS,	// S+ 캐릭터 카드 획득
	EVENT_KIND_RP_INCREASE,				// 랭킹 포인트 증가
	EVENT_KIND_EXP_INCREASE,			// 경험치 증가
	EVENT_KIND_RANKING_BOAST,			// 랭킹 자랑 새로운 아이템 주기(이벤트)
	EVENT_KIND_GOLD_CLOVER,				// Gold Clover Event
	EVENT_KIND_ALLOW_ACCESS_LIST,		// 중국 특정 접속자만 접속 가능하게
	EVENT_KIND_MAX
};

//- EVENT_KIND_LOGIN, EVENT_KIND_PLAY
enum
{
	EVENT_PROMOTION_NONE = 0,
	EVENT_PROMOTION_CHACHACHA,
	EVENT_PROMOTION_PONG,
	EVENT_PROMOTION_ANIMAL,
	EVENT_PROMOTION_GOGOGO,
	EVENT_PROMOTION_MAGUMAGU,
};

typedef struct _EVENT_INFO
{
	__int64		EVENT_SEQ;
	char		TITLE[MAX_EVENT_TITLE+1];
	char		CONTENT[MAX_EVENT_CONTENT+1];
	char		NICKNAME[MAX_NICKNAME_LEN+1];
	char		SCRIPT[MAX_EVENT_SCRIPT+1];
	int			PROMOTION;						// 일반적인 이벤트는 EVENT_PROMOTION_NONE, 크로스 프로모션
	int			KIND;							// Event 종류
												// - 0:시간접속보상 이벤트
												// - 1:판수 이벤트

	int			STATE;							// Event 상태(0:비활성, 1: 활성)
	int			MARKET;							// 공개 구분(0:모두, 1: ISO, 2:GOOGLE)
	int			CONDITION;						// 조건 수치
	__int64		REWARD_ITEM;					// 보상 Item
	int			CURRENCY_TYPE;
	__int64		CURRENCY_VALUE;					// 금액
	__int64		START_DATETIME;					// EVENT 시작 시간
	__int64		END_DATETIME;					// EVENT 종료 시간	
}EVENT_INFO;


// 기간제 이벤트 정보
struct TERM_EVENT_INFO
{	
	__int64					eventSeq;	
	int						eventKind;	// 이벤트 종류
	int						joinCount;	// 참여 횟수	
	__int64					resetDate;	// 이벤트 리셋되는 날짜	
	__int64					lastJoinDate;	// 마지막 참여 날짜
	int						termCount;	// 주기 횟수	
};



typedef struct _TERM_DAILY_EVENT_REWARD_ITEM_INFO
{
	int		    JOIN_COUNT;					// 출석 횟수
	int			REWARD_ITEM;				// 보상 Item
	int			REWARD_SKILLSTOREINDEX;		// 보상 스킬 스토어 인덱스
	int			CURRENCY_TYPE;				// 금액 타입
	int			PROBABIILTY_TABLE_INDEX;	// 보상 확률 테이블
	char		Display[ MAX_STRING_TERMDAILYREWARDEVENT_VALUE_LEN + 1 ];
	
	
}TERM_DAILY_EVENT_REWARD_ITEM_INFO;


typedef struct _TERM_DEAILY_REWARD_PROBABILITY_TABLE
{
	int		    INDEX;					// 확률 인덱스
	__int64		MIN;					// 최소값
	__int64		MAX;					// 최대값
	int			PERCENT;				// 확률


}TERM_DEAILY_REWARD_PROBABILITY_TABLE;


typedef struct _EVENT_INFO_CL
{
	__int64		EVENT_SEQ;
	char		TITLE[MAX_EVENT_TITLE+1];
	char		CONTENT[MAX_EVENT_CONTENT+1];
	char		NICKNAME[MAX_NICKNAME_LEN+1];
	int			PROMOTION;						// 일반적인 이벤트는 EVENT_PROMOTION_NONE, 크로스 프로모션
	int			KIND;							// Event 종류
												// - 0:시간접속보상 이벤트
												// - 1:판수 이벤트

	int			STATE;							// Event 상태(0:비활성, 1: 활성)
	int			MARKET;							// 공개 구분(0:모두, 1: ISO, 2:GOOGLE)
	int			CONDITION;						// 조건 수치
	__int64		REWARD_ITEM;					// 보상 Item
	int			CURRENCY_TYPE;
	__int64		CURRENCY_VALUE;					// 금액
	__int64		START_DATETIME;					// EVENT 시작 시간
	__int64		END_DATETIME;					// EVENT 종료 시간
}EVENT_INFO_CL;

typedef struct _EVENT_USER_INFO
{
	__int64		uID;							// 0일수 있음
	__int64		EVENT_SEQ;						// event 고유 번호
	int			PROMOTION;						// 프로모션 종류 기본 0
	int			KIND;							// Event 종류
												// - 0:시간접속보상 이벤트
												// - 1:판수 이벤트
	int			ACCOMPLISH;						// 달성도
	int			CONDITION;						// 조건 수치(참조가 되는 값이다. DB에 존재하지 않는다.)
	char		SCRIPT[MAX_EVENT_SCRIPT+1];
}EVENT_USER_INFO;

typedef struct _EVENT_USER_INFO_CL
{
	__int64		uID;							// 0일수 있음
	__int64		EVENT_SEQ;						// event 고유 번호
	int			PROMOTION;						// 프로모션 종류 기본 0
	int			KIND;							// Event 종류
												// - 0:시간접속보상 이벤트
												// - 1:판수 이벤트
	int			ACCOMPLISH;						// 달성도
	int			CONDITION;						// 조건 수치(참조가 되는 값이다. DB에 존재하지 않는다.)
}EVENT_USER_INFO_CL;

// 스토어 이벤트 구조체
typedef struct _EVENT_STORE_INFO
{
	int iteminfoUID;
	int displayOrder;
	int flag_HOT;
	int flag_NEW;
	int sellPriceValue;
	__int64 durationValue;
	int sellPriceRate;
	int rewardItemInfoUID;
	__int64 start_DateTime;
	__int64 end_DateTime;
} EVENT_STORE_INFO;


// 스토어 이벤트 구조체
typedef struct _EVENT_STORE_INFO_SERVER
{
	int iteminfoUID;
	int displayOrder;
	int flag_HOT;
	int flag_NEW;
	int sellPriceValue;
	__int64 durationValue;
	int sellPriceRate;
	__int64 start_DateTime;
	__int64 end_DateTime;
	int rewardItemInfoUID;
	char nickName[MAX_NICKNAME_LEN + 1];
	char message[MAX_MAIL_MESSAGE_LEN + 1];
} EVENT_STORE_INFO_SERVER;

// 스토어 리스트 ON OFF 구조체
typedef struct _ENABLE_STORE_INFO
{
	int iteminfoUID;
	int ISONSALE_GOOGLE;
	int ISONSALE_APPLE;
	int ISGIFT_GOOGLE;
	int ISGIFT_APPLE;
} ENABLE_STORE_INFO;

// 스토어 리스트 ON OFF 구조체
typedef struct _EVENT_ITEM_BUFF_INFO
{
	__int64 uID;
	int iteminfoUID;
	int	BUFF_INDEX[MAX_ITEM_GRADE];
	int	USEABLE_TIME;
	__int64	START_DATETIME;
	__int64	END_DATETIME;
} EVENT_ITEM_BUFF_INFO;

#define			MAX_CHAT_DATA				7200
#define			MAX_CHAT_WATCHER_DATA		512


//- DDR_TB.ACCOUNT_BLOCK Field의 값
enum{
	BLOCK_USEBUG = 1,						// s2253, 정지사유 : 버그악용
	BLOCK_USEHACK = 2,						// s2254, 정지사유 : 불법 프로그램 사용
	BLOCK_ABUSING = 3,						// s2255, 어뷰징
	BLOCK_BADWORD = 4,						// s2256, 욕설
	BLOCK_AD = 5,							// 광고
	BLOCK_IMPERSONATION = 6,				// 운영자 사칭
	BLOCK_UNTRUTH = 7,						// 허위사실 유포
	BLOCK_TRADEGOLDE = 8,					// 현금거래
};


typedef struct _CHAT_REPORT_CL
{
	char	szReporter[MAX_SNS_UID_LEN+1];			// 신고자의 sns UID
	char	szPerson[MAX_SNS_UID_LEN+1];			// 피 신고자의 sns UID
	int		nKind;									// 사유
} CHAT_REPORT_CL;

typedef struct _CHAT_REPORT
{
	char	szReporter[MAX_SNS_UID_LEN+1];			// 신고자의 sns UID
	char	szPerson[MAX_SNS_UID_LEN+1];			// 피 신고자의 sns UID
	char	szChat[MAX_CHAT_DATA+1];				// Chatting 내용
	int		nKind;									// 사유
} CHAT_REPORT;


//////////////////////////////////////////////////////////////////////////
// 우주맵 블랙홀 시스템
//////////////////////////////////////////////////////////////////////////
struct SScBlackHoleChoose
{
	int iSvrPNum;
};

struct SCsBlackHoleChoose
{
	int iUserNum;
	int iRgnNum;
	bool bUseFlag;
};

struct SScBlackHoleCreate
{
	int iWhiteHolePos;
	int iBlackHolePos;
};

//////////////////////////////////////////////////////////////////////////
// 테마파크 (갈림길, 야간개장, 퍼레이드) : (갈림길 변경 제외)
//////////////////////////////////////////////////////////////////////////

struct SScThemeparkBeforeOpenNight	//야간 개장 알림
{
};

struct SScThemeparkOpenNight	//야간 개장
{
};

struct SScThemeparkParadeCarComeOut	//퍼레이드 카 출현
{
	int iBlockPos;
};

struct SScThemeparkParadeCarMove	//퍼레이드 카 이동
{
	int iBlockPos;
};

struct SScThemeparkParadeCarArrival	//퍼레이트 카 방향으로 이동
{
	unsigned char btArrivalCount;
	int iApplyPnum[MAX_PLAYER];
};

struct SScThemeparkGateSelect	//갈림길 설정 요청
{
	int iSvrPNum;
};

struct SCsThemeparkGateSelect	//갈림길 플레이어 설정
{
	int iUserNum;
	int iBlockPos;
};

struct SScThemeparkGateNoti	//갈림길 적용 알림
{
	int iCurPos;
	int iGatePos;
};

//////////////////////////////////////////////////////////////////////////
// AI
//////////////////////////////////////////////////////////////////////////
//- 난이도 
#define			MAX_AI				10

//- Clear 조건
enum{
	AI_LOOSE = 0,
	AI_NOCLEAR = 1,
	AI_CLEAR_TRIPLE_MONOPOLY = 2,
	AI_CLEAR_LINE_MONOPOLY = 3,
	AI_CLEAR_RESORT_MONOPOLY = 4,
	MAX_AI_CLEAR,
};

typedef struct _AI_PLAYER_READ_INFO
{
	int			nDifficulty;					// AI 난이도(AI_BEGIN ~ AI_GOD)
	char		szName[MAX_NICKNAME_LEN];		// Nick Name Size
	int			nItemUID;						// 장착 카드 번호
	int			nItemGrade;						// 장착 카드 등급
	__int64		nInitMoney;						// AI 초기 금액
	__int64		nInitPlayerMoney;				// Player 초기 금액
	int			nBuyResort;						// 관광지 구매 여부
	int			nBuyArea;						// 지역 구입 여부
	int			nTakeoverArea;					// 상대방 지역 인수 여부
	int			nBuildArea;						// 건설여부
	int			nBuildLandMark;					// Land Mark 건설 여부
	int			nWorldTourDest;					// 세계 여행지		-1: 가지 않음, 0: 랜덤, 1이상 대상 지역
	int			bStoreChanceCard;				// Chance Card 보관여부
	int			bEscapeIsland;					// 무인도 통행료 지급 탈출 여부. false = 주사위
}AI_PLAYER_READ_INFO;

enum{
	AI_BUY_RESORT = 0,							// 관광지 구매 여부
	AI_BUY_AREA,								// 지역 구입 여부
	AI_TAKEOVER_AREA,							// 상대방 지역 인수 여부
	AI_BUILD_AREA,								// 건설여부
	AI_BUILD_LANDMARK,							// Land Mark 건설 여부
	MAX_AI_ACTION,
};

typedef struct _AI_PLAYER_INFO
{
	int			nSEQ;							// AI 고유 번호
	int			nDifficulty;					// AI 난이도(AI_BEGIN ~ AI_GOD)
	char		szName[MAX_NICKNAME_LEN];		// Nick Name Size
	int			nAIAction[MAX_AI_ACTION];		
	int			nWorldTourDest;					// 세계 여행지		-1: 가지 않음, 0: 랜덤, 1이상 대상 지역
	bool		bStoreChanceCard;				// Chance Card 보관여부
	bool		bEscapeIsland;					// 무인도 통행료 지급 탈출 여부. false = 주사위
	__int64		nInitMoney;						// AI 초기 금액
	__int64		nInitPlayerMoney;				// Player 초기 금액
	USERINFO	UI;
}AI_PLAYER_INFO;


typedef struct _AI_REWARD_INFO
{
	int			nDifficulty;					// AI 난이도(AI_BEGIN ~ AI_GOD)
	int			nReward[MAX_AI_CLEAR];			// Clear 보상
}AI_REWARD_INFO;


// 랜덤박스 정보
typedef struct _RANDOM_BOX_INFO
{	
	int rateValue;
	int skillStoreIndex;
	int itemInfoUID;
} RANDOM_BOX_INFO;


// 옵션 데이터 정보
typedef struct _OPTION_DATA
{
	int optionType;
	TMoney optionValue;
} OPTION_DATA;


// 기능 OnOff 정보
typedef struct _ONOFF_DATA
{
	int FuncNum;
	bool bOnOff;
} ONOFF_DATA;


// DB에서 읽어온 수정할 아이템 정보
typedef struct _ITEM_MODIFY_INFO
{
	int itemInfoUID;
	int	rewardSPointEXP;
} ITEM_MODIFY_INFO;

typedef struct _SNS_INVITE_FRIEND 
{
	char		szSNSFriendUID[MAX_SNS_UID_LEN+1];
	__int64		nLimitTime;
}SNS_INVITE_FRIEND;



typedef struct _ST_INT64{
	__int64 i64;
} ST_INT64;

union UNION_INT64{
	__int64 i64;
	long l[2];
	short s[4];
	char c[8];
};

typedef struct _TENCENT_QUERY_QUALIFY
{
	int first_save;						// 최초 충전에 만족 여부(1:만족, 0:불만족)
	int wx_pay;							// 위쳇 지불기능 개통 여부(1:개통, 0:미개통)
	int kj_pay;							// 쾌속 결제기능 개통 여부(1:개통, 0:미개통)
	int qualified;						// 자격 있는지 여부(1:있음, 2:자격 없음)
	char qualified_info[256];			// 자격 검증 정보
	int is_show_act_page;				// 이벤트 페이지 팝업 여부
	char discounttype[32];				// 이벤트 유형, sdk로 전송 필요
	char discounturl[256];				// 이벤트 URL, sdk로 전송 필요

	void Reset()
	{
		first_save = 0;
		wx_pay = 0;
		kj_pay = 0;
		qualified = 2;
		is_show_act_page = 0;
		memset(qualified_info, 0, sizeof(qualified_info)); 
		memset(discounttype, 0, sizeof(discounttype)); 
		memset(discounturl, 0, sizeof(discounturl)); 
	}
}TENCENT_QUERY_QUALIFY;

typedef struct _TENCENT_PAY_RESULT
{
	int provide_result;					// 송품 결과(1:송품성공, 0:송품실패/송품하지 않았음)
	int present_flag;					// 지급(0:지급하지 않았음. 1:지급하였음, -1:지급실패)
	int present_result;					// 결과 지급(1:개통, 0:미개통)
	char goods_names[128];				// 지급 물품 명칭
	char goods_num[8];					// 지급 물품 수령
	char present_info[256];				// 지급 정보

	void Reset()
	{
		provide_result = 0;
		present_flag = 0;
		present_result = 0;
		memset(goods_names, 0, sizeof(goods_names)); 
		memset(goods_num, 0, sizeof(goods_num)); 
		memset(present_info, 0, sizeof(present_info)); 
	}
}TENCENT_PAY_RESULT;

typedef struct tencentBridgeIDIPHeader	//랭킹서버를 거쳐가는 데이터
{
	unsigned int	uiSeqid;
	unsigned int	uiVersion;						
	char			szServiceName[16];			//ServerCommy.h 의 SERVICE_NAME_LENGTH 의 사이즈가 변경되면...
	char			ucAuthenticate[32];			//ServerCommy.h 의 AUTHENTICATE_LENGTH 의 사이즈가 변경되면...
	int				nSid;
	char			szSnsUID[MAX_SNS_UID_LEN];
}TENCENT_BRIDGE_IDIP_HEADER ;



class CBufferUnit
{
public:
    bool m_bUse;
    int m_Size;
    char *m_pBuf;
};

typedef struct _PACKETHEADER {
	char MsgID[2];
	short Signal;
	int MsgLen;
	char Encode;
	char Compress;
	unsigned short PackCnt;
	unsigned int PackCRC;
} PACKETHEADER;

class CCommMsg
{
public:
	struct VSTRING
	{
		short *pbufsize;
		char *pstr;
	};

public:
	int TotalSize;
	char *pData;
	PACKETHEADER *pHead;
	static int CompressReservedType;
	static int CompressSizeLimit;


protected:
	CBufferUnit *pBufUnit;
	int NowOffset;
};



class CCS_CHAT_REPORT : public CCommMsg
{
public:
	CHAT_REPORT_CL			*m_pInfo;
	short					*m_nLen;
	char					*m_szChat;		// MAX_CHAT_DATA
};

#pragma pack( pop )

#endif