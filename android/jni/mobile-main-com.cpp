 
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <android/log.h>
#include "mobile-main-com.h" 

#include "android-injector.h" 

typedef struct
{
	void * pHookedAddr;
	int nNum;
	char szHookedlib[64];
}HookParamStruct ,*PHookParamStruct;

typedef struct
{
	int client_fd;
	int sockfd; 
	struct sockaddr_in client_addr;
	void * pcom;
}thread_param,*pthread_param;

// list current running process by travelling /proc/<pid>/cmdline directory
int MOBILE_MAIN_COM::list_process(char * szprocess_list)
{
	int id;
	pid_t pid = -1;
	DIR* dir;
	FILE *fp;
	char filename[32];
	
	
	struct dirent * entry; 
	dir = opendir("/proc");
	if (dir == NULL)
	{
		printf("dir == NULL\n");
		return -1;
	}
	
	int ilistLen = 0;
	char * pStart = szprocess_list;
	while((entry = readdir(dir)) != NULL) {
		id = atoi(entry->d_name);
		if (id != 0)
		 {
			sprintf(filename, "/proc/%d/cmdline", id);
			fp = fopen(filename, "r");
			if (fp) 
			{
				char cmdline[256] = {0};
				fgets(cmdline, sizeof(cmdline), fp);
				fclose(fp); 
				if (strlen(cmdline) <= 0)
				{
					continue;
				}
				char szProcessInfo[512]={0};
				sprintf(szProcessInfo, "[%d]%s|", id,cmdline); 
				printf("%d---%s\n",strlen(cmdline), szProcessInfo); 
				memcpy(pStart,szProcessInfo,sizeof(szProcessInfo));
				pStart +=strlen(szProcessInfo);
			}
		}
	} 
	closedir(dir);
	return pid;
}
 // thread for client connnection
void * MOBILE_MAIN_COM::thread_main(void *param)
{
	MOBILE_MAIN_COM * pmain_com = (MOBILE_MAIN_COM *)param;
	int sockfd;
	struct sockaddr_in s_add;
	int sin_size;
	unsigned short portnum = 20087;
	printf("Hello,welcome to my server : port = %d!\r\n",portnum);
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(-1 == sockfd)
	{
		printf("socket fail ! \r\n");
		exit(1);
	}

	printf("socket ok !\r\n");

	bzero(&s_add,sizeof(struct sockaddr_in));
	s_add.sin_family=AF_INET;
	s_add.sin_addr.s_addr=htonl(INADDR_ANY);
	s_add.sin_port=htons(portnum);

	if(-1 == bind(sockfd,(struct sockaddr *)(&s_add), sizeof(struct sockaddr)))
	{
		printf("bind fail !\r\n");
		exit(1);
	}
	printf("bind ok !\r\n");

	// aysn
	int yes = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
	{
		perror("setsockopt");
		exit(1);
	}
	if(-1 == listen(sockfd,5))
	{
		printf("listen fail !\r\n");
		exit(1);
	}
	printf("listen ok\r\n");

	fd_set readfds;
	struct timeval tv ={0,50};

	while(1)
	{
		sleep(1);
		pthread_t  id;
		int ret;
		struct sockaddr_in client_addr;
		thread_param tparam ;

		sin_size = sizeof(struct sockaddr_in);

		FD_ZERO(&readfds);
		FD_SET(sockfd,&readfds);
		//printf("begin select \r\n");
		int iret = select(sockfd+1,&readfds,NULL,NULL,&tv);
		//printf("iret = %d \t",iret);
		if (-1 == iret)
		{
			pmain_com->m_bsend = 0;
			break;					//select failed 
		}
		else if (0 == iret)
		{
			continue;				// again check fd
		}
		else
		{
			int client_fd = accept(sockfd, (struct sockaddr *)(&client_addr), &sin_size);
			printf("client-fd= %d\r\n",client_fd);
			char szecho_welcome[64] = "good baby from server";
			pmain_com->send_msg_packet(TRANS_CMD_ECHO,szecho_welcome,strlen(szecho_welcome),client_fd,0);
			 
			if (client_fd <= 0)
			{
				perror("accept");
				break;;
			}
			tparam.client_fd = client_fd;
			tparam.client_addr = client_addr;
			tparam.sockfd = sockfd;
			tparam.pcom = pmain_com;

			ret = pthread_create(&id,NULL,&thread_sub,&tparam);
			if(ret!=0)
			{
				printf  ("Create pthread error!\r\n");
				break;
			} 
			printf("accept ok!\r\nServer start get connect from %#x : %#x\r\n",ntohl(client_addr.sin_addr.s_addr),ntohs(client_addr.sin_port));
		} 
	}
	pthread_exit(0);
	printf  ("shutdown server......!\r\n");
}

// thread for one client from pc
void * MOBILE_MAIN_COM::thread_sub(void *param)
{
	 thread_param *ptparam =(thread_param *)param;
	 MOBILE_MAIN_COM *pmain_com = (MOBILE_MAIN_COM *)ptparam->pcom; 
	int irecvbytes; 
	int client_fd;  
	char buf[BUFFER_SIZE] = {0};
	char * pmsg_body = NULL;
	int imsg_type = 0;
	client_fd = ptparam->client_fd;
	pmain_com->m_to_client_fd = client_fd;

	printf("thread_sub --> client-fd= %d\r\n",client_fd); 
	// aysn
	int yes = 1;
	if (setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)  
	{
		perror("setsockopt");
		exit(1);
	}

	while(1)
	{  
		fd_set readfds; 
		struct timeval tv ={0,300*1000}; 
		int ret; 
		struct sockaddr_in client_addr; 
		thread_param tparam ;

		FD_ZERO(&readfds); 
		FD_SET(client_fd,&readfds);   
		int iret = select(client_fd+1,&readfds,NULL,NULL,&tv); 
		if (-1 == iret)
		{
			pmain_com->m_bsend = 0;
			break;					// select error 
		}
		else if (0 == iret)
		{
			continue;				// again check fd
		}
		else
		{
			irecvbytes = recv(client_fd, buf, BUFFER_SIZE, 0);
			if (irecvbytes <= 0)
			{
				break;				// select error
			} 
			else
			{
				printf("len: %d,  recv: %s\r\n",irecvbytes,buf); 
 				int imsg_body_len = 0;
				
				imsg_body_len = pmain_com->recv_msg_packet(buf,irecvbytes,client_fd,&imsg_type);
				
				if ((EXIT_CLIENT == imsg_type) || (SHUTDOWN_SERVER == imsg_type))
				{
					pmain_com->m_bsend = 0;
		 			break;
				} 
			} 
		} 
	} 
	close(client_fd); 
	if (SHUTDOWN_SERVER == imsg_type)
	{
		close(ptparam->sockfd);
		printf("shutdown server\r\n");  
	}
	printf("close the remote client\r\n");
	pthread_exit(0);

} 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// below are some functions about communication 
// send raw data directly by send API
int MOBILE_MAIN_COM::send_raw_data(int imsg_type,char * pmsg_body,int ibody_len,int fd_client)
{ 
	int iraw_len = ibody_len+sizeof(raw_packet);
	char * praw_packet  = (char*)malloc(iraw_len);
	memset(praw_packet,0,iraw_len);
	raw_packet *praw_head = (raw_packet*)praw_packet;
	praw_head->imsg_type = imsg_type;
	praw_head->ibody_len = ibody_len;
	if (ibody_len > 0)
	{
		memcpy(praw_packet+sizeof(raw_packet),pmsg_body,ibody_len);
	} 
	
	int iret = send(fd_client,praw_packet,iraw_len,0);
	//printf("imsg_type = %d,iret= %d \n", imsg_type,iret);
	if (praw_packet)
	{
		free(praw_packet);
		praw_packet = NULL;
	}
	usleep(10000);
	return iret;
}
// send data according to  begin ,end and body of msg data
int MOBILE_MAIN_COM::transmit_data(int imsg_type,char * pmsg_body,int ibody_len,int client_fd)
{
	//begin
	send_raw_data(DATA_TRANSMIT_BEGIN,NULL,0,client_fd);  
	int iSendCount = 512;
	int isend_times = ibody_len/iSendCount;
	int iremain_count = ibody_len%iSendCount;
	char * pmsg_begin = pmsg_body;
	printf("isend_times = %d,iremain_count = %d\n", isend_times,iremain_count);
  
	int i = 0;
	for (i = 0; i < isend_times; ++i)
	{
		printf("send-NO %d\n", i);
		send_raw_data(DATA_TRANSMIT,pmsg_begin+i*iSendCount,iSendCount,client_fd);
	}

	if (iremain_count > 0)
	{
		printf("send-ramains =%d\n", iremain_count); 
		char sz_test_str[] = "what a big suprise";
		send_raw_data(DATA_TRANSMIT,pmsg_begin+isend_times*iSendCount,iremain_count,client_fd); 
	} 
	//end
	send_raw_data(DATA_TRANSMIT_END,NULL,0,client_fd); 
	return 1;
} 
// build msg packet and call transimit_data
int MOBILE_MAIN_COM::send_msg_packet(int imsg_type,char * pmsg_body,int ibody_len,int client_fd,int target_fd)
{ 
	int imsg_len = sizeof(msg_packet)+ibody_len;
	{ 
		char * pmsg_data = (char *)malloc(imsg_len); 
		// build head
		msg_packet head;
		head.imsg_type = imsg_type;
		head.ibody_len = ibody_len; 
		 
		memcpy(pmsg_data,&head,sizeof(msg_packet));  
		// build body
		void * pbegin = pmsg_data + sizeof(msg_packet);
		memcpy(pbegin,pmsg_body,ibody_len); 
		// send msg
		transmit_data(imsg_type,pmsg_data,imsg_len,client_fd);
		free(pmsg_data);
		pmsg_data = NULL;
	}
	return imsg_len;
} 
// recv msg packet from pc
int MOBILE_MAIN_COM::recv_msg_packet(char * pmsg_data,int imsg_len,int client_fd,int *pmsg_type)
{ 
	msg_packet head;
	memcpy(&head,pmsg_data,sizeof(msg_packet));  
	char *pmsg_body = pmsg_data + sizeof(msg_packet);
	int imsg_type = 0;
	if (imsg_len == head.ibody_len + sizeof(msg_packet))
	{
		imsg_type =  head.imsg_type;
		if (GET_PROC_LIST == imsg_type)				// get device process list
		{ 
			char * pszprocess_list = (char*)malloc(1024*10);
			memset(pszprocess_list,0,1024*10);
			list_process(pszprocess_list); 
		 	send_msg_packet(GET_PROC_LIST,pszprocess_list,strlen(pszprocess_list),client_fd,0);
		}
		else if(INJECT_PROCESS == imsg_type)		// inject so file to target process
		{
			pid_t target_pid = head.iparams[0]; 
			long uladdr = 0;
			if (-1 != target_pid) 
			{
				ANDROID_INJECTOR inj;
				HookParamStruct hookparam;
				hookparam.pHookedAddr = 0;
				hookparam.nNum = 10;
				strcpy(hookparam.szHookedlib, "/data/local/tmp/libmainso.so"); 
				inj.inject_remote_process(target_pid, "/data/local/tmp/libmainso.so","hook_entry", (const char*) &hookparam, sizeof(hookparam),0); 
			}  
		} 
		else										// unknown command from pc
		{
			char sz_unkown_cmd[] = "unknown command";
			send_msg_packet(TRANS_CMD_ECHO,sz_unkown_cmd,strlen(sz_unkown_cmd),client_fd,0);
		}

		printf("len: %d,  data: %s\r\n",head.ibody_len,pmsg_body);
	}
	else
	{
		printf("len: %d,  error body\r\n",head.ibody_len);
	} 
	*pmsg_type = head.imsg_type;
	return head.ibody_len;
}
// init mobile-main-com
void MOBILE_MAIN_COM::init_com()
{ 
	int ret = pthread_create(&m_main_thread_id, NULL, &thread_main, this);
	if (ret != 0)
	{
		printf("Create main thread error!\r\n");
		return ;
	}
}