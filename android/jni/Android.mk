LOCAL_PATH := $(call my-dir)

######mainso######

include $(CLEAR_VARS)

LOCAL_LDLIBS	+=	-L$(SYSROOT)/usr/lib -llog 

LOCAL_CPP_EXTENSION	:=	.cpp

LOCAL_MODULE	:= libmainso

LOCAL_SRC_FILES	:= mainso.cpp
LOCAL_SRC_FILES	+= android-hook.cpp
LOCAL_SRC_FILES	+= mobile-so-com.cpp

LOCAL_ARM_MODE	:= arm

include $(BUILD_SHARED_LIBRARY)

#####mainproc#####

include $(CLEAR_VARS)

LOCAL_LDLIBS	+=	-L$(SYSROOT)/usr/lib -llog 

LOCAL_CPP_EXTENSION	:=	.cpp

LOCAL_MODULE	:= mainproc

LOCAL_SRC_FILES	:= mainproc.cpp
LOCAL_SRC_FILES	+= android-injector.cpp
LOCAL_SRC_FILES	+= mobile-main-com.cpp

LOCAL_ARM_MODE	:= arm

include $(BUILD_EXECUTABLE)
