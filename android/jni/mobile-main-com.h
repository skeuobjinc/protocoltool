#ifndef	_MOBILE_MAIN_COM_H_
#define	_MOBILE_MAIN_COM_H_

#include <stdio.h>
#include <stdlib.h>
#include <asm/user.h>
#include <asm/ptrace.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <dlfcn.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <elf.h>
#include <android/log.h>
#include <errno.h>

#include <fcntl.h>  
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <dlfcn.h>
#include <arpa/inet.h>
 //#include <linux/delay.h>
#include <unistd.h>

#define DATA_TRANSMIT_BEGIN			0X00000001		// begin transmition
#define DATA_TRANSMIT	     		0X00000002		// transmit data to pc
#define DATA_TRANSMIT_END			0X00000003		// end transmition 
#define SHUTDOWN_SERVER 			0X00000004		// shutdown the thread_main
#define EXIT_CLIENT		 			0X00000005		// exit from current thread sub 
#define TRANS_CMD_ECHO	     		0X00000006		// echo what the thread has done for our commmand from pc
#define GET_PROC_LIST	     		0X00000007		// get process list
#define INJECT_PROCESS	     		0X00000008		// inject to a specified process
///////////////////////////////////////////////////////////////////////////////////////////
 
#define PORT            20087
#define BUFFER_SIZE     1024  
#define LOG_TAG "MOBILE_MAIN_COM"
#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, fmt, ##args)
 


typedef struct
{
	int imsg_type;
	int ibody_len;
	int iparams[2];
}msg_packet,*pmsg_packet;

typedef struct
{
	int imsg_type;
	int ibody_len;
}raw_packet,*praw_packet;



class MOBILE_MAIN_COM
{
public:
	 
	MOBILE_MAIN_COM()
	{
		m_main_thread_id = 0;
		m_to_client_fd = 0; 
	 	m_bsend = 0; 
	}

	~MOBILE_MAIN_COM()
	{
		 
	}
public:

	pthread_t  m_main_thread_id;
	int m_to_client_fd ; 
	int m_bsend ;

public:

	void init_com();
	static void * thread_main(void *param);
	static void * thread_sub(void *param);
	int list_process(char * szprocess_list);

	// below are some functions about communication 
	// send raw data directly by send API
	int send_raw_data(int imsg_type,char * pmsg_body,int ibody_len,int fd_client);
	// send data according to  begin ,end and body of msg data
	int transmit_data(int imsg_type,char * pmsg_body,int ibody_len,int client_fd);
	// build msg packet and call transimit_data
	int send_msg_packet(int imsg_type,char * pmsg_body,int ibody_len,int client_fd,int target_fd);
	// recv msg packet from pc
	int recv_msg_packet(char * pmsg_data,int imsg_len,int client_fd,int *pmsg_type);
}; 

#endif