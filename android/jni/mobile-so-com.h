#ifndef	_MOBILE_SO_COM_H_
#define	_MOBILE_SO_COM_H_

#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h> 
#include <android/log.h>
#include <unistd.h>  
#include <stdio.h>     
#include <elf.h>  
#include <fcntl.h>  
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <dlfcn.h>
#include <arpa/inet.h>
 
typedef struct
{
	int imsg_type;
	int ibody_len;
	int client_fd;
	char sz_IP[16]; 
}msg_packet,*pmsg_packet;


#define PORT            20086
#define BUFFER_SIZE     1024   
class MOBILE_SO_COM
{
public:
	 
	MOBILE_SO_COM()
	{
		m_main_thread_id = 0;
		m_to_client_fd = 0; 
	 	m_bsend = 0;
	 	m_bauto = 0; 
	 	m_bret_none = 0;
	}

	~MOBILE_SO_COM()
	{
		 
	}

public:
	pthread_t  m_main_thread_id;
	int m_to_client_fd ;  
	int m_bsend ;
	int m_bauto ;
	int m_bret_none;
public: 

	void init_com();
	static void * thread_main(void *param);
	static void * thread_sub(void *param);

	// below are functions about communication with pc
	// send msg packet to pc
	int send_msg_packet(int imsg_type,char * pmsg_body,int ibody_len,int client_fd,int target_fd); 
	// recv msg packet from pc
	int recv_msg_packet(char * pmsg_data,int imsg_len,int client_fd,int *pmsg_type);

	// transmit hijacked-send/recv data to pc
	void transmit_send_data(char * pmsg_body,int ibody_len,int target_fd);
	void transmit_recv_data(char * pmsg_body,int ibody_len,int target_fd);

	// execute cmd from pc 
	int proxy_sender(int client_fd,void* pmsg_data,int len,int flag);

	// whether send hijacked data to pc
	int is_send_to_pc(){return m_bsend;};
	// automaticly send data after modify data
	int is_auto(){return m_bauto;};
	// do nothing after hijack the function
	int is_ret_none(){return m_bret_none;};
}; 

#endif