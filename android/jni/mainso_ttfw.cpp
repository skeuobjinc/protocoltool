
#define DEBUG_MODE

#include "android-hook.h"
#include "UserStruct.h"
#include "GameDefine.h"
#include <stdio.h>
#include <dlfcn.h>
#include <string>
#include <vector>

//全局定义
ANDROID_HOOK hk;
MOBILE_SO_COM so_com; 

typedef struct
{
	void * pHookedAddr;
	int nNum;
	char szHookedlib[64];
}HookParamStruct ,*PHookParamStruct;

void commonHook(const char* const funcStr, void* new_func, void** old_func)
{
	LOGD("func=%s",funcStr);
	void *pHandle = dlopen("/data/app-lib/com.tencent.modoomarble-1/libgame.so", RTLD_LAZY);
	void *addr =dlsym(pHandle,funcStr);
	dlclose(pHandle);
	hk.Hook_advance(addr, new_func, old_func);
}
 

/**
* 以上为测试人员可能感兴趣的逻辑相关函数:
********************************************************************************************
*/

extern unsigned long * pPackCnt;  // this global defined in mobile-so-com.cpp,just for the tmp case ttfw
int (*oldsend)(int);

int mysend(int data) 
{
	if (so_com.is_send())
	{ 
		char *buff=*(char **)(data+0x1b0); 
		int len=*(int *)(data+0x1b8);
		int target_fd=*(int*)(data+0x14);
		pPackCnt =(unsigned long *)(data+0x1a8);
		so_com.send_msg(buff,len,target_fd);
		//
		LOGD("buf=>%08x,len =%d, target_fd = %d",(unsigned int)buff,len,target_fd);
	}
	LOGD("funk mysend execution....");
	return oldsend(data);
}
 


//入口函数

extern "C" void hook_entry(void * pParam)
{
	HookParamStruct *phookparam = (HookParamStruct *)pParam;
 
	LOGD("Hook success\n");
	LOGD("Start hooking\n"); 
	LOGD("AIWEN-PARAM-1:%s\n",phookparam->szHookedlib);
	LOGD("AIWEN-PARAM-2:%08x\n",(unsigned int )phookparam->pHookedAddr);
	LOGD("AIWEN-PARAM-3:%d\n",phookparam->nNum);
	
	so_com.init_com();
	void *pHandle = dlopen("/data/app-lib/com.tencent.modoomarble-1/libgame.so", RTLD_LAZY);
	void *sendaddr = dlsym(pHandle, "_ZN14clClientSocket4SendEi");
	dlclose(pHandle);

	LOGD("send addr=%08x\n", (unsigned int)sendaddr); 
	hk.Hook_advance(sendaddr, (void*)mysend, (void **)&oldsend);

}