﻿#pragma once

//#include <iostream>

#define MAKE_STR( Value_ )	#Value_
// 게임 버전
#define		VERSION			40
#define		MIN_VERSION		40

#define IP_LEN			24	// IP 버퍼사이즈

//#include "./MarbleItem/MarbleItem.Define.h"
#define _VERSION_MATCH_
 
#if _WIN32
 
typedef	__int64 TMoney;

#else


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
typedef __int64_t __int64;
typedef __int32_t __int32;
typedef __int16_t __int16;
typedef __int8_t __int8;
typedef	__int64 TMoney;
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
typedef int64_t __int64;
typedef int32_t __int32;
typedef int16_t __int16;
typedef int8_t __int8;
typedef	__int64 TMoney;
#endif

#endif

#define __RGB(r,g,b)          ((__int32)(((char)(r)|((__int32)((char)(g))<<8))|(((__int32)(char)(b))<<16)))

typedef struct tagPOINT_
{
	long  x;
	long  y;
} __POINT;

enum
{
	GKType_None = 0,					//	없음.
	MIN_GKType = 1,	
	GKType_FreeChangeStruct	= 1,		//  마블 부동산
	GKType_SpaceTravel,					// 	세계여행 초대장
	GKType_Donation,					// 	꼴등에게 불우이웃 돕기
	GKType_GoIsland,					// 	무인도 탐험
	GKType_OlympicOpen,					// 	올림픽 개최
	GKType_OlympicTour,					// 	올림픽 관광
	GKType_EscapeIsland,				// 	무인도 탈출
	GKType_GoStart,						// 	새마음 새출발
	GKType_GoMacao,						// 	마카오 관광
	GKType_GoTax,						// 	강제 징수
	GKType_Earthquake,					// 	지진
	GKType_ChangeCity,					// 	도시 교환
	GKType_ForceSell,					// 	강제 매각
	GKType_BlackOut,					// 	정전
	GKType_YellowSand,					// 	황사
	GKType_Shield,						// 	방패
	GKType_Angel,						// 	천사
	GKType_DiseaseWide,					// 	전염병
	GKType_Tsunami,						// 	쓰나미
	GKType_VIPCard,						// 	할인쿠폰
	GKType_TransferCity,				//  도시 양도		공격 찬스카드는 아닌데 프로세스가 비슷해서 공격으로 처리한다.	
	GKType_RipoffPrices,				//	바가지 요금	
	GKType_BlackHoleExplore_Space,		//	블랙홀 탐사, 퍼레이드 관람
	GKType_Accident,					//  교통사고
	GKType_GotoBokbulbok,				//  복불복	
	GKType_FreePass_Park,				//	테마파크 자유이용권
	MAX_GKType,
};


enum EGKAType
{
	GKAType_Attack,
	GKAType_Defense,
	GKAType_Neutral,
	GKAType_Max,
	GKAType_Null
};




enum
{
	DiceType_None,
	DiceType_Normal,				// 말 이동
	DiceType_Normal_X2,				// 말 이동
	DiceType_Normal_Reverse,		// 말 이동
	DiceType_Not_Escape_Island,		// 무인도 탈출	
};

enum 
{
	MoveType_Normal,		// 말 이동
	MoveType_ProcessMove,	// 지시에 따라 이동 찬스카드 이동 같은거
	MoveType_SpaceTravel,	// 우주여행	
	MoveType_ThemeParkGate,	// 테마파크맵 갈림길
};

enum EFinishType
{
	FinishType_Normal = 0,
	FinishType_3Monopoly,
	FinishType_LineMonopoly,
	FinishType_AllTour,
	FinishType_3MonopolyLine,
	FinishType_LineAllTour,
	FinishType_Time,
	FinishType_Turn,
	FinishType_Max,
	FinishType_Buy_Card = 10,
	FinishType_Enchant  = 11,
};



enum EGender
{
	Gender_Female,
	Gender_Male,
	Gender_Max,
	Gender_Null
};


enum EGameMode
{
	//GameMode_Normal,
	//GameMode_Gamble,
	GameMode_Max,
	GameMode_Null
};


enum EGroup
{	
	Group_Beginner = 1, // 초보
	Group_Lower,		// 서민
	Group_Middle,		// 중산
	Group_Upper,		// 부자
	Group_Tycoon,		// 재벌
	Group_VIP,
	Group_Free,			// 자유
	Group_Rookie,
	MAX_GROUP = 30,			// 최대 그룹 개수
};

enum
{
	TURN_STATE_NONE = 0,
	TURN_STATE_RESERVE_NEXT_TURN,					// 턴 딜레이를 주기 위해~
	TURN_STATE_CHARACTER_SELECT,					// 캐릭터를 선택하시요.
	TURN_STATE_DECISION_PLAY_TURN,					// 턴 결정.	
	TURN_STATE_DECISION_PLAY_TURN_DONE_EFFECT,		// 턴 결정하고나서 약긴 클라이언트 효과때문에 딜레이를 준다.
	TURN_STATE_THROW_DICE,							// 서버가 주사위를 던저라고 요청한 상태
	TURN_STATE_THROW_DICE_EFFECT,					// 주사위를 던지고 나서 주사위 애니 끝날때 까지의 상태
	TURN_STATE_MOVE_PLAYER,							// 이동 완료 했다
	TURN_STATE_CHANGE_STRUCT_ASK,					// 건물을 건설해라.
	TURN_STATE_MUST_SELL_FOR_TOLL,					// 건물을 매각해라.
	TURN_STATE_TAKE_OVER_DECIDE,					// 인수 할꺼냐?
	TURN_STATE_CHANCE_CARD_OPEN,					// 찬스카드를 오픈했다.
	TURN_STATE_ATTACK_CHOOSE,						// 공격대상을 선택해라.	
	TURN_STATE_CHANGE_STRUCT_FREE,					// 자유건설 할곳을 정해라~
	TURN_STATE_SPACE_TRAVEL_CHOOSE,					// 우주여행 장소 선택하시오
	TURN_STATE_CREATE_OLYMPIC_CHOOSE,				// 이벤트 개최할 지역을 선택해라
	TURN_STATE_ISLAND_CHARGE_CHOOSE,				// 무인도 탈출 방법을 선택하시오
	TURN_STATE_TAX,									// 세금징수.
	TURN_STATE_BETTING_MACAU,						// 마카오 베팅~
	TURN_STATE_BETTING_MACAU_EFFECT,				// 마카오 베팅 효과
	TURN_STATE_BETTING_KANGWON,						// 강원 베팅
	TURN_STATE_BETTING_KANGWON_EFFECT,				// 강원 베팅 효과			- 사용 안함 수현
	TURN_STATE_BOKBULBOK_CARD_OPEN,					// 복불복카드를 받았다.		- 사용 안함 수현
	TURN_STATE_BOKBULBOK_TARGET_MAKE_START,			// 복불복 대상을 지정해라. 룰렛을 돌려라	- 사용 안함 수현
	TURN_STATE_BOKBULBOK_TARGET_MAKE_START_OK,		// 볼북복 룰렛을 돌렸다	
	TURN_STATE_BETTING_LOTTO,						//로또 베팅
	TURN_STATE_BETTING_LOTTO_EFFECT,				//로또 베팅 효과
	TURN_STATE_ECONOMY_CARD_OPEN,					//경제카드를 오픈했다.
	TURN_STATE_BLACKHOLE_CHOOSE,					// 블랙홀 생성 장소 지정
	TURN_STATE_BETTING_ALIEN_HUNT,					//에일리언 사냥 베팅
	TURN_STATE_BETTING_ALIEN_HUNT_EFFECT,			//에일리언 사냥 베팅 효과
	TURN_STATE_START_POS,							// 시작 위치
	TURN_STATE_THEMEPARK_GATE_SELECT,				// 테마파크맵 갈림길 선택
	MAX_TURN_STATE,									// 

};
//////////////////////////////////////////////////////////////////////////
//서동화 우주맵 작업
//////////////////////////////////////////////////////////////////////////
enum
{
	Begin_MapKind = 0,
	MapKind_World_8 = Begin_MapKind,
	MapKind_Space,			// 우주맵
	MapKind_Korea,			// 한국맵
	MapKind_Easy,			// 이지모드
	MapKind_ThemePark,		// 테마파크
	Max_MapKind,
};
//////////////////////////////////////////////////////////////////////////



enum
{
	BokbulbokCard_None = 0,						// 없음
	BokbulbokCard_Move_Prison,				// 감옥
	BokbulbokCard_ForceSell,				// 매각
	BokbulbokCard_RipoffPrices,				// 바가지
	BokbulbokCard_Move_Reverse,				// 뒤로가기
	BokbulbokCard_Move_X2,					// X2
	BokbulbokCard_Move_ChanceCard,			// 찬스
	BokbulbokCard_Withdraw_X1,				// 월급
	BokbulbokCard_Withdraw_X2,				// 월급
	BokbulbokCard_Withdraw_X3,				// 월급
	BokbulbokCard_Tax,						// 세금
	BokbulbokCard_Move_Tarvel,				// 여행
	BokbulbokCard_BlackOut,					// 정전
	BokbulbokCard_Move_Start,				// 출발
	MAX_BokbulbokCard,
};

enum
{
	BokbulbokTarget_None = 0,				// 없음
	BokbulbokTarget_Except_Me,				// 나 빼고 다
	BokbulbokTarget_Rank_First,				// 1등만
	BokbulbokTarget_Rank_Last,				// 꼴지 메롱
	BokbulbokTarget_All,					// 모두
	BokbulbokTarget_Only_Me,				// 나만
	MAX_BokbulbokTarget,
};

enum
{
	ForceMoveType_Normal = 0,				// 강제이동 기본
	ForceMoveType_BlackHole,				// 강제이동 블랙홀
	ForceMoveType_ParadeCar,				// 퍼레이드카
};


#define MAX_PLAYER		4			// 최대 게임할수 있는플레이어
#define MAX_SUPERROOM	20		// 최대 슈퍼방 개수	[게임 아이템 작업]
#define	MAX_CHANNEL		9
#define MAX_NEWPLAYER	4   


#define MAX_BOARD_BLOCK_NUM		40
#define MAX_BOARD_LINE_NUM		4

enum
{
	LINE_BLOCK_NUM_BEGIN  = 8,
	LINE_BLOCK_NUM_8 = LINE_BLOCK_NUM_BEGIN,
	LINE_BLOCK_NUM_9,
	LINE_BLOCK_NUM_10,
	LINE_BLOCK_NUM_END,
};

enum eSNSTYPE
{
	SNS_BEGIN = 0,
	SNS_GUEST = SNS_BEGIN,
	SNS_FACEBOOK,
	SNS_KAKAO = 3,
	SNS_WECHAT = 4,
	SNS_QQ = 5,
	SNS_LINE_JP = 6,
	SNS_LINE_GUEST = 7,
	SNS_END,
};




enum eIAPTYPE
{
	IAP_BEGIN = 0,
	IAP_GOOGLE = IAP_BEGIN,
	IAP_APPLE,
	IAP_END,
};

#define MAX_FESTIVAL_NUM					3			// 축제 개최 수


#define MAX_PAY_TOOL_COUNT			4

#define MAX_SNS_UID_LEN			64		// SNS 로그인일 경우 ID
#define MAX_SNS_PICURL_LEN		512		// SNS 사진 Url

#define MAX_NICKNAME_LEN		64		// 닉네임
#define MAX_GREETING_LEN		512		// 인사말
#define MAX_MAIL_MESSAGE_LEN	256

#define MAX_STRING_INDEX_LEN	16
#define MAX_STRING_FILENAME_LEN	64

#define MAX_ROOM_TITLE_LEN		32		// 방제목
#define MAX_ROOM_PASS_LEN		5		// 방 암호 길이

#define MAX_THROW_DICE_NUM		3		// 던질수 있는 최대 주사위 갯수

#define RESERVE_MAX_PLAYER_LEVEL 150	// vector::reserve(최대 레벨)
#define LIMIT_PLAYER_LEVEL_BONUS 50

#define MAX_SKILL_SLOT 4

#define MAX_REWARD_WEEK_SCORE 500000

#define HEARTBEAT_TIME			60000	// Heart beat Send Time

#define MAX_RECV_MAIL_BOX_LIST	20
#define MAX_RECV_SPOINT_LIST	20
#define MAX_SEND_SPOINT_LIST	20
#define MAX_SEND_FRIEND_LIST	200

#define MAX_IAP_ORDERID_LEN		64
#define MAX_IAP_PRODUCTID_LEN	64

#define MAX_IAP_PRICE_LEN		8		// 결제 금액
#define MAX_IAP_CURRENCY		4		// 화폐단위


#define MAX_IAP_SIGNATURE_LEN		1024
#define MAX_IAP_SIGNED_LEN		1024 * 3


#define MAX_STRING_TERMDAILYREWARDEVENT_VALUE_LEN	14	// 일일보상 클라이언트 출력용 문구 길이

#define GAME_VIEW_WIDTH				(1024)			// 해상도
#define GAME_VIEW_HEIGHT			(768)			// 해상도

#define MAX_LOAD_VIP 3
#define MAX_ANTI_DATA_LEN 65400


enum eFreeRandomBox
{
	eFreeRandomBox_begin = 0,
	eFreeRandomBox_normal = eFreeRandomBox_begin,
	eFreeRandomBox_expensive,
	eFreeRandomBox_normal_re,		// 재구매
	eFreeRandomBox_expensive_re,	// 재구매
	eFreeRandomBox_max
};

#define			LIMIT_INVITE_COUNT		200				// 친구 초대 최대 개수



enum eSpointExpType
{
	eSpointExpType_begin = 0,
	eSpointExpType_none = eSpointExpType_begin,
	eSpointExpType_Attend,							// 출석
	eSpointExpType_Invitation_Friend,				// 초대
	eSpointExpType_Ranking_Boast,					// 랭킹자랑
	eSpointExpType_SendClover,						// 클로버 보내기
	eSpointExpType_SendClover_Quiescence,			// 클로버 보내기 휴면
	eSpointExpType_BuyItem,							// 아이템 구매시
	eSpointExpType_BuySkill,						// 스킬 구매시
	eSpointExpType_FailedEnchantSkill,				// 스킬 강화 실패시		
	eSpointExpType_1Game,							// 한판 할때( 아래 승리 case 제외 )
	eSpointExpType_Win_Tri,							// 트리플 독점 승리
	eSpointExpType_Win_Line,						// 라인 승리
	eSpointExpType_Win_Tour,						// 관광지 독점
	eSpointExpType_Ranking_Boast_Ext,				// 자랑하기 보상 추가 13
	eSpointExpType_MAX
};

#define TEAM_NO_SINGLE		0
#define TEAM_NO_A			1
#define TEAM_NO_B			2
#define MAX_TEAM_NO			3

#define MAX_TEAM_PLAYER_NUM		2 // 최대 팀원수


#define READY_STATE_LOADING			0
#define READY_STATE_NONE			1
#define READY_STATE_READY			2
#define READY_STATE_RESULT			3
#define MAX_ROOM_MASTER_OUT_TIME	10		// 초 모든 유저가 다 래디했는데 이상 됬을경우 방장 강퇴.

//#define	SOCIAL_RECHARGE_TIME					2400		// Social Point 충전 간격. 30 * 60	
#define MAX_SOCIAL_RECHARGE						5

#define c_RgnCnt_Null			-1


#define PNUM_NULL			-1

#define MAX_PACKET_BUFFER_SIZE 25600

enum eSEED_TYPE
{
	BEGIN_SEED_TYPE = 0,
	SEED_MIAMI = BEGIN_SEED_TYPE,	// 카카오 이코노미
	SEED_MACAU,						// 비지니스
	SEED_LASVEGAS,					// 퍼스트
	SEED_EASY,						// 이지 모드
	MAX_SEED_TYPE,
};

enum enPROPERTY_KIND
{
	Property_Kind_Land = 0,		// 토지
	Property_Kind_Villa,		// 별장
	Property_Kind_Building,		// 빌딩
	Property_Kind_Hotel,		// 호텔
	Property_Kind_Landmark,		// 랜드마크
	Property_Kind_Max			
}; // 건물 종류

#define STRUCT_EMPTY		0
#define STRUCT_CREATE		1

enum enPRICE_TYPE
{
	PriceType_Con = 0,			// 건설비
	PriceType_Sell,				// 매각
	PriceType_Toll,				// 통행료
	PriceType_Take,				// 인수 가격
	PriceType_Max
}; // 가격 종료

enum EPosType
{
	PosType_State = 0,
	PosType_Tour,
	PosType_Beach,
	PosType_GoldenKey,
	PosType_Start,
	PosType_DesertedIsland,
	PosType_SpaceTravel,
	PosType_Macau,
	PosType_Tax,
	PosType_Event,
	PosType_Prison,
	PosType_Exhibition,
	PosType_TrainTravel,
	PosType_Bokbulbok,
	PosType_Kangwon,
	PosType_Gate,				// 테마파크맵 갈림길
	Max_PosType,
	
};



#define MAX_OLYMPIC_CNT						4		// 최대 올림픽 개최 수

#define NULL_VALUE		-1


//////////////////////////////////////////////////////////////////////////
// 마블 아이템

enum
{
	Ability_Kind_Island_Escape = 0,		// 무인도 탈출
	Ability_Kind_Macau_Win,				// 마카오승리
	Ability_Kind_Func_Discount,			// 국세청/매각/무인도/올림픽/세계여행 할인 증가
	Ability_Kind_ChanceCard,			// 황금찬스카드
	Ability_Kind_Build_Discount,		// 건설비용할인 건설, 매각
	Ability_Kind_Dice_Gage,				// 주사위게이지
	Ability_Kind_Takeover_Discount,		// 인수비용 할인
	Ability_Kind_Exp_Booster,			// 경험치 부스터
	Ability_Kind_Gold_Booster,			// 골드 부스터
	Ability_Kind_RP_Booster,			// RankPoint 부스터
	Ability_Kind_Dice_Odd,				// 주사위 홀짝
	Ability_Kind_PassMoney_Discount,	// 통행료 할인
	Max_Ability_Kind,
};

enum ITEM_TYPE
{
	ITEM_TYPE_COIN = 0,		// 캐쉬 구매 코인이 아니고 이벤트 코인
	ITEM_TYPE_CHARACTERCARD_DECK,
	ITEM_TYPE_CHARACTERCARD,
	ITEM_TYPE_DICE,
	ITEM_TYPE_MONEY,
	ITEM_TYPE_ODD,
	ITEM_TYPE_SOCIAL_POINT,
	ITEM_TYPE_GOLD,
	ITEM_TYPE_RESET_RECORD,
	ITEM_TYPE_DICE_BOOSTER,
	ITEM_TYPE_WITHDRAW_BOOSTER,
	ITEM_TYPE_RANDOM_GOLD_CARD,
	ITEM_TYPE_RANDOM_GOLD_CARD_OVERLAP,
	ITEM_TYPE_TICKET_BUSINESS,
	ITEM_TYPE_TICKET_FIRST,
	ITEM_TYPE_COIN_P,
	ITEM_TYPE_TICKET_FRIEND_PLAY,
	ITEM_TYPE_NICKNAME_RESET,
	ITEM_TYPE_SKILL_INVENTORY,
	ITEM_TYPE_RANDOM_BOX,
	ITEM_TYPE_GOLD_P,
	ITEM_TYPE_SPOINT_P,
	ITEM_TYPE_SKIN,
	ITEM_TYPE_CARDMIX,
	MAX_ITEM_TYPE,
};


enum ITEM_GRADE_TYPE
{
	BEGIN_ITEM_GRADE = 0,
	ITEM_GRADE_C = BEGIN_ITEM_GRADE,//ITEM_GRADE_NORMAL
	ITEM_GRADE_B,//ITEM_GRADE_RARE,
	ITEM_GRADE_A,//ITEM_GRADE_LEGEND,
	ITEM_GRADE_A_PLUS,//ITEM_GRADE_SEASON,
	ITEM_GRADE_S,
	//ITEM_GRADE_S_PLUS,
	MAX_ITEM_GRADE,
};

enum DURATIONT_TYPE
{
	BEGIN_DURATION_TYPE = 1,
	DURATION_TYPE_COUNT = BEGIN_DURATION_TYPE,
	DURATION_TYPE_TIME,
	DURATION_TYPE_ONCE,
	MAX_DURATION_TYPE,
};

enum CURRENCY_TYPE
{
	CURRENCY_CASH = 1,	 // 현금
	CURRENCY_COIN = 2,   // 다이아
	CURRENCY_MONEY = 3,		
	CURRENCY_SOCIAL = 4,
	CURRENCY_GOLD = 5,
	CURRENCY_COIN_P = 6,	// 유료 지급 다이아
	CURRENCY_RANK = 7,
	CURRENCY_GOLD_P = 8,	// 유료 지급 골드
	CURRENCY_SOCIAL_P = 9,	// 유료 지급 소셜 포인트
};

//////////////////////////////////////////////////////////////////////////
// 카드 조합 성공 실패 확률
enum
{
	Card_Mix_Pro_Fail = 0,
	Card_Mix_Pro_Keep,
	Card_Mix_Pro_Succ,	
	Max_Card_Mix_Pro,
};

enum
{
	CARDBUY_TYPE_NORMAL = 1,
	CARDBUY_TYPE_RARE,
	CARDBUY_TYPE_LUCKY,
	CARDBUY_TYPE_ADMIN,
	CARDBUY_TYPE_SPECIAL,
};

//////////////////////////////////////////////////////////////////////////
// 확률 종류
enum
{
	CardMixProKind_Normal = 0,	// 일반 확률
	CardMixProKind_LuckyItem,	// 행운권 확률
	MaxCardMixProKind,			// 행운권 확률
};

enum CARD_PROPERTY
{
	Card_Property_None,			// 무속성
	Card_Property_Mix_Free,		// 조합 프리 카드(밥 카드)
};

// 메일 박스 종류 ----------------------------------------------------
enum
{
	MAIL_BOX_KIND_NORMAL = 0,
	MAIL_BOX_KIND_USESTRING,
	MAIL_BOX_KIND_DISCONNECT_IAP,
	MAIL_BOX_KIND_ADMIN,
	MAIL_BOX_KIND_NEW_USESTRING,
};

#define LIMIT_ADD_SPOINT_TIME			300		//  소셜 포인트 지급 시간 5분( 5 * 60 )
//#define LIMIT_ADD_SPOINT_TIME			60		//  소셜 포인트 지급 시간 5분( 5 * 60 )

#define MIN_DEAILY_BONUS_MONEY				2000000
#define MAX_DEAILY_BONUS_MONEY				5000000

#define MAX_MARBLE_ITEM_ADD_NUM			30		// 최대 구매 할수 있는 카드 횟수
#define MAX_SKILL_ITEM_ADD_NUM			1		// 최대 구매 할수 있는 스킬

#define MAX_MARBLE_ITEM_MIX_NUM			5
#define MAX_MARBLE_ITEM_UPGRADE_NUM		2

#define MAX_INVITE_FRIEND_REWARD_EVENT 3

//@todo 임시로 적용 삭제할 데이터
#define MAX_APPLY_ABILITY_PRO_NUM		1000
#define MAX_APPLY_SKILL_PRO_NUM			100

#define	MAX_EVENT					20

#define MAX_BLOCK_MESSAGE 64		//블럭 영자메시지 
#define MAX_SNS_INVITE_COUNT			300		// Global 전용. 최대 초대 개수 제한(제한시간내에 최대개수)

#pragma pack( push, 1 )

//////////////////////////////////////////////////////////////////////////
// 강화
typedef struct
{	
	int				iEnchantNum;			// 강화 횟수
	int				iEnchantPro;			// 확률
	int				iGetStar;				// 얻는 별 개수
	TMoney			tEnchantFee;			// 강화 머니.
} stENCHANT_PRO;


typedef struct
{	
	__int64		OwnedItemUID;					// 유니크 넘버
	int			ItemInfoUID;					// 아이템 UID
	int			gradeType;						// 아이템 등급
	
	__int64		shUseDay;						// 사용 가능한 기간(일)
	short		shUseNum;						// 사용 가능한 회수
	short		isUsable;						// 사용 유무

	int			Ability[Max_Ability_Kind];		// 능력치
	int			EnchantAbility[Max_Ability_Kind];

	int			EnchantNum;					// 인첸트 한 횟수
	int			EnchantExp;				// 인첸트 경험치
	int			MaxEnchantNum;					// 인첸트 가능한 최대 횟수

	int			BuffAbilityIndex;
	__int64		BuffAbilityUsableTime;
	__int64		equipedOwnedSkinUID;					// 장착된 Skin Item

} stMARBLE_ITEM;

/*
typedef struct
{
	// 현재 가지고 있는 최대 아이템 개수
	//int				maxItemNum;
	int				itemListNum;
	
	// stMARBLE_ITEM 구조체만큼 버퍼에 넣어 전송 한다.
	char			itemListBuffer[MAX_PACKET_BUFFER_SIZE];

	//stMARBLE_ITEM	stMarbleItem[MAX_MARBLE_ITEM_NUM];
} stMARBLE_ITEM_LIST;*/

typedef struct
{
	char cCell[Property_Kind_Max];
} stSTRUCT_CELL;	// 

typedef struct
{
	bool			bOwnerFlag;
	int				OwnerServPNum;
	stSTRUCT_CELL	stStruct;		// 세워져있는 건물 

	int				iPayTollCnt;					// 해변 : 남이 방문한 회수 == 통행료 지불회수 관광지 : 다른 관광지 구매 개수

	char			cGKApplyServerPnum;				// 정전,전염병,황사을 사용한 Pnum
	char			cBlackOutCnt;					// 정전 통행료 0원			0 : 없음
	char			cDiseaseWideCnt;				// 전염병 통행료 50%	0 : 없음
	char			cYellowSandCnt;					// 황사 통행료 50%	0 : 없음


	char			cOlympicCnt;					// 올림픽 개최 중이냐?
	bool			bMonopolyFlag;					// 독점 중이냐?
	bool			bFestivalFlag;					// 미션 중이냐?


//	short			shBokbulbokCardNo;				// 복불복 카드 번호.

	TMoney			tSaveTaxMoney;					// 누적되는 세금.
	// 블랙홀 시스템
	char			cBlackHole;						// 0: 없다, 1:블랙홀, 2:화이트홀
	// 테마파크맵 갈림길
	int				iGateBlock;						// 지정된 블록, 0 : 없다. 맵상에 갈림길위치(4곳)만 지정가능
	bool			bOpenNight;						// 20140120 : 야간 개장 효과
} stSTRUCT_INFO;	// 

#pragma pack( pop )

/////////////////////////////////////////////////////////////////////////
// 테마파크
#define MAP_THEMEPARK_OPEN_NIGHT_BEFORE_NOTI_TRUN 13 // 테마파크맵 야간개장 공지
#define MAP_THEMEPARK_OPEN_NIGHT_TRUN 10 // 테마파크맵 야간개장 시작 턴
#define MAP_THEMEPARK_PARADE_CAR_COME_OUT_TRUN 5 // 테마파크맵 퍼레이드카 나오는 경과 턴(총턴-남은턴)
#define MAP_THEMEPARK_GATE_BLOCK_COUNT 4

// ISO-639-1 기준 언어별 코드표
enum LANGUAGE_CODE
{
	LANGUAGE_CODE_START = 0,
	LANGUAGE_CODE_AFAR,
	LANGUAGE_CODE_ABKHAZIAN,
	LANGUAGE_CODE_AFRIKAANS,
	LANGUAGE_CODE_AMHARIC,
	LANGUAGE_CODE_ARABIC,
	LANGUAGE_CODE_ASSAMESE,
	LANGUAGE_CODE_AYMARA,
	LANGUAGE_CODE_AZERBAIJANI,
	LANGUAGE_CODE_BASHKIR,
	LANGUAGE_CODE_BYELORUSSIAN,
	LANGUAGE_CODE_BULGARIAN,
	LANGUAGE_CODE_BIHARI,
	LANGUAGE_CODE_BISLAMA,
	LANGUAGE_CODE_BENGALI,
	LANGUAGE_CODE_TIBETAN,
	LANGUAGE_CODE_BRETON,
	LANGUAGE_CODE_CATALAN,
	LANGUAGE_CODE_CORSICAN,
	LANGUAGE_CODE_CZECH,
	LANGUAGE_CODE_WELCH,
	LANGUAGE_CODE_DANISH,
	LANGUAGE_CODE_GERMAN,
	LANGUAGE_CODE_BHUTANI,
	LANGUAGE_CODE_GREEK,
	LANGUAGE_CODE_ENGLISH,
	LANGUAGE_CODE_ESPERANTO,
	LANGUAGE_CODE_SPANISH,
	LANGUAGE_CODE_ESTONIAN,
	LANGUAGE_CODE_BASQUE,
	LANGUAGE_CODE_PERSIAN,
	LANGUAGE_CODE_FINNISH,
	LANGUAGE_CODE_FIJI,
	LANGUAGE_CODE_FAEROESE,
	LANGUAGE_CODE_FRENCH,
	LANGUAGE_CODE_FRISIAN,
	LANGUAGE_CODE_IRISH,
	LANGUAGE_CODE_SCOTSGAELIC,
	LANGUAGE_CODE_GALICIAN,
	LANGUAGE_CODE_GUARANI,
	LANGUAGE_CODE_GUJARATI,
	LANGUAGE_CODE_HAUSA,
	LANGUAGE_CODE_HINDI,
	LANGUAGE_CODE_HEBREW,
	LANGUAGE_CODE_CROATIAN,
	LANGUAGE_CODE_HUNGARIAN,
	LANGUAGE_CODE_ARMENIAN,
	LANGUAGE_CODE_INTERLINGUA,
	LANGUAGE_CODE_INDONESIAN,
	LANGUAGE_CODE_INTERLINGUE,
	LANGUAGE_CODE_INUPIAK,
	LANGUAGE_CODE_FORMER_INDONESIAN,
	LANGUAGE_CODE_ICELANDIC,
	LANGUAGE_CODE_ITALIAN,
	LANGUAGE_CODE_INUKTITUT,
	LANGUAGE_CODE_FORMER_HEBREW,
	LANGUAGE_CODE_JAPANESE,
	LANGUAGE_CODE_FORMER_YIDDISH,
	LANGUAGE_CODE_JAVANESE,
	LANGUAGE_CODE_GEORGIAN,
	LANGUAGE_CODE_KAZAKH,
	LANGUAGE_CODE_GREENLANDIC,
	LANGUAGE_CODE_CAMBODIAN,
	LANGUAGE_CODE_KANNADA,
	LANGUAGE_CODE_KOREAN,
	LANGUAGE_CODE_KASHMIRI,
	LANGUAGE_CODE_KURDISH,
	LANGUAGE_CODE_KIRGHIZ,
	LANGUAGE_CODE_LATIN,
	LANGUAGE_CODE_LINGALA,
	LANGUAGE_CODE_LAOTHIAN,
	LANGUAGE_CODE_LITHUANIAN,
	LANGUAGE_CODE_LATVIAN,
	LANGUAGE_CODE_MALAGASY,
	LANGUAGE_CODE_MAORI,
	LANGUAGE_CODE_MACEDONIAN,
	LANGUAGE_CODE_MALAYALAM,
	LANGUAGE_CODE_MONGOLIAN,
	LANGUAGE_CODE_MOLDAVIAN,
	LANGUAGE_CODE_MARATHI,
	LANGUAGE_CODE_MALAY,
	LANGUAGE_CODE_MALTESE,
	LANGUAGE_CODE_BURMESE,
	LANGUAGE_CODE_NAURU,
	LANGUAGE_CODE_NEPALI,
	LANGUAGE_CODE_DUTCH,
	LANGUAGE_CODE_NORWEGIAN,
	LANGUAGE_CODE_OCCITAN,
	LANGUAGE_CODE_OROMO,
	LANGUAGE_CODE_ORIYA,
	LANGUAGE_CODE_PUNJABI,
	LANGUAGE_CODE_POLISH,
	LANGUAGE_CODE_PASHTO,
	LANGUAGE_CODE_PORTUGUESE,
	LANGUAGE_CODE_QUECHUA,
	LANGUAGE_CODE_RHAETO,
	LANGUAGE_CODE_KIRUNDI,
	LANGUAGE_CODE_ROMANIAN,
	LANGUAGE_CODE_RUSSIAN,
	LANGUAGE_CODE_KINYARWANDA,
	LANGUAGE_CODE_SANSKRIT,
	LANGUAGE_CODE_SINDHI,
	LANGUAGE_CODE_SANGRO,
	LANGUAGE_CODE_SERBO,
	LANGUAGE_CODE_SINGHALESE,
	LANGUAGE_CODE_SLOVAK,
	LANGUAGE_CODE_SLOVENIAN,
	LANGUAGE_CODE_SAMOAN,
	LANGUAGE_CODE_SHONA,
	LANGUAGE_CODE_SOMALI,
	LANGUAGE_CODE_ALBANIAN,
	LANGUAGE_CODE_SERBIAN,
	LANGUAGE_CODE_SISWATI,
	LANGUAGE_CODE_SESOTHO,
	LANGUAGE_CODE_SUDANESE,
	LANGUAGE_CODE_SWEDISH,
	LANGUAGE_CODE_SWAHILI,
	LANGUAGE_CODE_TAMIL,
	LANGUAGE_CODE_TEGULU,
	LANGUAGE_CODE_TAJIK,
	LANGUAGE_CODE_THAI,
	LANGUAGE_CODE_TIGRINYA,
	LANGUAGE_CODE_TURKMEN,
	LANGUAGE_CODE_TAGALOG,
	LANGUAGE_CODE_SETSWANA,
	LANGUAGE_CODE_TONGA,
	LANGUAGE_CODE_TURKISH,
	LANGUAGE_CODE_TSONGA,
	LANGUAGE_CODE_TATAR,
	LANGUAGE_CODE_TWI,
	LANGUAGE_CODE_UIGUR,
	LANGUAGE_CODE_UKRAINIAN,
	LANGUAGE_CODE_URDU,
	LANGUAGE_CODE_UZBEK,
	LANGUAGE_CODE_VIETNAMESE,
	LANGUAGE_CODE_VOLAPUK,
	LANGUAGE_CODE_WOLOF,
	LANGUAGE_CODE_XHOSA,
	LANGUAGE_CODE_YIDDISH,
	LANGUAGE_CODE_YORUBA,
	LANGUAGE_CODE_ZHUANG,
	LANGUAGE_CODE_CHINESE,
	LANGUAGE_CODE_ZULU,
	LANGUAGE_CODE_END,
};


enum SERVICECODE
{
	SERVICECODE_NULL = 0,
	SERVICECODE_KAKAO,
	SERVICECODE_LINE,
	SERVICECODE_TENCENT,
};



enum TENCENT_QUALIFY_FROM
{
	TENCENT_QUALIFY_FROM_INGAME = 0,
	TENCENT_QUALIFY_FROM_MARKET,
};

enum TENCENT_ACCOUNTTYPE
{
	TENCENT_ACCOUNTTYPE_BG = 0,
	TENCENT_ACCOUNTTYPE_SAVE,
};
