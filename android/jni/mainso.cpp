
#define DEBUG_MODE

#include "android-hook.h"
#include "UserStruct.h"
#include "GameDefine.h"
#include <stdio.h>
#include <dlfcn.h>
#include <string>
#include <vector>
#include <elf.h>  
#include <fcntl.h>  
#include <sys/mman.h>
#include "linker.h"
//全局定义
ANDROID_HOOK hk_send;
ANDROID_HOOK hk_recv;
MOBILE_SO_COM so_com; 

typedef void (*PFN_MSHookFunction)(void *symbol, void *replace, void **result);

typedef struct
{
	void * pHookedAddr;
	int nNum;
	char szHookedlib[64];
}HookParamStruct ,*PHookParamStruct;

void commonHook(const char* const funcStr, void* new_func, void** old_func)
{
	LOGD("func=%s",funcStr);
	void *pHandle = dlopen("/data/app-lib/com.tencent.modoomarble-1/libgame.so", RTLD_LAZY);
	void *addr =dlsym(pHandle,funcStr);
	dlclose(pHandle);
	//hk.Hook_advance(addr, new_func, old_func);
}
 
void dumpmem(char* psz_mem)
{
	char szMemCache[16] = {0};
	char szMemHex[32] = {0};
	memset(szMemCache,0,16);
	memcpy(szMemCache,(char*)psz_mem,12);
 	char szTmp[5] = {0};
	for (int i = 0; i < 12; ++i)
	{
		sprintf(szTmp,"%02x",szMemCache[i]);
		memcpy(szMemHex+i*2,szTmp,2);
	}
	LOGD("FuncHead=>%s",szMemHex);
}
/**
* 以上为测试人员可能感兴趣的逻辑相关函数:
********************************************************************************************
*/

extern unsigned long * pPackCnt;  // this global defined in mobile-so-com.cpp,just for the tmp case ttfw

////////////////////////////////////////////////////////////////////////////////////////////
// hook sample

int (*oldsend)(int a1,int a2,int a3);
int (*oldrecv)(int a1,int a2, int a3);
int (*oldhashid)(char *a1, int a2, unsigned int a3);
void *sendaddr;
void *recvaddr;
void *hashidaddr;

int mysend(int a1,int a2,int a3)
{
    char ipAddr[INET_ADDRSTRLEN];//保存点分十进制的ip地址
    struct sockaddr_in peeraddr;
    int len;
    int ret = getpeername(a1, (struct sockaddr *)&peeraddr, &len);
    if(ret ==0 )
    	LOGD("send peer address = %s:%d\n", inet_ntop(AF_INET, &peeraddr.sin_addr, ipAddr, sizeof(ipAddr)), ntohs(peeraddr.sin_port));
    if (so_com.is_send_to_pc())// whether send the hijacked data to pc
	{   
		char *buff=(char*)(a2);
		int len = a3;
		so_com.transmit_send_data(buff, len, a1/**(int *)(a1 + 16)*/);// a1 is socket
		//so_com.transmit_recv_data(buff,len,0);
		//
		LOGD("send===>>>>>shit....buf=>%08x,len =%d",(unsigned int)buff,len);
		//return iret; 
	}
	//LOGD("funk mysend execution....");
	return oldsend(a1, a2, a3);                //call old send
}

int myrecv(int a1, int a2, int size)
{
	char ipAddr[INET_ADDRSTRLEN];
	struct sockaddr_in peeraddr;
	int len;
    int ret = getpeername(a1, (struct sockaddr *)&peeraddr, &len);
    if(ret ==0 )
    	LOGD("recv peer address = %s:%d\n", inet_ntop(AF_INET, &peeraddr.sin_addr, ipAddr, sizeof(ipAddr)), ntohs(peeraddr.sin_port));
    ret = oldrecv(a1, a2, size);
    if(ret <=  0)
    	return ret;
    if (so_com.is_send_to_pc())// whether send the hijacked data to pc
	{   
		char *buff=(char*)(a2);
		int len = size;
		so_com.transmit_recv_data(buff,len,a1);
		//
		LOGD("recv===>>>>>shit....buf=>%08x,len =%d",(unsigned int)buff,ret);
		//return iret; 
	}
	return ret;
}

void* get_module_base(pid_t pid, const char* module_name)
{
	FILE *fp;
	long addr = 0;
	char *pch;
	char filename[32];
	char line[1024];

	if (pid < 0) {
		/* self process */
		snprintf(filename, sizeof(filename), "/proc/self/maps");
	} else {
		snprintf(filename, sizeof(filename), "/proc/%d/maps", pid);
	}

	fp = fopen(filename, "r");

	if (fp != NULL) {
		while (fgets(line, sizeof(line), fp)) {
			if (strstr(line, module_name)) {
				pch = strtok( line, "-" );
				addr = strtoul( pch, NULL, 16 );

				if (addr == 0x8000)
					addr = 0;

				break;
			}
		}

		fclose(fp) ;
	}

	return (void *)addr;
}

void replaceFunc(void *handle, const char *name, void* pNewFun, void** pOldFun)
{

	if (!handle)
		return;

	soinfo *si = (soinfo*)handle;
	Elf32_Sym *symtab = si->symtab;
	const char *strtab = si->strtab;
	Elf32_Rel *rel = si->plt_rel;
	unsigned count = si->plt_rel_count;
	unsigned idx;

	bool fit = 0;

	for (idx = 0; idx<count; idx++)
	{
		unsigned int type = ELF32_R_TYPE(rel->r_info);
		unsigned int sym = ELF32_R_SYM(rel->r_info);
		unsigned int reloc = (unsigned)(rel->r_offset + si->base);
		char *sym_name = (char *)(strtab + symtab[sym].st_name);

		if (strcmp(sym_name, name) == 0)
		{
			uint32_t page_size = getpagesize();
			uint32_t entry_page_start = reloc& (~(page_size - 1));
			mprotect((uint32_t *)entry_page_start, page_size, PROT_READ | PROT_WRITE);

			*pOldFun = (void *)*((unsigned int*)reloc);
			*((unsigned int*)reloc) = (unsigned int)pNewFun;
			LOGD("find %s function at address: %p",name,(void*)*pOldFun);
			fit = 1;
			break;
		}
		rel++;
	}

	if (!fit) {
		LOGD("not find :%s in plt_rel", name);
	}
}

extern "C" void hook_entry(void * pParam)
{
	HookParamStruct *phookparam = (HookParamStruct *)pParam;
 
	LOGD("Hook success\n");
	LOGD("Start hooking\n");
	LOGD("AIWEN-PARAM-1:%s\n",phookparam->szHookedlib);
	LOGD("AIWEN-PARAM-2:%08x\n",(unsigned int )phookparam->pHookedAddr);
	LOGD("AIWEN-PARAM-3:%d\n",phookparam->nNum);
	so_com.init_com();

////////////////////////////////////////////////////////////////////////////////////////////

	void *pHandle = dlopen("/data/local/tmp/libsubstrate.so", RTLD_LAZY);                    // so that needed hook
	void *modulebase = get_module_base(-1, "libarcanelegends.so");
	sendaddr = (void *)((unsigned)modulebase + 0x682BE4+1);
	recvaddr = (void *)((unsigned)modulebase + 0x682C24+1);
	hashidaddr = (void *)((unsigned)modulebase + 0x60C1E4+1);
	PFN_MSHookFunction pfn_MSHookFunction = NULL;
	pfn_MSHookFunction = (PFN_MSHookFunction)dlsym(pHandle, "MSHookFunction");
	pfn_MSHookFunction(sendaddr, (void *)mysend, (void **)&oldsend);
	pfn_MSHookFunction(recvaddr, (void *)myrecv, (void **)&oldrecv);
	//hk_send.Hook_advance(sendaddr, (void*)mysend, (void **)&oldsend);      // hook function
	//hk_recv.Hook_advance(recvaddr, (void *)myrecv, (void **)&oldrecv);
	dlclose(pHandle);
	LOGD("base addr = %08x\n", (unsigned int)modulebase);
	LOGD("send addr=%08x\n", (unsigned int)sendaddr);
	LOGD("recv addr=%08x\n", (unsigned int)recvaddr);

////////////////////////////////////////////////////////////////////////////////////////////
        // @sendaddr is the original address of send function
        // @mysend  is our function
        // @oldsend is a varible to store original (function jump back )
	//hk_send.Hook_advance(sendaddr, (void*)mysend, (void **)&oldsend);      // hook function
	//hk_recv.Hook_advance(recvaddr, (void *)myrecv, (void **)&oldrecv);
}
 