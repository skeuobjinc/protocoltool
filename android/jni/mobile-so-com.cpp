 
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <android/log.h>
#include "mobile-so-com.h" 

#define LOG_TAG "MOBILE_SO_COM"
#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, fmt, ##args)


#define START_SEND_TO_PC     	 0X00000001     // start hijacking
#define STOP_SEND_TO_PC     	 0X00000002     // stop hijacking 
#define EXIT_CLIENT              0X00000003     // exit from current thread sub
#define SHUTDOWN_SERVER      	 0X00000004     // shutdown the thread_main
#define TRANS_CMD_ECHO           0X00000005     // echo what the thread has done for our commmand from pc
#define TRANS_SEND_DATA          0X00000006     // transmit the hijacked-send data to pc 
#define TRANS_RECV_DATA          0X00000007     // transmit the hijacked-received data to pc 
#define PROXY_SEND_DATA          0X00000008     // proxy mobile to send msg to target server

///////////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	int client_fd;
	int sockfd; 
	struct sockaddr_in client_addr;
	void * pcom;
}thread_param,*pthread_param;
 
unsigned long * pPackCnt = NULL;

/////////////////////////////////////////////////////////////////////////////////////////////////////
// below are functions about communication
int  MOBILE_SO_COM::send_msg_packet(int imsg_type,char * pmsg_body,int ibody_len,int client_fd,int target_fd)
{
	int imsg_len = sizeof(msg_packet)+ibody_len; 
	{
		char * pmsg_data = (char *)malloc(imsg_len); 
		// build head
		msg_packet head;
		head.imsg_type = imsg_type;
		head.ibody_len = ibody_len; 
		{
			head.client_fd = target_fd;
			memset(head.sz_IP,0,16);
			memcpy(head.sz_IP,"127.0.0.1",sizeof("127.0.0.1")); 
		}
		memcpy(pmsg_data,&head,sizeof(msg_packet));  
		// build body
		void * pbegin = pmsg_data + sizeof(msg_packet);
		memcpy(pbegin,pmsg_body,ibody_len); 
		// send msg
		send(client_fd,pmsg_data,imsg_len,0);
		free(pmsg_data);
		pmsg_data = NULL;
		printf("send_msg_packet->%s\n",pmsg_body );
	}
	return imsg_len;
} 
// recv msg packet from pc
int MOBILE_SO_COM::recv_msg_packet(char * pmsg_data,int imsg_len,int client_fd,int *pmsg_type)
{ 
	msg_packet head; 
	memcpy(&head,pmsg_data,sizeof(msg_packet));  
	char *pmsg_body = pmsg_data + sizeof(msg_packet);
	if (imsg_len == head.ibody_len + sizeof(msg_packet))
	{
		printf("len: %d,  data: %s\r\n",head.ibody_len,pmsg_body);
		LOGD("len: %d,  data: %s\r\n",head.ibody_len,pmsg_body);
		int imsg_type = head.imsg_type;
		if (START_SEND_TO_PC == imsg_type)					// start hijack data from hooked function
		{
			m_bsend = 1;
			char sz_hijack_start[] = "start hijack data...";
			send_msg_packet(TRANS_CMD_ECHO,sz_hijack_start,strlen(sz_hijack_start),client_fd,0);
		}
		else if (STOP_SEND_TO_PC == imsg_type)				// stop hijack data from hooked function
		{
			m_bsend = 0;
			char sz_hijack_stop[] = "stop hijack data...";
			send_msg_packet(TRANS_CMD_ECHO,sz_hijack_stop,strlen(sz_hijack_stop),client_fd,0);
		}
		else if (PROXY_SEND_DATA == imsg_type)				// proxy pc to send protocol buf hijacked from mobile processs
		{    
			proxy_sender(head.client_fd,pmsg_body,head.ibody_len,0);

			char sz_proxy_ok[] = "prox send data ok...";
			send_msg_packet(TRANS_CMD_ECHO,sz_proxy_ok,strlen(sz_proxy_ok),client_fd,0);
		}
		else												// unknown command
		{
			char sz_unkown_cmd[] = "unknown command";
			send_msg_packet(TRANS_CMD_ECHO,sz_unkown_cmd,strlen(sz_unkown_cmd),client_fd,0);
		} 
	}
	else
	{
		printf("len: %d,  error body\r\n",head.ibody_len);
	} 
	*pmsg_type = head.imsg_type;
	return head.ibody_len;
} 

void  MOBILE_SO_COM::transmit_send_data(char * pmsg_body,int ibody_len,int target_fd)
{
	send_msg_packet(TRANS_SEND_DATA,pmsg_body,ibody_len,m_to_client_fd,target_fd);
}
void  MOBILE_SO_COM::transmit_recv_data(char * pmsg_body,int ibody_len,int target_fd)
{
	send_msg_packet(TRANS_RECV_DATA,pmsg_body,ibody_len,m_to_client_fd,target_fd);
}
int  MOBILE_SO_COM::proxy_sender(int client_fd,void* pmsg_data,int len,int flag)
{ 
	if (pPackCnt)
	{
		(*pPackCnt)++; // auto to increase the packet counter
		LOGD("pPackCnt = %08x",(unsigned int)pPackCnt);
	}
    return send(client_fd,pmsg_data,len,0);
}

// main thread for accept client
void *MOBILE_SO_COM::thread_main(void *param)
{
	MOBILE_SO_COM * pso_com = (MOBILE_SO_COM *)param; 
	int sockfd;
	struct sockaddr_in s_add;
	int sin_size;
	unsigned short portnum=20086;
	printf("Hello,welcome to my server : port = %d!\r\n",portnum);
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(-1 == sockfd)
	{
		printf("socket fail ! \r\n");
		exit(1);
	}

	printf("socket ok !\r\n");

	bzero(&s_add,sizeof(struct sockaddr_in));
	s_add.sin_family=AF_INET;
	s_add.sin_addr.s_addr=htonl(INADDR_ANY);
	s_add.sin_port=htons(portnum);

	if(-1 == bind(sockfd,(struct sockaddr *)(&s_add), sizeof(struct sockaddr)))
	{
		printf("bind fail !\r\n");
		exit(1);
	}
	printf("bind ok !\r\n");

	// aysn
	int yes = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
	{
		perror("setsockopt");
		exit(1);
	}
	if(-1 == listen(sockfd,5))
	{
		printf("listen fail !\r\n");
		exit(1);
	}
	printf("listen ok\r\n");

	fd_set readfds;
	struct timeval tv ={0,50};

	while(1)
	{
		sleep(1);
		pthread_t  id;
		int ret;
		struct sockaddr_in client_addr;
		thread_param tparam ;

		sin_size = sizeof(struct sockaddr_in);

		FD_ZERO(&readfds);
		FD_SET(sockfd,&readfds);
		//printf("begin select \r\n");
		int iret = select(sockfd+1,&readfds,NULL,NULL,&tv);
		printf("iret = %d \t",iret);
		if (-1 == iret)
		{
			pso_com->m_bsend = 0;
			break;					//select failed 
		}
		else if (0 == iret)
		{
			continue;				// again check fd
		}
		else
		{
			int client_fd = accept(sockfd, (struct sockaddr *)(&client_addr), &sin_size);
			printf("client-fd= %d\r\n",client_fd);
			char sz_welcome[] = "good baby from server";
			pso_com->send_msg_packet(TRANS_CMD_ECHO,sz_welcome,strlen(sz_welcome),client_fd,0);
			if (client_fd <= 0)
			{
				perror("accept");
				break;;
			}
			tparam.client_fd = client_fd;
			tparam.client_addr = client_addr;
			tparam.sockfd = sockfd; 
			tparam.pcom = (void*)pso_com;
			ret=pthread_create(&id,NULL,&thread_sub,&tparam);
			if(ret!=0)
			{
				printf  ("Create pthread error!\r\n");
				break;
			}

			printf("accept ok!\r\nServer start get connect from %#x : %#x\r\n",ntohl(client_addr.sin_addr.s_addr),ntohs(client_addr.sin_port));
		}

	}
	pthread_exit(0);
	printf  ("shutdown server......!\r\n");
}

// sub thread for one socket communiction
void *MOBILE_SO_COM::thread_sub(void *param)
{
	thread_param  * ptparam = (thread_param *) param;
	MOBILE_SO_COM * pso_com = (MOBILE_SO_COM *)ptparam->pcom; 
	int irecvbytes; 
	int client_fd;  
	char buf[BUFFER_SIZE] = {0};
	char * pmsg_body = NULL;
	int imsg_type = 0;
	client_fd = ptparam->client_fd;
	pso_com->m_to_client_fd = client_fd;
	printf("thread_sub --> client-fd= %d\r\n",client_fd);
	LOGD("thread_sub --> client-fd= %d\r\n",client_fd);
	// aysn
	int yes = 1;
	if (setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) 
	{
		perror("setsockopt");
		exit(1);
	}

	while(1)
	{  
		fd_set readfds; 
		struct timeval tv ={0,300*1000}; 
		int ret; 
		struct sockaddr_in client_addr; 
		thread_param tparam ;

		FD_ZERO(&readfds); 
		FD_SET(client_fd,&readfds);   
		int iret = select(client_fd+1,&readfds,NULL,NULL,&tv); 
		if (-1 == iret)
		{
			pso_com->m_bsend = 0;
			break;					// select error 
		}
		else if (0 == iret)
		{
			continue;				// again check fd
		}
		else
		{
			irecvbytes = recv(client_fd, buf, BUFFER_SIZE, 0);
			if (irecvbytes <= 0)
			{
				pso_com->m_bsend = 0;
				break;				// select error
			} 
			else
			{  
				int imsg_body_len = 0; 
				imsg_body_len = pso_com->recv_msg_packet(buf,irecvbytes,client_fd,&imsg_type);
				
				if ((EXIT_CLIENT == imsg_type) || (SHUTDOWN_SERVER == imsg_type))
				{
					pso_com->m_bsend = 0;
		 			break;
				} 
			} 
		} 
	} 
	close(client_fd);
	if (SHUTDOWN_SERVER == imsg_type)				// shutdown main thread  accepting connections from client
	{
		close(ptparam->sockfd);
		printf("shutdown server\r\n");  
	} 
	printf("close the remote client\r\n");
	LOGD("close the remote client\r\n");
	pthread_exit(0);
}

// init mobile-main-com
void MOBILE_SO_COM::init_com()
{ 
	int ret = pthread_create(&m_main_thread_id, NULL, &thread_main, this);
	if (ret != 0)
	{
		printf("Create main thread error!\r\n");
		return ;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////