local cos = math.cos
local pi = math.pi
local deg = math.deg
local rad = math.rad
local acos = math.acos
local asin = math.asin
local abs = math.abs

-- -0.898706    0.000000    -0.438552
-- 0.825361    0.000000    -0.564606
cos0 = -0.898706; sin0 = -0.438552
-- cos0 = 0.825361; sin0 = -0.564606
-- cos0 = 0.818352; sin0 = -0.574717


local c1 = acos(cos0)
local c2 = -c1

local s1 = asin(sin0)
local s2
if s1 >= 0 then
    local x = abs(s1 - pi / 2)
    s1 = pi / 2 + x
    s2 = pi / 2 - x
else
    local x = abs(s1 + pi / 2)
    s1 = -(pi / 2) + x
    s2 = -(pi / 2) - x
end

local print_du = function(x)
    print(deg(x))
end

local equ = function(x, y)
    if abs(x - y) < 0.0001 then
        return true
    else
        return false
    end
end

if equ(c1, s1) then
    print_du(c1)
elseif equ(c1, s2) then
    print_di(c1)
elseif equ(c2, s1) then
    print_du(c2)
elseif equ(c2, s2) then
    print_du(c2)
else
    print("no~")
end
print("===================")
print_du(c1)
print_du(c2)
print_du(s1)
print_du(s2)



