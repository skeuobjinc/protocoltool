local ids = require("proto\\ids")

function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, hash, pos = string.unpack("<LL", raw, pos)
    local out = {}
    out[#out + 1] = 'AddAnimationStateModifier'
    out[#out + 1] = string.format("id[0x%08X]", id)
    out[#out + 1] = string.format("%s[0x%08X]", ids.get_hash_name(hash), hash)
    return table.concat(out, ' ')
end
