
require ("proto\\itemsId")
-- require ("itemsId")

function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local ids, typeid, pos = string.unpack("<LL", raw, pos)
    local x, pos = string.unpack(">f", raw, pos)
    local z, pos = string.unpack(">f", raw, pos)
    local y, pos = string.unpack(">f", raw, pos)
    local t, pos = string.unpack("<I4", raw, pos)
    local items_info = items_ids[typeid]
    if items_info == nil then
        return string.format("not create monster\r\nx : %f, y : %f",x, y)
    end
    if items_info[1] == "item" then
        -- if items_info[3] == '金子' then
            pos = pos + 4 * 5
            local r, pos = string.unpack(">f", raw, pos)
            local newcor = process_cor(x, y, t, r)
            return string.format("type: %s\r\nname:%s\r\nx : %f, y : %f\r\n%s\r\nr : %f", items_info[1], items_info[3], x, y, newcor, r)
        -- end
        -- return string.format("type: %s\r\nname:%s\r\nx : %f, y : %f\n", items_info[1], items_info[3], x, y)
    end
    return string.format("type: %s\r\ndifficulty: %s\r\nmonster_type: %s\r\nx : %f, y : %f", items_info[1], items_info[2], items_info[3], x, y)
end

function unpack_num(t, w)
    local f1 = (t >> w) & 0x1FF
    f1 = f1 / 511.0
    if ((t >> w) & 0x200) == 0 then
        f1 = f1 * 1.0
    else
        f1 = f1 * -1.0
    end
    return f1
end

function get_org_4(t)
    local u1 = unpack_num(t, 2)
    local u2 = unpack_num(t, 12)
    local u3 = unpack_num(t, 22)
    local m = u1*u1 + u2*u2 + u3*u3
    if m > 1.0 then
        m = 1.0
    else
        m = math.sqrt(1.0 - m)
    end

    local flag = t & 3
    local l
    if flag == 0 then
        l = {m, u1, u2, u3}
    elseif flag == 1 then
        l = {u1, m, u2, u3}
    elseif flag == 2 then
        l = {u1, u2, m, u3}
    elseif flag == 3 then
        l = {u1, u2, u3, m}
    end
    return l
end

function gen_rotation_matrix(t)
    local l = get_org_4(t)
    local u1, u2, u3, u4 = table.unpack(l)
    local a1 = {
        1.0 - (u2 * u2 + u3 * u3) * 2;
        (u1 * u2 + u3 * u4) * 2;
        (u1 * u3 - u2 * u4) * 2;
    }

    local a2 = {
       (u1 * u2 - u3 * u4) * 2;
       1.0 - (u1 * u1 + u3 * u3) * 2;
       (u2 * u3 + u1 * u4) * 2;
    }

    local a3 = {
        (u1 * u3 + u2 * u4) * 2;
        (u2 * u3 - u1 * u4) * 2;
        1.0 - (u1 * u1 + u2 * u2) * 2;
    }

    return {a1, a2, a3}
end

function format_mat(mat)
    local out = {}
    for i = 1, 3 do
        out[#out + 1] = string.format("[%f    %f    %f]", mat[i][1], mat[i][2], mat[i][3])
    end
    return table.concat(out, "\r\n")
end

function process_cor(x, y, t, r)
    local out = {}
    local mat = gen_rotation_matrix(t)
    out[#out + 1] = format_mat(mat)
    local cos0 = mat[1][1]
    local sin0 = mat[1][3]
    local xd = x - r * sin0
    local yd = r * cos0 + y
    out[#out + 1] = string.format("[%f\t%f]", xd, yd)
    return table.concat(out, "\r\n")
end
