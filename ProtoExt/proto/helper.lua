local _M = {}

_M.hex2str = function (hex)
    local out = {}
    for i = 1, #hex do
        out[#out + 1] = string.format("%02X", string.byte(hex, i))
    end
    return table.concat(out, ' ')
end

return _M