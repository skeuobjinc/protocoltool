
function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, pos = string.unpack("<L", raw, pos)
    local x, y, pos = string.unpack(">ff", raw, pos)
    local x1, y1 = string.unpack("<HH", raw, pos)
    x1 = f16_to_f32(x1)
    y1 = f16_to_f32(y1)
    return string.format("x : %f, y : %f\r\nx1 : %f, y1 : %f", x, y, x1, y1)
end