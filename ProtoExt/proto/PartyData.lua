local helper = require('proto\\helper')

function read_member(raw, pos)
    local member = {}
    local guid, name, b1, b2, city, u2, s1, s2, b3, b4, b5, b6
    member.guid, pos = string.unpack("<c16", raw, pos)
    member.name, pos = string.unpack("<s2", raw, pos)
    member.b1, pos = string.unpack("<B", raw, pos)
    member.b2, pos = string.unpack("<B", raw, pos)
    member.city, pos = string.unpack("<L", raw, pos)
    member.u2, pos = string.unpack("<L", raw, pos)
    member.s1, pos = string.unpack("<c16", raw, pos)
    member.s2, pos = string.unpack("<c16", raw, pos)
    member.b3, pos = string.unpack("<B", raw, pos)
    member.b4, pos = string.unpack("<B", raw, pos)
    member.b5, pos = string.unpack("<B", raw, pos)
    member.b6, pos = string.unpack("<B", raw, pos)
    return member, pos
end

function read_invite(raw, pos)
    local invite = {}
    invite.partyGUID, pos = string.unpack("<c16", raw, pos)
    invite.name, pos = string.unpack("<s2", raw, pos)
    return invite, pos
end


function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local partyGUID, leaderGUID, pos = string.unpack("<c16c16", raw, pos)

    -- 读取组内成员
    local members = {}
    local n, pos = string.unpack("<H", raw, pos)

    for i = 1, n do 
        local member

        member, pos = read_member(raw, pos)
        members[#members + 1] = member
    end

    -- 读取邀请个数
    local invites = {}
    local n, pos = string.unpack("<H", raw, pos)
    for i = 1, n do 
        local invite
        invite, pos = read_invite(raw, pos)
        invites[#invites + 1] = invite
    end

    local out = {}
    local write = function (t)
        out[#out + 1] = t
    end

    write(string.format("partyGUID : %s", helper.hex2str(partyGUID)))
    write(string.format("leaderGUID : %s", helper.hex2str(leaderGUID)))
    write('members : {')
    for _, member in ipairs(members) do
        write(string.format("\t[%d]", _))
        write(string.format("\t\tname : %s", member.name))
        write(string.format("\t\tguid : %s", helper.hex2str(member.guid)))
    end
    write('}')

    write('invites : {')
    for _, invite in ipairs(invites) do
        write(string.format("\tpartyGUID : %s", helper.hex2str(invite.partyGUID)))
        write(string.format("\tname : %s", invite.name))
    end
    write('}')
    return table.concat(out, "\r\n")
end