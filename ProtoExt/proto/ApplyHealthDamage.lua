function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, pos = string.unpack("<L", raw, pos)
    local x, pos = string.unpack("<H", raw, pos)
    x = f16_to_f32(x)
    local u1, pos = string.unpack("<L", raw, pos)
    local out = {}
    out[#out + 1] = string.format("被伤害者Id : 0x%X", id)
    out[#out + 1] = string.format("伤害 : %f", x)
    out[#out + 1] = string.format("伤害来源Id : 0x%X", u1)
    return table.concat(out, "\r\n")
end
