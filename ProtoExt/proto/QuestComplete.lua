local ids = require("proto\\ids")

function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, idme, hash, u1, pos = string.unpack("<LLLL", raw, pos)
    local out = {}
    out[#out + 1] = 'QuestComplete'
    out[#out + 1] = string.format("id[0x%08X] - idme[0x%08X]", id, idme)
    out[#out + 1] = string.format("%s[0x%08X] - u1[%08X]", ids.get_hash_name(hash), hash, u1)
    return table.concat(out, '\r\n')
end
