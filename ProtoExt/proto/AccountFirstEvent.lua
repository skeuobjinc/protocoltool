local ids = require("proto\\ids")

function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local hash1, hash2, pos = string.unpack("<LL", raw, pos)
    return string.format("AccountFirstEvent %s[%08X] %s[%08X]", ids.get_hash_name(hash1), hash1,
     ids.get_hash_name(hash2), hash2)
end
