function main(raw)
    local list = {
        "deviceID",
        "deviceModel",
        "systemVersion",
        "bundleVersion",
        "countryCode",
        "language",
        "pushNotificationDeviceToken",
        "source"
    }
    local len, cmdid, pos = string.unpack("<LL", raw)
    local type, pos = string.unpack("<L", raw, pos)
    local out = {}
    local key = 0
    local str = ""
    for i=1,8 do
        key, str, pos = string.unpack("<Ls2", raw, pos)
        if str ~= "" then
            out[#out + 1] = string.format("%s : %s", list[i], encode_str(str, key, 0, 0xFF))
        else
            out[#out + 1] = string.format("%s : %s", list[i], "null")
        end
    end
    -- 特色设备信息
    local n, pos = string.unpack("<H", raw, pos)
    out[#out + 1] = '{'
    for i = 1, n do
        local mid, key, str
        mid, key, str, pos = string.unpack("<LLs2", raw, pos)
        out[#out + 1] = string.format("\t[%08X] : %s", mid, encode_str(str, key, 0, 0xFF))
    end
    out[#out + 1] = '}'

    local hex = {}
    for i=pos, #raw - 1 do
        hex[#hex + 1] = string.format("%02X", string.byte(raw, i))
    end
    out[#out + 1] = string.format("%s : %s", "Advertising ID", table.concat(hex, ' '))
    out[#out + 1] = string.format("end : %d", string.byte(raw, #raw))
    return table.concat(out, '\r\n')
end 