function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, id1, pos = string.unpack("<LL", raw, pos)
    return string.format("0x%08X 0x%08X", id, id1)
end