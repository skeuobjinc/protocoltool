function main(raw)
    local out = {}
    local len, cmdid, pos = string.unpack("<LL", raw)
    local u1, u2, pos = string.unpack("<LL", raw, pos)
    local nfile, pos = string.unpack("<H", raw, pos)
    out[#out + 1] = '{'
    for i = 1, nfile do
        local filename, crc32, len
        filename, crc32, len, pos = string.unpack("<s2LL", raw, pos)
        out[#out + 1] = string.format("\t[%02d] [%08X] [%08X] %s", i, crc32, len, filename)
    end
    out[#out + 1] = '}'

    local hex = {}
    for i = pos, pos + 16 do
        hex[#hex + 1] = string.format("%02X", string.byte(raw, i))
    end
    out[#out + 1] = string.format("accountIdentifer : %s", table.concat(hex, " "))

    pos = pos + 16
    local n, pos = string.unpack("<H", raw, pos)
    for i = 1, n do
        out[#out + 1] = string.format("cluster : %s", string.unpack("<s2", raw, pos))
    end

    return table.concat(out, "\r\n")
end
