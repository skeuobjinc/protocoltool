local ids = require("proto\\ids")

function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local hash1, pos = string.unpack("<L", raw, pos)
    return string.format("AddZoneFlag %s[%08X]", ids.get_hash_name(hash1), hash1)
end