function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, pos = string.unpack("<L", raw, pos)
    local x, y, l, pos = string.unpack(">fff", raw, pos)
    local skill, n, pos = string.unpack("<LB", raw, pos)
    local out = {}
    out[#out + 1] = string.format("id : %d", id)
    out[#out + 1] = string.format("x : %f, y : %f, l : %f", x, y, l)
    out[#out + 1] = string.format("skill : 0x%08X", skill)
    out[#out + 1] = string.format("n : %d", n)
    return table.concat(out, "\r\n")
end