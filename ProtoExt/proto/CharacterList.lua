function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local n, pos = string.unpack("<L", raw, pos)
    local idlist = {}
    for i = 1, n do
        idlist[#idlist + 1], pos = string.unpack("c16", raw, pos)
    end

    local characterList = {}
    n, pos = string.unpack("<L", raw, pos)
    for i = 1, n do
        local ids, name, level, heroClassHash, renameCast
        ids, heroClassHash, name, level, renameCast, pos = string.unpack("<c16Ls2LL", raw, pos)
        characterList[#characterList + 1] = {
            ["ids"] = ids;
            ["name"] = name;
            ["level"] = level;
            ["heroClassHash"] = heroClassHash;
            ["renameCast"] = renameCast;
        }
    end
    local out = {}
    for i = 1, n do
        out[#out + 1] = '{'
        out[#out + 1] = string.format("\tname : %s", characterList[i].name)
        out[#out + 1] = string.format("\tlevel : %d", characterList[i].level)
        out[#out + 1] = string.format("\theroClassHash : 0x%08X", characterList[i].heroClassHash)
        out[#out + 1] = string.format("\trenameCast : 0x%d", characterList[i].renameCast)
        out[#out + 1] = '}'
    end
    return table.concat(out, "\r\n")
end