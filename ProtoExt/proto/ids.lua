local _M = {}

_M.ids = {}

_M.get_hash_name = function (hash)
    local name = _M.ids[hash]
    if name ~= nil then
        return name
    else
        return string.format("0x%08X", hash)
    end
end

local IDS = function (hash, name)
    _M.ids[hash] = name
end
IDS(0x13380A76, 'save_villagers')
IDS(0xC3A6A434, 'equip_inventory')
IDS(0x109595EC, 'completed_cutscene_0_0_0')
IDS(0xC500384D, 'tutorial_cutscene_end')
IDS(0x29AFEEB4, 'tutorial_01')
IDS(0xE60B5A59, 'tutorial_01a')
IDS(0xBD6F22D8, 'started_cutscene_0_0_0')
IDS(0x1C5EFAA8, 'tutorial_04')
IDS(0xEE3579AB , 'tutorial_05')
IDS(0xFD658A5F , 'tutorial_06')
IDS(0x9C66931C , 'tutorial_06a')
IDS(0x0F0E095C , 'tutorial_07')
IDS(0x3FFA2DF8 , 'visited_inventory')
IDS(0x511D3578 , 'tutorial_08')
IDS(0x90B51CBD , 'tutorial_select_weapon')
IDS(0xA376B67B , 'tutorial_09')
IDS(0xCF9A0D39 , 'tutorial_equip_weapon')
IDS(0xE9B3B34E , 'save_villagers_complete')
IDS(0xA5DA76AA , 'tutorial_exit_inventory')
IDS(0xA78A2D46 , 'waiting_to_complete')
IDS(0xC866F5C0 , 'tutorial_10')
IDS(0x3A0D76C3 , 'tutorial_11')
IDS(0xDB360634 , 'tutorial_13')
IDS(0x0FFC62DF , 'tutorial_14')
IDS(0xFD97E1DC , 'tutorial_15')
IDS(0xE3AF1AFF , 'active_quest')
IDS(0x0DC0A9FB , 'tutorial_15a')
IDS(0x1CAC912B , 'tutorial_17')
IDS(0x9DD4D2EA , 'door_closed_2')
IDS(0x9080FF46 , 'tutorial_pack_01')
IDS(0x42BFAD0F , 'tutorial_18')
IDS(0x39D56AE1 , 'tutorial_17b')
IDS(0xB0D42E0C , 'tutorial_19')
IDS(0x3DEA3158 , 'quest/brackenridge/karl_01.qst')
IDS(0x7B45A986 , 'started_cutscene_gustav_jarl')
IDS(0x399EFBA9 , 'skipped_cutscene_gustav_jarl')
IDS(0xD7F37055 , 'quest/brackenridge/tavern_01.qst')
IDS(0xE83F6C56 , 'pose_run_to_wounded')
return _M