local ids = require("proto\\ids")

function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, hash1, hash2, pos = string.unpack("<LLL", raw, pos)
    local out = {}
    out[#out + 1] = string.format("id : 0x%08X", id)
    out[#out + 1] = string.format("hash1 : %s[0x%08X]", ids.get_hash_name(hash1), hash1)
    out[#out + 1] = string.format("hash2 : %s[0x%08X]", ids.get_hash_name(hash2), hash2)
    return table.concat(out, "\r\n")
end
