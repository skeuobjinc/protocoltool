function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, pos = string.unpack("<L", raw, pos)
    local hp, pos = string.unpack(">f", raw, pos)
    local out = {}
    out[#out + 1] = string.format("id : 0x%08X", id)
    out[#out + 1] = string.format("hp : %f", hp)
    return table.concat(out, "\r\n")
end
