local ids = require("proto\\ids")

function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local hash1, hash2, hash3, pos = string.unpack("<LLL", raw, pos)
    local out = {}
    out[#out + 1] = "TutorialAdvanceToNextStage"
    out[#out + 1] = string.format("\t%s[%08X]", ids.get_hash_name(hash1), hash1)
    out[#out + 1] = string.format("\t%s[%08X]", ids.get_hash_name(hash2), hash2)
    out[#out + 1] = string.format("\t%s[%08X]", ids.get_hash_name(hash3), hash3)
    return table.concat(out, "\r\n")
end