function main(raw)
    local len, cmdid, pos = string.unpack("<LL", raw)
    local id, pos = string.unpack("<L", raw, pos)
    local seepd, pos = string.unpack(">f", raw, pos)
    return string.format("id : %d, seepd : %f", id, seepd)
end