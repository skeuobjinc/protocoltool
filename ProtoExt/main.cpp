﻿#include <Windows.h>
#include <stdio.h>
#include <string>
#include <vector>
#include "define.h"
#include "lua.hpp"

void set_lua_environment(lua_State *L);

DWORD WINAPI DllMain(void* dllHandle, unsigned long reason, void* reserved) {
    return TRUE;
}

static std::wstring cover_utf8_2_unicode(const char *utf8String) {
    int len = MultiByteToWideChar(CP_UTF8, 0, utf8String, -1, NULL, 0);
    if (len == 0) {
        return L"";
    }

    std::vector<wchar_t> utf16Buf(len);
    MultiByteToWideChar(CP_UTF8, 0, utf8String, -1, &utf16Buf[0], utf16Buf.size());

    std::wstring retVal(&utf16Buf[0]);
    return retVal;
}

std::string cover_unicode_2_asccii(const wchar_t *unicodeString) {
    int len = WideCharToMultiByte(CP_ACP, 0, unicodeString, -1, nullptr, 0, 0, false);
    if (len == 0) {
        return "";
    }

    std::vector<char> ascciiBuf(len);
    WideCharToMultiByte(CP_ACP, 0, unicodeString, -1, &ascciiBuf[0], ascciiBuf.size(), 0, false);

    std::string retVal(&ascciiBuf[0]);
    return retVal;
}

PROTO_EXT_EXPORT
void explain_proto(const wchar_t *protoName, const char *data, size_t dataLen, wchar_t *out, size_t outLen) {
    char fileName[MAX_PATH] = { 0 };
    std::string strProtoName = cover_unicode_2_asccii(protoName);
    sprintf_s(fileName, ".\\proto\\%s.lua", strProtoName.c_str());
    OutputDebugStringA(fileName);
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);
    set_lua_environment(L);

    if (L != nullptr) {
        if (!luaL_dofile(L, fileName)) {
            if (lua_getglobal(L, "main") == LUA_TFUNCTION) {
                lua_pushlstring(L, data, dataLen);
                lua_pcall(L, 1, 1, 0);
                // 无论是异常 还是 正常 返回结果都在第一个参数里面
                std::wstring text = cover_utf8_2_unicode(lua_tostring(L, -1));
                wcscpy_s(out, outLen - 1, text.c_str());
            } else {
                wcscpy_s(out, outLen - 1, L"协议脚本中没有main函数.");
            }
        } else {
            std::wstring text = cover_utf8_2_unicode(lua_tostring(L, -1));
            wcscpy_s(out, outLen - 1, text.c_str());
        }
    } else {
        wcscpy_s(out, outLen - 1, L"申请lua_State失败.");
    }
    lua_close(L);
}

PROTO_EXT_EXPORT
bool script_run(const char *data, size_t dataLen, wchar_t *out, size_t outLen) {
    bool result = false;
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);
    set_lua_environment(L);

    if (L != nullptr) {
        if (!luaL_dofile(L, "script.lua")) {
            if (lua_getglobal(L, "main") == LUA_TFUNCTION) {
                lua_pushlstring(L, data, dataLen);
                if (lua_pcall(L, 1, 1, 0) == LUA_OK) {
                    result = true;
                } else {
                    result = false;
                }
                // 无论是异常 还是 正常 返回结果都在第一个参数里面
                std::wstring text = cover_utf8_2_unicode(lua_tostring(L, -1));
                wcscpy_s(out, outLen - 1, text.c_str());
            } else {
                wcscpy_s(out, outLen - 1, L"协议脚本中没有main函数.");
                result = false;
            }
        } else {
            std::wstring text = cover_utf8_2_unicode(lua_tostring(L, -1));
            wcscpy_s(out, outLen - 1, text.c_str());
            result = false;
        }
    } else {
        wcscpy_s(out, outLen - 1, L"申请lua_State失败.");
        result = false;
    }
    lua_close(L);
    return result;
}