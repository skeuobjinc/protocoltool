#include <stdint.h>
#include <stdio.h>
#include <string>
#include "lua.hpp"
#include "ieeehalfprecision.h"


static int l_f16_to_f32(lua_State *L) {
    uint16_t num = (uint16_t)(lua_tointeger(L, 1) & 0xFFFF);
    float n = 0;
    halfp2singles(&n, &num, 1);
    lua_pushnumber(L, n);
    return 1;
}

static int l_hex_to_str(lua_State *L) {
    size_t size = 0;
    const char *raw = luaL_checklstring(L, 1, &size);
    char *buf = new char[size * 2 + 1];
    for (size_t i = 0; i < size * 2; i += 2) {
        sprintf_s(buf, 3, "%02X", *raw);
        raw = raw + 1;
    }
    lua_pushstring(L, buf);
    delete[] buf;
    return 1;
}

static int crypt_byte_key_iter(int32_t *key, int low, int limit) {
    int32_t m = *key ^ 0x075bd924;
    int32_t a = (-2092037281LL * m) >> 32;
    a = m + a;

    int32_t x = 0;
    if (m >= 0) {
        x = 0;
    } else {
        x = -1;
    }

    a = (a >> 16) - x;
    m = 0xFFFE0CE3 * a + m;
    int32_t p = 0xFFFFF4EC * a;

    m = 0x41A7 * m + p;

    if (m < 0) {
        m = m + 0x80000000;
        m = m + 0xFFFFFFFF;
    }

    *key = m ^ 0x75BD924;

    float mmm = (float)(m / 2147483648.0);
    float fl = (limit + 1) * mmm;
    mmm = 1.0f - mmm;

    fl = fl + mmm * low;

    int result = (fl > 255) ? 255 : (int)fl;

    return result;
}

static int l_encode_str(lua_State *L) {
    size_t len = 0;
    const char *str = luaL_checklstring(L, 1, &len);
    int32_t key = (int32_t)luaL_checkinteger(L, 2);
    int32_t low = (int32_t)luaL_checkinteger(L, 3);
    int32_t limit = (int32_t)luaL_checkinteger(L, 4);
    std::string out;
    for (size_t i = 0; i < len; i++) {
        uint8_t t = (uint8_t)str[i] ^ (uint8_t)crypt_byte_key_iter(&key, low, limit);
        out.push_back((char)t);
    }
    lua_pushlstring(L, out.c_str(), out.size());
    return 1;
}

void set_lua_environment(lua_State *L) {
    lua_register(L, "f16_to_f32", l_f16_to_f32);
    lua_register(L, "hex_to_str", l_hex_to_str);
    lua_register(L, "encode_str", l_encode_str);
}